<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Wachtwoorden moeten tenminste 8 tekens lang zijn en overeenkomen',
    'reset' => 'Uw wachtwoord is gewijzigd.',
    'sent' => 'We hebben u een mail gestuurd met een wachtwoord-link.',
    'token' => 'Dit token is niet juist.',
    'user' => 'Dit email-adres is niet bij ons bekend.',

];
