@extends('layouts.email')

@section('content')

    <?php
    use App\User;

    setlocale(LC_ALL,'nl_NL.utf8');
    try
    {
        $goedkeurder = User::findOrFail($aanvraag->goedgekeurd_door);
        $goedkeurNaam = $goedkeurder->name;
    } catch (Exception $e)
    {
        $goedkeurNaam = '';
    }
    ?>
    <p>Geachte {{ $aanvraag->deelnemer->name }},<br/></p>

    <p>U heeft op {{ strftime("%e %B %Y", strtotime($aanvraag->ingediend)) }} een aanvraag tot certificatie voor het register
        {{ $aanvraag->register->naam }} ({{ $aanvraag->register->code }}) gedaan.<br/></p>

    <p>Met genoegen delen we u mede dat deze aanvraag is goedgekeurd op {{ strftime("%e %B %Y", strtotime($aanvraag->goedgekeurd)) }}.
        Uw certificatie is {{ $aanvraag->register->geldig }} maanden geldig vanaf deze datum.<br/></p>

    <p>Groeten,<br/></p>

    <p>Stichting Anoniem<br/></p>
    @if($goedkeurNaam != '')<p>{{ $goedkeurNaam }}</p>@endif

@endsection
