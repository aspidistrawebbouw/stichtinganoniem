@extends('layouts.email')

@section('content')

    <?php
    use App\User;
    use App\Aanvraag;
    setlocale (LC_ALL, 'nl_NL.utf8');
    ?>
    <p>Geachte {{ $admin->name }},<br/></p>

    <p>Er heeft zich een nieuwe gebruiker aangemeld op het extranet van Stichting Anoniem.</p>

    <p>Naam: {{ $user->name }}</p>
    <p>Email: {{ $user->email }}</p>
    <p>Klik <a href="{{ url('personen/' . $user->id . '/edit' ) }}">hier</a> om deze persoon te bewerken.</p>

    <p>Groeten,<br/></p>

    <p>Stichting Anoniem<br/></p>

@endsection
