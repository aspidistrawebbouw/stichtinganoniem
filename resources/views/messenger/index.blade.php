@extends('layouts.app')

@section('content')
@include('messenger.partials.flash')

<a href="{{ url('/messages/create') }}"><button class="btn btn-primary">Nieuw bericht</button></a>
@each('messenger.partials.thread', $threads, 'thread', 'messenger.partials.no-threads')

@stop
