@extends('layouts.app')

@section('content')

<h3>Aanvraag certificatie voor register: {{ $register->naam }} ({{ $register->code }})</h3>
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif	
</div>

@if ($vervolg == 0)
<p>Dit is de eerste maal dat u dit certificaat aanvraagt.</p>
<form class="pure-form" action="{{ $action }}" method="POST"  enctype="multipart/form-data">
	<input type="hidden" name="vervolg" value="0"/>
	<input type="hidden" name="register_id" value="{{ $register->id }}"/>
	@csrf
	<p><input type="checkbox" value="J" name="voorwaarden">Ja, ik heb de <span style="padding: 0 5px"><a href="{{ url('/registers/' . $register->id ) }}">algemene informatie</a></span> gelezen.</input></p>
	<p><input type="checkbox" value="J" name="betaald"> Ik heb betaald (hier komt t.z.t. de betaalgateway)</input></p>
	<p><button class="pure-button button-success waarsch"><i class="fa fa-save"></i> Aanvraag opslaan</button></p>
	<p>Nadat u op de knop "Aanvraag opslaan" heeft gedrukt, kunt u de aanvraag aanvullen met onderbouwende documenten.</p>
</form>
@else
<p>U hebt dit certificaat al eerder behaald en vraagt nu verlenging aan.</p>
<form class="pure-form" action="{{ $action }}" method="POST"  enctype="multipart/form-data">
	<input type="hidden" name="vervolg" value="1"/>
	<input type="hidden" name="register_id" value="{{ $register->id }}"/>
	@csrf
	<p><input type="checkbox" value="J" name="voorwaarden">Ja, ik heb de <span style="padding: 0 5px"><a href="{{ url('/registers/' . $register->id ) }}">algemene informatie</a></span> gelezen.</input></p>
	<p><input type="checkbox" value="J" name="betaald"> Ik heb betaald (hier komt t.z.t. de betaalgateway)</input></p>
	<p><button class="pure-button button-success waarsch"><i class="fa fa-save"></i> Aanvraag opslaan</button></p>
	<p>Nadat u op de knop "Aanvraag opslaan" heeft gedrukt, kunt u de aanvraag aanvullen met onderbouwende documenten.</p>
</form>
@endif
@endsection
