@extends('layouts.app')

@section('content')

    @php
        use App\Prestatie;
        setlocale(LC_ALL, 'nl_NL.utf8')
    @endphp

    @if (!$aanvraag->vervolg())
        <h3>Aanvraag toelating tot register: {{ $register->naam }} ({{ $register->code }})</h3>
    @else
        <h3>Aanvraag verlenging certificatie register: {{ $register->naam }} ({{ $register->code }})</h3>
    @endif
    <div id="app">
        @include('flash-message')
        @yield('content')
        @if($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </div>
    <p>Deelnemer: {{ $aanvraag->deelnemer->name }}</p>
    <p>Status: {{ $aanvraag->status->naam }}</p>
    @if ($aanvraag->aanvraagStatus == 1 && $aanvraag->afgekeurd)
        <p>Deze aanvraag is afgekeurd op {{ strftime("%e %B %Y", strtotime($aanvraag->afgekeurd)) }}.</p>
    @endif

    <h4>Algemene informatie</h4>
    @foreach ($register->teksten as $tekst)
        <p>{!! $tekst->tekst !!}</p>
    @endforeach

    @php
        if (!$aanvraag->vervolg())
        {
            echo "<h4>Toelatingscriteria</h4>"; $criteria = $register->toelatingsCriteria();
        } else
        {
            echo "<h4>Criteria voor verlenging</h4>"; $criteria = $register->vervolgCriteria();
        }
        $i = 1
    @endphp
    @foreach ($criteria as $crit)
        <fieldset>
            <h4 id="crit{{ $crit->id}}">{{ $i }}. {{ $crit->naam }}</h4>
            <p>{!! nl2br($crit->beschrijving) !!}</p>
            <p>{{ $crit->invulinstructie }}</p>
            <?php
            if ($aanvraag->prestaties->where ('activiteit_id', $crit->id)->isNotEmpty ()) {
                $prestatie = $aanvraag->prestaties->where ('activiteit_id', $crit->id)->first ();
                $action = url ('/prestaties/' . $prestatie->id);
            } else {
                $prestatie = new Prestatie();
                $action = url ('/prestaties');
                echo '<p class="text-primary"><strong>U heeft dit criterium nog niet ingevuld.</strong></p>';
            }
            ?>
            <form class="pure-form" action="{{ $action }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="aanvraag_id" value="{{ $aanvraag->id }}"/>
                <input type="hidden" name="activiteit_id" value="{{ $crit->id }}"/>
                @if ($aanvraag->prestaties->where('activiteit_id', $crit->id)->count() == 1)
                    <input type="hidden" name="_method" value="PUT">
                @endif
                @if ($crit->criType_id == 7)
                    @component('aanvragen.pepunten', ['prestatie' => $prestatie, 'crit' => $crit, 'wijzigbaar' => $wijzigbaar ])
                    @endcomponent
                @elseif ($crit->criType_id == 8)
                    @component('aanvragen.upload', ['prestatie' => $prestatie, 'wijzigbaar' => $wijzigbaar ])
                    @endcomponent
                @elseif ($crit->criType_id == 10)
                    @component('aanvragen.tekstveld', ['prestatie' => $prestatie, 'wijzigbaar' => $wijzigbaar ])
                    @endcomponent
                @elseif ($crit->criType_id == 11)
                    @component('aanvragen.tekstblok', ['prestatie' => $prestatie, 'wijzigbaar' => $wijzigbaar ])
                    @endcomponent
                @elseif ($crit->criType_id == 12)
                    @component('aanvragen.janee', ['prestatie' => $prestatie,'crit' => $crit, 'wijzigbaar' => $wijzigbaar])
                    @endcomponent
                @elseif ($crit->criType_id == 13)
                    @component('aanvragen.opleidingentabel', ['crit' => $crit, 'prestatie' => $prestatie, 'wijzigbaar' => $wijzigbaar ])
                    @endcomponent
                @elseif ($crit->criType_id == 14)
                    @component('aanvragen.werkgeverstabel', ['crit' => $crit, 'prestatie' => $prestatie, 'wijzigbaar' => $wijzigbaar ])
                    @endcomponent
                @endif
                @if ($aanvraag->status->naam == 'opgeslagen')
                    <p>
                        <button class="pure-button button-success waarsch"><i class="fa fa-save"></i> Opslaan</button>
                    </p>
                @endif
                @if (session('success' . $crit->id))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ session('success' . $crit->id) }}</strong>
                    </div>
                @endif
            </form>
        </fieldset>
        @php
            $i++
        @endphp
    @endforeach
    <p style="white-space: pre-wrap;">{{ $aanvraag->register->slottekst }}</p>

    @if ($aanvraag->status->naam == 'opgeslagen')
        <p>U kunt uw aanvraag, ook als deze nog niet compleet is, opslaan zonder deze ter beoordeling in te dienen.
            Pas als u op de onderstaande knop Indienen drukt, wordt de aanvraag in behandeling genomen. <strong>Let
                op:</strong> Als u uw aanvraag ingediend heeft, kunt u deze niet meer wijzigen.</p>
        <p><a href="{{ url('/indienen/' . $aanvraag->id) }}">
                <button class="pure-button button-success waarsch"><i class="fas fa-paper-plane"></i> Indienen</button>
            </a></p>
    @elseif ($aanvraag->status->naam == 'ingediend')
        <p>U heeft deze aanvraag ingediend op {{ strftime("%e %B %Y", strtotime($aanvraag->ingediend)) }}. De aanvraag
            is momenteel in behandeling bij de Toetsingscommissie van het register. U kunt deze aanvraag daarom niet
            meer wijzigen.</p>
    @elseif ($aanvraag->status->naam == 'goedgekeurd')
        <p>Gefeliciteerd! U voldoet aan de criteria voor dit register. U kunt deze aanvraag daarom niet meer
            wijzigen.</p>
    @endif
    @if (isset($scrollTo))
        <script>
            var element = document.getElementById("{{ $scrollTo }}");
            element.scrollIntoView();
        </script>
    @endif

@endsection
