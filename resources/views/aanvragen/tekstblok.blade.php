<!--

Bij een nieuwe aanvraag is $prestatie een nieuwe, lege instantie van Prestatie. Dan is $prestatie->id dus niet gezet.
Bij een opgeslagen $prestatie behorende bij dit criterium bestaat er dus een Tekst ($prestatie->tekst) die we kunnen uitvragen.
-->
<textarea name="tekst" cols="50" @if (!$wijzigbaar) disabled
          @endif style="white-space: pre-wrap;">@if (isset($prestatie->id) && $prestatie->tekst && $prestatie->tekst->tekst) {{ $prestatie->tekst->tekst }} @endif</textarea>
