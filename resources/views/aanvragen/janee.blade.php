<!--

Bij een nieuwe aanvraag is $prestatie een nieuwe, lege instantie van Prestatie. Dan is $prestatie->id dus niet gezet.
Bij een opgeslagen $prestatie behorende bij dit criterium bestaat er dus een JaNee ($prestatie->janee) die we kunnen uitvragen.
-->
	<p> <input type="radio" name="janee" value="{{ $crit->veld1 }}" @if (isset($prestatie->id) && $prestatie->janee->janee) checked @endif 
		@if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input> {{ $crit->veld1 }}&nbsp;
		<input type="radio" name="janee" value="{{ $crit->veld2 }}" @if (isset($prestatie->id) && !$prestatie->janee->janee) checked @endif
		@if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input> {{ $crit->veld2 }}
	</p>
