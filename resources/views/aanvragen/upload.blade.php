	<p>Het bestand moet een PDF-document zijn of een afbeelding (jpg, png, etc.). De maximale upload-grootte is 50Mb.</p>
	<input type="file" name="upload" accept="image/*,.pdf" @if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif>
	<p>Evt. opmerkingen hierbij:</p>
	<textarea name="opmerkingen" rows="5" style="width: 500px;">{{ $prestatie->opmerkingen }}</textarea>
	@if (isset($prestatie->uploads) && $prestatie->uploads->count() > 0)
		<p>Geuploade documenten</p>
		<table class="pure-table">
			<thead><tr><th>Titel</th><th>Ge&uuml;pload op</th><th><i class="far fa-trash-alt"></th></i></thead>
		<?php  foreach ($prestatie->uploads as $upload) { ?>
			<tr>
				<td><i class="fa fa-file"></i> <a href="{{ Storage::disk('uploads')->url($upload->filenaam) }}" download="{{ $upload->originelefilenaam }}">{{ $upload->originelefilenaam }}</a></td>
				<td>{{ $upload->updated_at }}</td>
				<td><input type="checkbox" name="doc{{ $upload->id }}" value="1"></input></td></a>
			</tr>
		<?php } ?>
		</table>
	@endif
