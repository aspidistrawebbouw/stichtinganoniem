@extends('layouts.app')

@section('content')

<?php use App\Prestatie; ?>

<h3>Aanvraag certificatie voor register: {{ $aanvraag->register->naam }} ({{ $aanvraag->register->code }})</h3>
@if ($aanvraag->vervolg())
<h3>Verlenging van een eerder verleend certificaat</h3>
@else
<h3>Toelating</h3>
@endif
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif	
</div>
<h4>Status: {{ $aanvraag->status->naam }}</h4>
<?php setlocale(LC_ALL, 'nl_NL.utf8'); ?>
<p>Deelnemer: <i class="fas fa-user"></i> {{ $aanvraag->deelnemer->name }} <i class="fas fa-envelope"></i> {{ $aanvraag->deelnemer->email }}</p>

<fieldset>
<p>Gegevens in <span class="citaat">blauw</span> zijn ingevuld door deelnemer.</p>
<h4>Criteria</h4>
<ol>
@foreach ($aanvraag->register->criteria->where('vervolg', $aanvraag->vervolg()) as $crit)
	<li><h5 id="crit{{ $crit->id}}">{{ $crit->naam }}</h5>
	<p><strong>Beschrijving</strong><br/>{!! $crit->beschrijving !!}</p>
	<p><strong>Soort criterium</strong>: {{ $crit->criteriumType->naam }}</p>

	@if ($aanvraag->prestaties->where('activiteit_id', $crit->id)->isNotEmpty())
		<?php $prestatie = $aanvraag->prestaties->where('activiteit_id', $crit->id)->first(); ?>

		<p><span style="color: green"><i class="fa fa-check"></i> Kandidaat heeft hieraan voldaan als volgt:</p> 
			@if ($crit->criType_id == 7) 
				@component ('aanvragen.toets.pepunten', ['prestatie' => $prestatie, 'crit' => $crit])
				@endcomponent
			@elseif ($crit->criType_id == 8)
				@component ('aanvragen.toets.uploads', ['prestatie' => $prestatie, 'crit' => $crit])
				@endcomponent
			@elseif ($crit->criType_id == 12)
				@component ('aanvragen.toets.janee', ['prestatie' => $prestatie, 'crit' => $crit])
				@endcomponent
			@elseif ($crit->criType_id == 13)
				@component ('aanvragen.toets.opleidingentabel', ['prestatie' => $prestatie, 'crit' => $crit])
				@endcomponent
			@elseif ($crit->criType_id == 14)
				@component ('aanvragen.toets.werkgevertabel', ['prestatie' => $prestatie, 'crit' => $crit])
				@endcomponent
			@endif
		<p>Laatste maal gewijzigd: <span class="citaat">{{ $prestatie->datum }}</span></p>
	@else
		<p><span style="color: red"><i class="fas fa-minus"></i></span> Kandidaat heeft hier nog niets ingevuld.</p>
	@endif
	</li>
@endforeach
</ol>
</fieldset>
@if ($aanvraag->aanvraagStatus == 2) 
<p>
	<form class="pure-form" action="{{ url('/aanvragen/' . $aanvraag->id . '/goedkeuren') }}" method="POST">
		@csrf
		<label for="goedgekeurdDT">Datum goedkeuring:</label><input type="date" name="goedgekeurdDT" value="{{ date('Y-m-d') }}"></input>
		<button class="pure-button button-success waarsch">Goedkeuren</button>
	</form>
</p>
	<a href="{{ url('/aanvragen/' . $aanvraag->id . '/afkeuren') }}"><button class="pure-button button-warning waarsch">Afkeuren</button></a>
</p>
@endif
<p><a href="{{ url('/aanvragen') }}"><button class="pure-button button-secondary"><i class="fas fa-arrow-left"></i> Terug naar lijst aanvragen</button></a></p>
<p><form class="pure-form" action="{{ url('/aanvragen/' . $aanvraag->id) }}" method="POST">
		@csrf
		<input type="hidden" name="_method" value="DELETE">
		<button class="pure-button button-warning"><i class="fa fa-trash"></i> Aanvraag verwijderen</button>
</form></p>

@endsection
