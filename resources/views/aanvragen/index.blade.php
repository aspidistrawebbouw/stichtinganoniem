@extends('layouts.app')

@section('content')

<?php
use App\User;
use App\Aanvraag;
?>
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif 
	
</div>
<h3>Beheer Aanvragen</h3>
<p>Toon: <button class="togglecategorieenAlles pure-button button-success">Alles</button>
		@foreach ($statussen as $cat)
			<button dataStr="{{ str_replace(' ', '_', $cat->naam) }}" class="togglecategorieen pure-button button-success"><i class="fas fa-filter"></i> {{ $cat->naam }}</button>&nbsp;
		@endforeach
</p>
<table id="aanvragenTabel" class="pure-table">
	<thead>
	<th>Deelnemer</th>
	<th>Register</th>
	<th>Toelating<br/>of verlenging</th>
	<th>Status (<span style="color: green;"><i class="fa fa-check"></i></span> geldig,<br/>
		<span style="color: orange;"><i class="fa fa-check"></i></span> bijna verlopen,
		<span style="color: red;"><i class="fa fa-times"></i></span> verlopen)
	</th>
	<th>Toetsingscommissie</th>
	</thead>
	@foreach ($aanvragen as $aanvraag)
	@if (Auth::user()->isAdmin()
		|| (Auth::user()->isBeheerder() && $aanvraag->register->wordtBeheerdDoor(Auth::user()->id))
		|| (Auth::user()->isTC() && $aanvraag->register->wordtGetoetstDoor(Auth::user()->id))
		)

	<tr class="{{ $aanvraag->status->naam }}">
		<td><a href="{{ url('/aanvragen/' . $aanvraag->id) }}">{{ $aanvraag->deelnemer->name }}</a></td>
		<td>{{ $aanvraag->register->code }} ({{ $aanvraag->register->naam }})</td>
		<td>@if ($aanvraag->vervolg())Verlenging @else Toelating @endif</td>
		<td>{{ $aanvraag->status->naam }}
			@if ($aanvraag->kritisch() == 1) <span style="color: green;"><i class="fa fa-check"></i></span>
			@elseif ($aanvraag->kritisch() == 2) <span style="color: orange;"><i class="fa fa-check"></i></span>
			@elseif ($aanvraag->kritisch() == 3) <span style="color: red;"><i class="fa fa-times"></i></span>
			@endif
		</td>
		<td>@foreach($aanvraag->register->tc as $toetser){{ $toetser->name }}@if ($toetser->id != $aanvraag->register->tc->last()->id), @endif @endforeach</td>
	</tr>
	@endif
	@endforeach
</table>
</form>
<script>
$(document).ready(function() {
    $('#aanvragenTabel').DataTable({
        "paging":   false,
        "ordering": true,
        "info":     false,
        "language": {
			search: "Zoek in de lijst:",   }
    } );
} );
</script>

@endsection
