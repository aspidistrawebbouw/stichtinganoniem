<!--

Bij een nieuwe aanvraag is $prestatie een nieuwe, lege instantie van Prestatie. Dan is $prestatie->id dus niet gezet.
Bij een opgeslagen $prestatie behorende bij dit criterium bestaat er dus een Tekst ($prestatie->tekst) die we kunnen uitvragen.
-->
	<p><input type="text" name="tekst" value="@if (isset($prestatie->id) && $prestatie->tekst->tekst){{$prestatie->tekst->tekst }}@endif"></input></p>
