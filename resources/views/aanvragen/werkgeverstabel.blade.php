			<table>
			<tr>
				<th>Periode</th>
				<th>Werkgever</th>
				<th>Functie</th>
			</tr>
			
<?php
	/* Hoeveel regels laten we zien?
	 * - als er geen velden ingevuld zijn: $crit->veld1
	 * - als er velden ingevuld zijn, maar minder dan het maximum ($prestatie->werkgevers->count() > 0 en < $crit->veld2 ) dan $prestatie->werkgevers->count() + 1
	 * - alles boven $prestatie->werkgevers->count() + 1 wordt aanvankelijk verborgen door CSS en weer vertoond door JavaScript
	 * - als het maximum aantal velden ingevuld is: precies dat aantal
	 */
 	$j = 1;
	foreach ($prestatie->werkgevers as $werkgever) 
 	{
 	?>
			<tr>
				<td><input type="text" name="periode{{ $j }}" value="{{ $werkgever->periode }}"
					@if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input></td>
				<td><input type="text" name="werkgever{{ $j }}" value="{{ $werkgever->naam }}" 
					@if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input></td>
				<td><input type="text" name="functie{{ $j }}" value="{{ $werkgever->functie }}" 
					@if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input></td>
			</tr>
			<?php $j++; 
	}	?>
			@for ($i = $j; $i <= $crit->veld2; $i++)
			<tr class="werkgRegel" @if ($i > $j && $i > $crit->veld1) style="display: none;" @endif>
				<td><input type="text" name="periode{{ $i }}" 
					@if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input></td>
				<td><input type="text" name="werkgever{{ $i }}" 
					@if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input></td>
				<td><input type="text" name="functie{{ $i }}" 
					@if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input></td>
			</tr>
			@endfor
			</table>
			<button type="button" class="pure-button button-success" id="werkgToev"><i class="fa fa-plus"></i> Regel toevoegen</button>
