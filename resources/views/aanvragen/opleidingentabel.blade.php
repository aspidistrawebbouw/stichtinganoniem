			<table>
			<tr>
				<th>Periode</th>
				<th>Opleiding</th>
				<th>Diploma behaald?</th>
			</tr>
			
<?php
	/* Hoeveel regels laten we zien?
	 * - als er geen velden ingevuld zijn: $crit->veld1
	 * - als er velden ingevuld zijn, maar minder dan het maximum ($prestatie->opleidingen->count() > 0 en < $crit->veld2 ) dan $prestatie->opleidingen->count() + 1
	 * - alles boven $prestatie->opleidingen->count() + 1 wordt aanvankelijk verborgen door CSS en weer vertoon door JavaScript
	 * - als het maximum aantal velden ingevuld is: precies dat aantal
	 */
 	$j = 1;
	foreach ($prestatie->opleidingen as $opleiding) 
 	{
 	?>
			<tr>
				<td><input type="text" name="periode{{ $j }}" value="{{ $opleiding->periode }}" 
					@if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input></td>
				<td><input type="text" name="opleiding{{ $j }}" value="{{ $opleiding->opleiding }}" 
					@if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input></td>
				<td style="text-align: center;"><input type="checkbox" name="diplomabehaald{{ $j }}" 
					@if ($opleiding->diploma) checked @endif 
					@if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input></td>
			</tr>
			<?php $j++; 
	}	?>
			@for ($i = $j; $i <= $crit->veld2; $i++)
			<tr class="oplRegel" @if ($i > $j && $i > $crit->veld1) style="display: none;" @endif>
				<td><input type="text" name="periode{{ $i }}" @if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input></td>
				<td><input type="text" name="opleiding{{ $i }}" @if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input></td>
				<td style="text-align: center;"><input type="checkbox" name="diplomabehaald{{ $i }}" @if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif></input></td>
			</tr>
			@endfor
			</table>
			<button type="button" class="pure-button button-success" id="oplToev"><i class="fa fa-plus"></i> Regel toevoegen</button>
