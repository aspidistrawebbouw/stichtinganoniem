<table class="pure-table citaat">
<tr>
	<th>Periode</th>
	<th>Opleiding</th>
	<th>Diploma<br/>behaald?</th>
</tr>
@foreach ($prestatie->opleidingen as $opleiding)
	<tr>
		<td>{{ $opleiding->periode }}</td>
		<td>{{ $opleiding->opleiding }}</td>
		<td style="text-align: center;">
			@if ($opleiding->diploma)<span style="color: green"><i class="fa fa-check"></i></span>
			@else <span style="color: red"><i class="fas fa-minus"></i></span>
			@endif
		</td>
	</tr>
@endforeach
</table>
