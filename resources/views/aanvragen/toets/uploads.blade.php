			<p>Geuploade documenten</p>
			@foreach ($prestatie->uploads as $upload)
				<p class="citaat" style="width: min-content; white-space:nowrap;"><i class="far fa-file"></i>&nbsp;<a class="citaat" href="{{ Storage::disk('uploads')->url($upload->filenaam) }}" download="{{ $upload->originelefilenaam }}">{{ $upload->originelefilenaam }}</a></p>
				<p>Opmerkingen:</p>
				<p class="citaat">{{ $prestatie->opmerkingen }}</p>
			@endforeach
