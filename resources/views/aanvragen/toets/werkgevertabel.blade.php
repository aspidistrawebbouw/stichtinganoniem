<table class="pure-table citaat">
<tr>
	<th>Periode</th>
	<th>Werkgever</th>
	<th>Functie</th>
</tr>
@foreach ($prestatie->werkgevers as $werkgever)
	<tr>
		<td>{{ $werkgever->periode }}</td>
		<td>{{ $werkgever->naam }}</td>
		<td>{{ $werkgever->functie }}</td>
	</tr>
@endforeach
</table>
