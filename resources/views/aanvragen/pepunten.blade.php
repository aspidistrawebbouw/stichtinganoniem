<p>U heeft voor verlenging van dit certificaat tenminste {{ $crit->veld1 }} PE-punten nodig.</p>
<p><label for="punten">PE-punten behaald in deze periode: </label><input type="number" step="1" min="0" max="{{ $crit->veld2 }}" name="punten"
@if ($prestatie->pepunten)
 value="{{ $prestatie->pepunten->punten }}"
@else 
 value="0"
@endif
@if (isset($prestatie->id) && $prestatie->aanvraag->status->naam != 'opgeslagen') readonly="readonly" @endif
>
</input></p>
