@extends('layouts.app')

@section('content')

<h3>Inloggen</h3>
<fieldset>
<form method="POST" class="pure-form pure-form-stacked" action="{{ route('login') }}">
	@csrf
	<p><label for="email" >E-mailadres:</label>
	<input id="email" type="email" name="email" 
		value="{{ old('email') }}" required autocomplete="email" autofocus>
		@error('email')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	<p><label for="password" class="col-md-4 col-form-label text-md-right">Wachtwoord:</label>
	<input id="password" type="password" name="password" required autocomplete="current-password">
		@error('password')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
		</p>
	<p><label for="remember"></label><input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>&nbsp;Onthoud mij</p>
	<button type="submit" class="btn btn-primary">Inloggen</button>
	<p><a class="btn btn-link" href="{{ route('password.request') }}">Wachtwoord vergeten?</a></p>
</form>
</fieldset>
@endsection
