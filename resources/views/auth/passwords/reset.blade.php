@extends('layouts.app')

@section('content')
<h3>Nieuw wachtwoord opslaan</h3>

<fieldset>
	<form method="POST" action="{{ route('password.update') }}">
		@csrf

		<input type="hidden" name="token" value="{{ $token }}">
<p>
			<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
				<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

				@error('email')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
				@enderror
</p>
	<p>Uw wachtwoord moet tussen de 8 en 30 tekens lang zijn, en tekens bevatten uit tenminste 3 van deze 4 tekensets:</p>
	<ul>
		<li>Hoofdletters A-Z</li>
		<li>Kleine letters a-z</li>
		<li>Cijfers 0-9</li>
		<li>Niet-alfanumerieke tekens als !, $, #, of %</li>
		<!--li>Overige <a href="https://en.wikipedia.org/wiki/List_of_Unicode_characters" target="_blank">Unicode-karakters</a> (bijv. &Aacute, &euml;, &oelig;)</li -->
	</ul>
<p>
			<label for="password" class="col-md-4 col-form-label text-md-right">Nieuw wachtwoord</label>

				<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

				@error('password')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
				@enderror
</p>
<p>
			<label for="password-confirm" class="col-md-4 col-form-label text-md-right">Wachtwoord bevestigen</label>

				<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
</p>
<p>
				<button type="submit" class="pure-button button-success">Wachtwoord opslaan</button>
</p>
	</form>
</fieldset>
@endsection
