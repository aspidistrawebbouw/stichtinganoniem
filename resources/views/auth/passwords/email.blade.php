@extends('layouts.app')

@section('content')

<h3>Wachtwoord vergeten</h3>
@if (session('status'))
	<div class="alert alert-success" role="alert">
		{{ session('status') }}
	</div>
@endif
<p>Als u uw wachtwoord vergeten bent, vul dan hieronder uw email-adres in. U ontvangt dan per mail een link waarmee u uw wachtwoord kunt resetten.</p>
<fieldset>
<form method="POST" class="pure-form pure-form-stacked" action="{{ route('password.email') }}">
    @csrf
	<label class="label-wide"  for="email" ><p>E-mailadres:</p></label>
	<input id="email" type="email" name="email" 
		value="{{ old('email') }}" required autocomplete="email" autofocus>
		@error('email')
			<span class="alert-danger" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
		<button type="submit" class="pure-button button-success">Stuur mij een link</button>
</form>

@endsection
