@extends('layouts.app')

@section('content')

    <fieldset>

        <h3>Bevestig uw email-adres</h3>
        @if (session('resent'))
            <div class="alert alert-success" role="alert">Een verificatie-link is naar uw email-adres gestuurd.</div>
        @endif
        <p>Uw account is bijna gereed. Check uw email: u zou mail van Stichting Anoniem moeten hebben ontvangen. Daarin
            zit een
            link. Als u daarop klikt, is uw email-adres geverifieerd.</p>
        <p>Als u geen email ontvangen heeft, klik dan <a href="{{ route('verification.resend') }}">hier</a> om er
            nogmaals een te laten versturen.</p>

@endsection
