@extends('layouts.app')

@section('content')

<h3>Account aanmaken</h3>
<fieldset>
<form method="POST" class="pure-form pure-form-stacked" action="{{ route('register') }}">
    @csrf
	<label class="label-wide" for="name" ><p>Uw naam:</p></label>
    <input id="name" type="text" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus >
	@error('name')
		<span class="invalid-feedback" role="alert">
			<strong>{{ $message }}</strong>
		</span>
	@enderror
	<label class="label-wide"  for="email" ><p>E-mailadres:</p></label>
	<input id="email" type="email" name="email" 
		value="{{ old('email') }}" required autocomplete="email" autofocus>
		@error('email')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	<p>Uw wachtwoord moet tussen de 8 en 30 tekens lang zijn, en tekens bevatten uit elk van deze 4 tekensets:</p>
	<ul>
		<li>Hoofdletters A-Z</li>
		<li>Kleine letters a-z</li>
		<li>Cijfers 0-9</li>
		<li>Niet-alfanumerieke tekens als !, $, #, of %</li>
		<!--li>Overige <a href="https://en.wikipedia.org/wiki/List_of_Unicode_characters" target="_blank">Unicode-karakters</a> (bijv. &Aacute, &euml;, &oelig;)</li -->
	</ul>
  	<label class="label-wide"  for="password" class="col-md-4 col-form-label text-md-right"><p>Wachtwoord:</p></label>
	<input id="password" type="password" name="password" required autocomplete="current-password"  >
		@error('password')
			<span class="alert-danger" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	<label class="label-wide"  for="password-confirm" ><p>Bevestig wachtwoord:</p></label>
	<input id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password" >
	<button type="submit" class="pure-button button-success">Verzenden</button>
</form>
</fieldset>
@endsection
