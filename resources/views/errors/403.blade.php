@extends('layouts.app')

@section('content')

<h3><strong>Deze actie is niet toegestaan.</strong></h3>

<p>U bent niet geautoriseerd voor deze actie.</p>
@endsection
