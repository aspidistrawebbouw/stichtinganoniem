<!-- 								 	-->
<!-- Tekstblokken van dit register 		-->
<!-- 								 	-->
<div id="tteksten" class="tabcontent">
<h4 id="teksten">Teksten op de informatiepagina</h4>
<fieldset>
	@if (session('tekstError'))
	<div class="alert alert-danger alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ session('tekstError') }}</strong>
	</div>
	@endif
<?php $i = 1; ?>
@foreach ($register->teksten->sortBy('volgnummer') as $tekst)
<div style="width: 100%; height: fit-content; display: flex; flex-direction: row; justify-content: flex-start;">
	<form action="{{ url('/updateTekst') }}" method="POST" class="pure-form" id="tekst{{ $tekst->volgnummer }}">
		@csrf
		<input type="hidden" name="tekst_id" value="{{ $tekst->id }}">
		<input type="hidden" name="register_id" value="{{ $register->id }}">
		<textarea rows="5" name="tekst" id="tekstblok{{ $tekst->volgnummer }}" 
			 @if ($tekst->tekst == "") placeholder="Type hier een alinea tekst" @endif
			style="width: 500px;">{{ $tekst->tekst }}</textarea>
	<script>CKEDITOR.replace('tekstblok{{ $tekst->volgnummer }}');</script>
	
		<p><button class="pure-button button-success"><i class="fa fa-save"></i> Tekst opslaan</button></p>	
	</form>
	<br/>
	@if ($teksten->count() > 1)
	<div>
		@if ($tekst->volgnummer != $teksten->min('volgnummer'))
			<form action="{{ url('/verplaatsTekst') }}" method="POST">
				@csrf
				<input type="hidden" name="tekst_id" value="{{ $tekst->id }}">
				<input type="hidden" name="richting" value="op">
				<button class="pure-button" type="submit" title="Verplaats dit tekstblok naar boven"><i class="fas fa-arrow-up"></i></button>
			</form>
		@else
			<button class="pure-button button-disabled" type="button" ><i class="fas fa-grip-lines"></i></button>
		@endif
	@endif
			<form action="{{ url('/verwijderTekst') }}" method="POST">
				@csrf
				<input type="hidden" name="tekst_id" value="{{ $tekst->id }}">
				<button type="submit" class="pure-button button-disabled" title="Verwijder dit tekstblok"><i class="fas fa-trash"></i></button>					
			</form>
	@if ($teksten->count() > 1)
		@if ($tekst->volgnummer != $teksten->max('volgnummer'))
			<form action="{{ url('/verplaatsTekst') }}" method="POST">
				@csrf
				<input type="hidden" name="tekst_id" value="{{ $tekst->id }}">
				<input type="hidden" name="richting" value="neer">
				<button class="pure-button" type="submit" title="Verplaats dit tekstblok naar beneden"><i class="fas fa-arrow-down"></i></button>
			</form>
		@else
			<button class="pure-button button-disabled" type="button"><i class="fas fa-grip-lines"></i></button>
		@endif
	</div>
	@endif
</div>
	@if (session('successTekst' . $tekst->id))
		<div class="alert alert-success alert-block" style="align-self: baseline;">
			<button type="button" class="close" data-dismiss="alert">×</button>	
			<strong>{{ session('successTekst' . $tekst->id) }}</strong>
		</div>
	@endif
<hr/>
<?php $i++; ?>
@endforeach
<p><form action="{{ url('/voegTekstToe') }}" method="POST">
	@csrf
	<input type="hidden" name="register_id" value="{{ $register->id }}">
	<button class="pure-button button-success"><i class="fas fa-plus"></i> Voeg tekstblok toe</button></form></p>
</fieldset>
</div> <!-- tabblad Teksten -->
