@extends('layouts.app')

@section('content')

<h3>Bevestiging verwijderen register: {{ $register->naam }} ({{ $register->code }})</h3>
<div class="alert alert-danger">
<h4>Pas op!</h4>
<p>U staat op het punt dit register te verwijderen.</p>
</div>
@if ($aanvragen->count() > 0)
<p>Er zijn nog aanvragen voor dit register. Deze zullen ook worden verwijderd:</p>
<table class="pure-table">
	<thead>
		<th>Deelnemer</th>
		<th>Status</th>
		<th>Aangemaakt</th>
		<th>Laatst gewijzigd</th>
	</thead>
	@foreach ($aanvragen as $aanvraag)
	<tr>
		<td>{{ $aanvraag->deelnemer->name }}</td>
		<td>{{ $aanvraag->status->naam }}</td>
		<td>{{ $aanvraag->created_at }}</td>
		<td>{{ $aanvraag->updated_at }}</td>
	</tr>
	@endforeach
</table>
@else
<p>Er zijn geen aanvragen voor dit register.</p>
@endif
@if ($criteria->count() > 0)
<p>Voor dit register zijn de volgende criteria geformuleerd. Deze zullen ook worden verwijderd:</p>
<table class="pure-table">
	<thead>
		<th>Naam</th>
		<th>Beschrijving</th>
		<th>Type</th>
		<th>Laatst gewijzigd</th>
	</thead>
	@foreach ($criteria as $activiteit)
	<tr>
		<td>{{ $activiteit->naam }}</td>
		<td>{{ $activiteit->beschrijving }}</td>
		<td>{{ $activiteit->criteriumType->naam }}</td>
		<td>{{ $activiteit->updated_at }}</td>
	</tr>
	@endforeach
</table>
@else
<p>Er zijn geen criteria voor dit register.</p>
@endif
@if ($register->beheerders->count() > 0)
<p>De volgende personen beheren dit register:</p>
<table class="pure-table">
	<thead>
		<th>Naam</th>
		<th>Email</th>
	</thead>
	@foreach ($register->beheerders as $beheerder)
	<tr>
		<td>{{ $beheerder->name }}</td>
		<td>{{ $beheerder->email }}</td>
	</tr>
	@endforeach
</table>
@else
<p>Er zijn geen beheerders aangewezen voor dit register.</p>
@endif

@if ($register->tc->count() > 0)
<p>Voor dit register bestaat de Toetsingscommissie uit de volgende leden. Deze zullen administratief worden ontheven van hun lidmaatschap:</p>
<table class="pure-table">
	<thead>
		<th>Naam</th>
		<th>Email</th>
	</thead>
	@foreach ($register->tc as $tc)
	<tr>
		<td>{{ $tc->name }}</td>
		<td>{{ $tc->email }}</td>
	</tr>
	@endforeach
</table>
@else
<p>Er is geen Toetsingscommissie bekend voor dit register.</p>
@endif
<div>
	<p>Weet u zeker dat u dit register wilt verwijderen?</p>
	<form action="{{ url('/registerDeleteConfirm/' . $register->id) }}" method="POST" class="pure-form">
		<button class="pure-button button-warning">Ja, ik weet het zeker</button>
		@csrf
	</form>
	<br/><a href="{{ url('/registers/' . $register->id . '/edit?scrollTo=talgemeen') }}"><button class="pure-button button-success">Nee, annuleren</button></a>
</div>
@endsection
