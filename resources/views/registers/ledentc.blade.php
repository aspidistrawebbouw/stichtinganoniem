<!-- 								 	-->
<!-- Leden TC voor dit register 		-->
<!-- 								 	-->
<div id="tledenTC" class="tabcontent">
<h4 id="ledenTc">Leden Toetsingscommissie {{ $register->code }}</h4>
<fieldset>
@if (Auth::user()->isAdmin() || $register->wordtBeheerdDoor(Auth::user()->id ))
	@if ($register->tc->count() > 0)
		<table class="pure-table">
		@foreach ($register->tc as $tc)
			<form action="{{ url('verwijderTc') }}" method="POST">
				@csrf
				<input type="hidden" name="register_id" value="{{ $register->id }}">
				<input type="hidden" name="lidtc" value="{{ $tc->id }}">
				<tr><td>{{ $tc->name }}</td><td>{{ $tc->email }}</td><td><button class="pure-button button-warning"><i class="fa fa-trash"></i> Verwijder</button></td></tr></form> 
		@endforeach
		</table>
	@endif
	<p>Voeg een lid toe:</p>
	<form action="{{ url('/voegTcToe') }}" method="POST">
		<input type="hidden" name="register_id" value="{{ $register->id }}">
		@csrf
		<select name="lidtc">
			@foreach($tcOverig as $tc)
				<option value="{{ $tc->id }}">{{ $tc->name }}</option>
			@endforeach
		</select>
		<button type="submit" class="pure-button button-success"><i class="fa fa-plus"></i> Voeg toe</button>
	</form>
@else
	@if ($register->tc->count() > 0)
		<table class="pure-table">
			@foreach ($register->tc as $tc)
				<tr><td>{{ $tc->name }}</td><td>{{ $tc->email }}</td><td></td></tr>
			@endforeach
		</table>
	@else
		<p>Deze worden door de Administrator toegevoegd.</p>
	@endif
@endif
</fieldset>

</div> <!-- Tabblad Leden TC -->
