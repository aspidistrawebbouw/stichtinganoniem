<!-- 								 	-->
<!-- Beheerders voor dit register 		-->
<!-- 								 	-->
<div id="tbeheerder" class="tabcontent">
<h4>Beheerder(s)</h4>
<fieldset>
@if (Auth::user()->isAdmin() || in_array(Auth::user()->id, $register->beheerders->pluck('id')->toArray() ))
	@if ($register->beheerders->count() > 0)
		<table class="pure-table">
		@foreach ($register->beheerders as $beheerder)
			<form action="{{ url('/verwijderBeheerder') }}" method="POST">
				@csrf
				<input type="hidden" name="register_id" value="{{ $register->id }}">
				<input type="hidden" name="beheerder" value="{{ $beheerder->id }}">
				<tr><td>{{ $beheerder->name }}</td><td>{{ $beheerder->email }}</td><td>
					<button class="pure-button button-warning"><i class="fa fa-trash"></i> Verwijder</button>
				</td></tr>
			</form> 
		@endforeach
		</table>
	@endif
	<p>Voeg een beheerder toe:</p>
	<form action="{{ url('/voegBeheerderToe') }}" method="POST">
		<input type="hidden" name="register_id" value="{{ $register->id }}">
		@csrf
		<select name="beheerder">
			@foreach($beheerdersOverig as $beheerder)
				<option value="{{ $beheerder->id }}">{{ $beheerder->name }}</option>
			@endforeach
		</select>
		<button type="submit" class="pure-button button-success"><i class="fa fa-plus"></i> Voeg toe</button>
	</form>
@else
	@if ($register->beheerders->count() > 0)
		<table class="pure-table">
			@foreach ($register->beheerders as $beheerder)
				<tr><td>{{ $beheerder->name }}</td><td>{{ $beheerder->email }}</td><td></td></tr>
			@endforeach
		</table>
	@else
		<p>Deze worden door de Administrator toegevoegd.</p>
	@endif
@endif
</fieldset>
</div> <!-- tabblad Beheer -->
