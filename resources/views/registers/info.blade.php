@extends('layouts.app')

@section('content')

<?php
use App\Contact;
use App\Register;
?>
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif 
	
</div>
<h2>{{ $register->naam}} ({{ $register->code}})</h2>
<h3>Algemene informatie</h3>
@foreach ($teksten as $tekst)
	<p style="white-space: pre-wrap;">{!! $tekst->tekst !!}</p>
@endforeach

<h3>Criteria voor opname in dit register</h3>
{!! $register->tekstvoortoel !!}
<!-- ol>
	@foreach ($register->toelatingsCriteria()->sortBy('volgnummer') as $crit)
		<li><h5>{{ $crit->naam }}</h5>
		<p>{!! $crit->beschrijving !!}</p>
		</li>
	@endforeach
</ol -->
{!! $register->tekstnatoel !!}
<h3>Criteria voor periodieke verlenging</h3>
{!! $register->tekstvoorverl !!}
<!--ol>
	@foreach ($register->vervolgCriteria()->sortBy('volgnummer') as $crit)
		<li><h5>{{ $crit->naam }}</h5>
		<p>{!! $crit->beschrijving !!}</p>
	</li>
	@endforeach
		</ol-->
{!! $register->tekstnaverl !!}
@if (isset($register->uploads) && $register->uploads->count() > 0)
	<h4>Informatiedocument(en) over dit register</h4>
	<p>Klik om te downloaden.</p>
	@foreach ($register->uploads as $upload)
		<p><i class="fa fa-file"></i> <a href="{{ Storage::disk('uploads')->url($upload->filenaam) }}"
										 download="{{ $upload->originelefilenaam }}">{{ $upload->originelefilenaam }}</a>
		</p>
	@endforeach
@endif
<a href="{{ url('aanvragen/create?register=' . $register->id) }}">
	<button type="button" class="pure-button">Vraag certificaat {{ $register->code }} aan</button>
</a>
<p><i class="fa fa-file"></i> <a href="https://www.stichtinganoniem.nl/resources/downloads/Reg.pdf" target="_blank"
								 download="Reg.pdf">Algemeen Reglement</a></p>
<div class="voettekst">
	<h4>Andere registers:</h4>
	<?php $i = 1; ?>
	<p style="max-width: 100%">@foreach ($alleRegisters as $reg)
			@if ($reg->id != $register->id)<a href="{{ url('registers/' . $reg->id) }}">{{ $reg->code }}</a>
			@if ($i < sizeOf($alleRegisters) - 1) - @endif
	<?php $i++; ?>
	@endif

	@endforeach
</div>
@endsection
