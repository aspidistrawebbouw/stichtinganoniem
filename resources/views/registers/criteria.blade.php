<!-- 								 		-->
<!-- Lijst criteria 		 			 	-->
<!-- 								 		-->
<div id="t{{ $tabTekst }}" class="tabcontent">

<p><strong>Tekst vóór criteria {{ $tabTekst }}</strong></p>
<form class="pure-form" action="{{ url('/registers/' . $register->id . '/edittekst') }}" method="POST">
	@csrf
	<input type="hidden" name="vervolg" value="{{ $vervolg }}"></input>
	<input type="hidden" name="plaats" value="voor"></input>
	<textarea rows="5" columns="200" name="tekstvoor{{ $vervolg }}">@if ($vervolg == 0) {!! $register->tekstvoortoel !!}  @else {!! $register->tekstvoorverl !!} @endif</textarea>
	<script>CKEDITOR.replace('tekstvoor{{ $vervolg }}');</script>
	<p><button type="submit" class="pure-button button-success waarsch">Opslaan</button></p>
</form>

<h4>Criteria voor {{ $tabTekst }}</h4>
<fieldset>
<table class="pure-table">
	<thead><tr>
		<th>Bewerk</th>
		<th>Criterium</th>
		<th>Verwijder</th>
		<th>Verplaats</th>
	</tr></thead><tbody>
<?php $i = 1; ?>
@foreach ($criteria as $crit)
	<tr>
		<td><a href="{{ url('/activiteiten/' . $crit->id . '/edit?register=' . $register->id) }}"><button type="submit" class="pure-button" title="Klik om dit criterium te bewerken"><i class="fas fa-pen"></i></button></a></td>
		<td><p>{{ $i }}.&nbsp;<strong>{{ $crit->naam }}</strong><br/>
		Soort criterium: <em>{{ $crit->criteriumType->naam }}</em></p>
		<p><strong>Beschrijving:</strong></p>
		<div >{!! nl2br($crit->beschrijving) !!}</div>
		<p><strong>Invulinstructie:</strong> {{ $crit->invulinstructie }} <br/>
		@if ($crit->criteriumType->naam == 'PE-punten')
		PE-punten vereist: {{ $crit->veld1 }}, PE-punten maximaal: {{ $crit->veld2 }}
		@endif
			@if (session('successCrit' . $crit->id))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>	
					<strong>{{ session('successCrit' . $crit->id) }}</strong>
				</div>
			@endif
		</td>
		<td><form class="pure-form" action="{{url('activiteiten',$crit->id)}}" method="post" name="deleteForm"> 
			<input type="hidden" name="register_id" value="{{ $register->id }}">
			@method('delete')
			@csrf 
			<button class="pure-button" type="submit" title="Verwijder dit criterium"><i class="fas fa-trash"></i></button>
			</form>
		</td>
		<td>
		@if ($criteria->count() > 1)
			<div>
				@if ($crit->volgnummer != $criteria->min('volgnummer'))
					<form action="{{ url('/verplaatsCrit') }}" method="POST">
						@csrf
						<input type="hidden" name="crit_id" value="{{ $crit->id }}">
						<input type="hidden" name="vervolg" value="{{ $vervolg }}">
						<input type="hidden" name="richting" value="op">
						<button class="pure-button" type="submit" title="Verplaats dit criterium naar boven"><i class="fas fa-arrow-up"></i></button>
					</form>
				@else
					<button class="pure-button button-disabled" type="button" title="Dit is het eerste criterium"><i class="fas fa-grip-lines"></i></button>
				@endif
			@endif
			@if ($criteria->count() > 1)
				@if ($crit->volgnummer != $criteria->max('volgnummer'))
					<form action="{{ url('/verplaatsCrit') }}" method="POST">
						@csrf
						<input type="hidden" name="crit_id" value="{{ $crit->id }}">
						<input type="hidden" name="vervolg" value="{{ $vervolg }}">
						<input type="hidden" name="richting" value="neer">
						<button class="pure-button" type="submit" title="Verplaats dit criterium naar beneden"><i class="fas fa-arrow-down"></i></button>
					</form>
				@else
					<button class="pure-button button-disabled" type="button" title="Dit is het laatste criterium"><i class="fas fa-grip-lines"></i></button>
				@endif
			</div>
			@endif
	</td>
	</tr>
	<?php $i++; ?>
@endforeach	
</thead>
</table>
<p><a href="{{ url('/activiteiten/create?register=' . $register->id . "&vervolg=" . $vervolg) }}"><button class="pure-button button-success"><i class="fas fa-plus"></i> Voeg criterium voor {{ $tabTekst }} toe</button></a></p>
</fieldset>

<p><strong>Tekst na criteria {{ $tabTekst }}</strong></p>
<form class="pure-form" action="{{ url('/registers/' . $register->id . '/edittekst') }}" method="POST">
	@csrf
	<input type="hidden" name="vervolg" value="{{ $vervolg }}"></input>
	<input type="hidden" name="plaats" value="na"></input>
	<textarea rows="5" columns="200" name="tekstna{{ $vervolg }}">@if ($vervolg == 0) {!! $register->tekstnatoel !!} @else {!! $register->tekstnaverl !!}@endif</textarea>
	<script>CKEDITOR.replace('tekstna{{ $vervolg }}');</script>
	<p><button type="submit" class="pure-button button-success waarsch">Opslaan</button></p>
</form>

</div> <!-- tabblad Criteria (toelating of verlenging) -->
