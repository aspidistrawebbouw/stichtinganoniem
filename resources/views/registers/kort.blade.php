@extends('layouts.app')

@section('content')

<?php
use App\Contact;
use App\Register;
?>
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif 
	
</div>
<fieldset>
<h2>{{ $register->naam}} ({{ $register->code}})</h2>
@foreach ($teksten as $tekst)
	<p style="white-space: pre-wrap;">{!! $tekst->tekst !!}</p>
@endforeach
<p>
	<a href="{{ url('/registers/' . $register->id) }}"><button type="button" class="pure-button">Meer informatie over {{ $register->code }}</button></a>
	@if (Auth::user()->isDeelnemer())
		<a href="{{ url('aanvragen/create?register=' . $register->id) }}"><button type="button" class="pure-button">Vraag certificaat {{ $register->code }} aan</button></a>
	@endif
</p>
</fieldset>
<p>
	Zie ook het Algemeen Reglement van Stichting Anoniem, dat u hier kunt downloaden:
</p>
<p><i class="fa fa-file"></i> <a href="https://www.stichtinganoniem.nl/resources/downloads/Reg.pdf" target="_blank"
								 download="Reg.pdf">Algemeen Reglement</a></p>
</div>
@endsection
