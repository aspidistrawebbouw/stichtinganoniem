@extends('layouts.app')

@section('content')

<?php 
use App\Register;
?>
<h3>Register: @if (isset($register)) {{ $register->naam }} ({{ $register->code }})@endif</h3>
<div id="app">
	@include('flash-message')
	@yield('content')
</div>
<?php 
if (isset($register)) { ?>
	<p>Zie ook de <a href="{{ url('registers/' . $register->id) }}">Informatiepagina</a> over dit register.</p>
<?php
	$toelatingsCriteria = $register->toelatingsCriteria()->sortBy('volgnummer'); 
	$vervolgCriteria = $register->vervolgCriteria()->sortBy('volgnummer');
} else
	$register = new Register();
?>

<div class="tab">
  <button class="tablinks" onclick="openTab('talgemeen')">Algemeen</button>
  <button class="tablinks" onclick="openTab('ttoelating')">Toelating</button>
  <button class="tablinks" onclick="openTab('tverlenging')">Verlenging</button>
  <button class="tablinks" onclick="openTab('tbeheerder')">Beheerder</button>
  <button class="tablinks" onclick="openTab('tledenTC')">Leden TC</button>
  <button class="tablinks" onclick="openTab('tteksten')">Teksten</button>
  <button class="tablinks" onclick="openTab('tdocumenten')">Documenten</button>
  <button class="tablinks" onclick="openTab('tstatistieken')">Statistieken</button>
</div>

<!-- Tabbladen 							-->
@include ('registers.algemeen')
@include ('registers.criteria', ['criteria' => $toelatingsCriteria, 'tabTekst' => 'toelating', 'vervolg' => '0'])
@include ('registers.criteria', ['criteria' => $vervolgCriteria, 'tabTekst' => 'verlenging', 'vervolg' => '1'])
@include ('registers.beheer')
@include ('registers.ledentc')
@include ('registers.teksten')
@include ('registers.documenten')
@include ('registers.statistieken')

<!-- 								 	-->
<!-- Navigatie 					 		-->
<!-- 								 	-->

<p><a href="{{ url('/registers') }}"><button class="pure-button button-secondary"><i class="fas fa-arrow-left"></i> Terug naar lijst Registers</button></a></p>
@if (isset($register->id) && $register->id != 0)
<form id="verwijderen" method="POST" action = "{{ url('registers/' . $register->id) }}">
	@csrf
	@method('DELETE')
	<p><button class="pure-button button-warning"><i class="fas fa-trash"></i> Register "{{ $register->naam }}" verwijderen</button></p>
</form>
@endif
<p class="scrollTo">{{ $scrollTo }}</p>
<script>
$( document ).ready(function() {
		var element = "{{ $scrollTo }}";
		openTab(element);
	});
</script>

@endsection
