<!-- 								 	-->
<!-- Stamgegevens van dit register 		-->
<!-- 								 	-->
<div id="talgemeen" class="tabcontent">
    <form class="pure-form" action="{{ $action }}" method="POST" enctype="multipart/form-data">
        <h4 id="talgemeen">Algemene eigenschappen van dit register</h4>
        @csrf
        <?php
        setlocale(LC_ALL, 'nl_NL.utf8');
        if (isset($register->id) && $register->id != 0)
        { ?>
        <input type="hidden" name="_method" value="PUT">
        <?php } else {
            if (!isset($register->id)) {
                $register = new App\Register();
            }
        } ?>
        <fieldset>
            <p>Velden met * zijn verplichte velden.</p>
            <p>
                <label for="code">Code*:</label> <input type='text' id='code' name='code' required
                                                        value='{{$register->code}}' style="width: 120px;"/>
            @if ($errors->has('code'))
                <div class="alert alert-danger">{{ $errors->first('code') }}</div>
                @endif
                </p>
                <p>
                    <label for="name">Naam*:</label>
                    <input class="focushier" type='text' id='naam' name='naam' required="required"
                           value='{{$register->naam}}' style="width: 500px;"/>
                @if ($errors->has('naam'))
                    <div class="alert alert-danger">{{ $errors->first('naam') }}</div>
                    @endif
                    </p>
                    <p>
                        <label for="actief">Actief (alleen actieve registers staan in de lijst die voor deelnemers
                            zichtbaar is):</label>
                        <input type='radio' id='actiefja' name='actief' value="J"
                               @if ($register->actief == 'J') checked @endif > Ja</input>
                        <input type='radio' id='actiefnee' name='actief' value="N"
                               @if ($register->actief != 'J') checked @endif > Nee</input>
                    </p>
                    <p>
                        <label for="geldig">Geldigheidsduur:</label>
                        <select name="geldig" id="geldig">
                            <option value="3" @if ($register->geldig == 3) selected @endif>3 maanden</option>
                            <option value="6" @if ($register->geldig == 6) selected @endif>6 maanden</option>
                            <option value="9" @if ($register->geldig == 9) selected @endif>9 maanden</option>
                            <option value="12" @if ($register->geldig == 12) selected='selected' @endif>1 jaar</option>
                            <option value="18" @if ($register->geldig == 18) selected @endif>1,5 jaar</option>
                            <option value="24" @if ($register->geldig == 24) selected @endif>2 jaar</option>
                            <option value="30" @if ($register->geldig == 30) selected @endif>2,5 jaar</option>
                            <option value="36" @if ($register->geldig == 36) selected @endif>3 jaar</option>
                            <option value="48" @if ($register->geldig == 48) selected @endif>4 jaar</option>
                            <option value="60" @if ($register->geldig == 60) selected @endif>5 jaar</option>
                        </select>
                    </p>
                    <p>
                        <button class="pure-button button-success"><i class="fa fa-save"></i> Opslaan</button>
                    </p>
        </fieldset>
    </form>
    <p>
    @if ($errors->has('naam'))
        <div class="alert alert-danger">{{ $errors->first('naam') }}</div>
        @endif
        </p>
</div> <!-- tabblad Algemeen -->
