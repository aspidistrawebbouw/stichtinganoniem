@extends('layouts.app')

@section('content')

<h3>Alle Registers</h3>
@foreach ($registers as $register)
<fieldset>
<h4>{{ $register->code }} {{ $register->naam }}</h4>
<?php $teksten = $register->teksten; ?>
@foreach ($teksten as $tekst)
<p>{!! $tekst->tekst !!}</p>
@endforeach
<p><a href="{{ url('/registers/' . $register->id) }}"><button type="button" class="pure-button">Meer informatie over {{ $register->code }}</button></a>
@if (Auth::user()->isDeelnemer())
 <a href="{{ url('aanvragen/create?register=' . $register->id) }}"><button type="button" class="pure-button">Vraag certificaat {{ $register->code }} aan</button></a></p>
@endif
</fieldset>
<p>&nbsp;</p>
@endforeach
@endsection
