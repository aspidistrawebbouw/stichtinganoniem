<!-- 								 	-->
<!-- Statistieken voor dit register		-->
<!-- 								 	-->
@php
use App\Aanvraag;
@endphp

<div id="tstatistieken" class="tabcontent">
<h4>Statistieken</h4>
<h5>De volgende personen zijn gecertificeerd:</h5>
<table class="pure-table">
	<tr>
		<th>Deelnemer</th>
		<th>Datum goedkeuring</th>
		<th>Geldig tot</th>
		<th>Goedgekeurd door</th>
	</tr>
	@foreach (Aanvraag::where('register_id', $register->id)->where('aanvraagStatus', 3)->get() as $cert)
	<tr>
		<td><a href="{{ url('/aanvragen/' . $cert->id . '/edit') }}">{{ $cert->deelnemer->name }}</a></td>
		<td>{{ strftime("%e %B %Y", strtotime($cert->goedgekeurd)) }}</td>
		<td>{{ strftime("%e %B %Y", strtotime($cert->goedgekeurd . " + " . $cert->register->geldig . " months - 1 days")) }}</td>
		<td>{{ $cert->goedkeurder->name }}</td>
	</tr>
	@endforeach
</table>
<h5>De volgende personen hebben een aanvraag tot toelating opgeslagen:</h5>
<table class="pure-table">
	<tr>
		<th>Deelnemer</th>
		<th>Datum laatste wijziging</th>
	</tr>
	@foreach (Aanvraag::where('register_id', $register->id)->where('aanvraagStatus', 1)->get() as $cert)
		@if (!$cert->vervolg())
			<tr>
				<td><a href="{{ url('/aanvragen/' . $cert->id . '/edit') }}">{{ $cert->deelnemer->name }}</a></td>
				<td>{{ strftime("%e %B %Y", strtotime($cert->updated_at)) }}</td>
			</tr>
		@endif
	@endforeach
</table>
<h5>De volgende personen hebben een aanvraag tot verlenging opgeslagen:</h5>
<table class="pure-table">
	<tr>
		<th>Deelnemer</th>
		<th>Datum laatste wijziging</th>
		<th>Huidig certificaat<br/>is geldig tot</th>
	</tr>
	@foreach (Aanvraag::where('register_id', $register->id)->where('aanvraagStatus', 1)->get() as $cert)
		@if ($cert->vervolg())
@php
	$geldigTot = Aanvraag::where('user_id', $cert->user_id)->where('register_id', $register->id)->where('aanvraagStatus', 3)->max('goedgekeurd');
@endphp
			<tr>
				<td><a href="{{ url('/aanvragen/' . $cert->id . '/edit') }}">{{ $cert->deelnemer->name }}</a></td>
				<td>{{ strftime("%e %B %Y", strtotime($cert->updated_at)) }}</td>
				<td>{{ strftime("%e %B %Y", strtotime($geldigTot . " + " . $cert->register->geldig . " months - 1 days")) }}</td>
			</tr>
		@endif
	@endforeach
</table>
<h5>De volgende personen hebben een aanvraag tot toelating ingediend:</h5>
<table class="pure-table">
	<tr>
		<th>Deelnemer</th>
		<th>Datum ingediend</th>
	</tr>
	@foreach (Aanvraag::where('register_id', $register->id)->where('aanvraagStatus', 2)->get() as $cert)
		@if (!$cert->vervolg())
			<tr>
				<td><a href="{{ url('/aanvragen/' . $cert->id . '/edit') }}">{{ $cert->deelnemer->name }}</a></td>
				<td>{{ strftime("%e %B %Y", strtotime($cert->ingediend)) }}</td>
			</tr>
		@endif
	@endforeach
</table>
<h5>De volgende personen hebben een aanvraag tot verlenging ingediend:</h5>
<table class="pure-table">
	<tr>
		<th>Deelnemer</th>
		<th>Datum ingediend</th>
	</tr>
	@foreach (Aanvraag::where('register_id', $register->id)->where('aanvraagStatus', 2)->get() as $cert)
		@if ($cert->vervolg())
			<tr>
				<td><a href="{{ url('/aanvragen/' . $cert->id . '/edit') }}">{{ $cert->deelnemer->name }}</a></td>
				<td>{{ strftime("%e %B %Y", strtotime($cert->ingediend)) }}</td>
			</tr>
		@endif
	@endforeach
</table>
</div> <!-- tabblad Statistieken -->
