<!-- 								 	-->
<!-- Documenten bij dit register 		-->
<!-- 								 	-->
<div id="tdocumenten" class="tabcontent">
<h4 id="documenten">Documenten op de informatiepagina</h4>
<fieldset>
	@if (session('documentError'))
		<div class="alert alert-danger alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>	
			<strong>{{ session('documentError') }}</strong>
		</div>
	@endif
	<form action="{{ url('/registers/' . $register->id . '/beheerDocument') }}" method="POST"  enctype="multipart/form-data">
	@csrf
		<p>Documenten hier geupload komen als download-link op de informatiepagina van dit register. Het bestand moet een PDF-document zijn of een afbeelding (jpg, png, etc.). De maximale upload-grootte is 50Mb.</p>
		<input type="file" name="logo" accept="image/*,.pdf" accept-charset="UTF-8" enctype="multipart/form-data"></input>
		@if (isset($register->uploads) && $register->uploads->count() > 0)
			<p>Geuploade documenten</p>
			<table class="pure-table">
				<thead><tr><th>Titel</th><th><i class="far fa-trash-alt"></th></i></thead>
			@foreach ($register->uploads as $upload)
				<tr>
					<td><i class="fa fa-file"></i> <a href="{{ Storage::disk('uploads')->url($upload->filenaam) }}" download="{{ $upload->originelefilenaam }}">{{ $upload->originelefilenaam }}</a></td>
					<td><input type="checkbox" name="doc{{ $upload->id }}" value="1"></input></td></a>
				</tr>
			@endforeach
			</table>
		@endif
		@if (session('documentSuccess'))
			<div class="alert alert-success alert-block" style="align-self: baseline;">
				<button type="button" class="close" data-dismiss="alert">×</button>	
				<strong>{{ session('documentSuccess') }}</strong>
			</div>
		@endif
	<p><button class="pure-button button-success">Wijzigingen opslaan</button></form></p>
	</form>
</fieldset>
</div> <!-- Tabblad Documenten -->
