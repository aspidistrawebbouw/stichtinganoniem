@extends('layouts.app')

@section('content')

<?php
use App\Contact;
use App\Register;
use App\Aanvraag;
?>
<h3>Beheer Registers</h3>
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif 
	
</div>
<p><a href="{{ url('registers/create')}}"><button class="pure-button button-success"><i class="fas fa-plus"></i> Maak nieuw register</button></a></p>
<table id="registerTabel" class="pure-table">
	<thead>
	<th>Code</th>
	<th>Naam</th>
	<th>Beheerder(s)</th>
	<th>Aanvragen</th>
	<th>Aanvragen ter beoordeling</th>
	<th>Info-<br/>tekst<br/>ingevoerd</th>
	<th>Info-<br/>pagina</th>
	<th>Aantal<br/>criteria<br/>toelating</th>
	<th>Aantal<br/>criteria<br/>verlenging</th>
	<th>Aantal leden<br>TC</th>
	<th>Actief</th>
	<th>Verwijder</th>
	</thead>
	@foreach ($registers as $register)
	<tr>
		<td><a href="{{ url('/registers/' . $register->id . '/edit') }}">{{ $register->code }}</a></td>
		<td>{{ $register->naam }}</td>
		<td>@foreach($register->beheerders as $beheerder){{ $beheerder->name }}@if ($beheerder->id != $register->beheerders->last()->id), @endif @endforeach</td>
		<td style="text-align: center;">{{ Aanvraag::where('register_id', $register->id)->count() }}</td>
		<td style="text-align: center;">{{ Aanvraag::where('register_id', $register->id)->where('aanvraagStatus', 2)->count() }}</td>
		<td style="text-align: center;">@if (sizeof($register->teksten) > 0)<i class="fas fa-check text-success"></i>@else<i class="fas fa-minus text-danger"></i>@endif</td>
		<td style="text-align: center;"><a href="{{ url('registers/' . $register->id) }}"><i class="fas fa-info"></i></a></td>
		<td style="text-align: center;">{{ $register->criteria->where('vervolg', 0)->count() }}</td>
		<td style="text-align: center;">{{ $register->criteria->where('vervolg', 1)->count() }}</td>
		<td style="text-align: center;">{{ $register->tc->count() }}</td>
		<td style="text-align: center;">@if ($register->actief == 'J')<i class="fas fa-check text-success"></i>@else<i class="fas fa-minus text-danger"></i>@endif</td>
		<td><form action="{{url('registers',$register->id)}}" method="post" name="deleteForm"> 
			@method('delete')
			@csrf 
			<button type="submit"><i class="fas fa-trash"></i></button>
			</form>
		</td>
		</tr>
	@endforeach
</table>
</form>
@endsection
