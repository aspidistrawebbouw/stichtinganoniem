@extends('layouts.app')

@section('content')


<h2>Welkom op het extranet van Stichting Anoniem</h2>

<p>Heeft u al een account? <a href="{{ route('login') }}">Log dan in</a>.</p>
<p>Nog geen account? <a href="{{ route('register') }}">Maak er dan één aan.</a></p>

@endsection
