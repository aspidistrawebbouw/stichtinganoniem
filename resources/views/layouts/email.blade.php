<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Stichting Anoniem</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed:700" rel="stylesheet">
</head>
<body style="margin: 0; padding: 0;">
<style>body {
        font-family: 'Roboto', sans serif;
    }</style>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600px"
                   style="border-collapse: collapse;">
                <tr>
                    <td style="background-color: #3f4a54;height: 30px; padding: 20px; ">
                        <span style="font-family: Times New Roman; font-weight: bold; font-size: 30px; color: #FFFFFF;">Stichting Anoniem</span>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #ffffff">
                        @yield('content')
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #3f4a54; padding: 20px; font-size: 14px; color: #FFFFFF;">
                        &copy; {{ date('Y') }} Stichting Anoniem
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
