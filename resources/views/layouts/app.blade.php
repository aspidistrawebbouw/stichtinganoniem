<?php use App\Register;

use Illuminate\Support\Facades\App;
use App\Register as MyRegister;

?>
		<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Stichting Anoniem Extranet">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link rel="stylesheet" href="{{ asset('css/pure-min.css') }}">
<!-- link href="{{ asset('css/baby-blue.css') }}" rel="stylesheet" -->

	<!--[if lte IE 8]>
	<link rel="stylesheet" href="css/layouts/side-menu-old-ie.css">
	<![endif]-->
	<!--[if gt IE 8]><!-->
<!-- link rel="stylesheet" href="{{ asset('css/layouts/side-menu.css') }}" -->
	<link rel="stylesheet" href="{{ asset('css/layouts/top-menu.css') }}">
	<!--<![endif]-->

	<!--[if lte IE 8]>
	<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/grids-responsive-old-ie-min.css">
	<![endif]-->
	<!--[if gt IE 8]><!-->
	<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/grids-responsive-min.css">
	<!--<![endif]-->

	<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
	<link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
<!-- link href="{{ asset('ckeditor/contents.css') }}" rel="stylesheet" -->

	<!-- Scripts -->
	<script src="{{ asset('js/app.js') }}"></script>
	<script src="{{ asset('js/custom.js') }}" defer></script>
	<script src="https://kit.fontawesome.com/f0238dfa97.js"></script>
	<script src="{{ asset('js/datatables.min.js') }}"></script>
	<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed:700" rel="stylesheet">

</head>
<body>
<div id="layout">
	<div class="header">
		<a href="{{ url('/home') }}" style="display:inline-block; height: 100%; width: 100%;"> <img
					class="pure-img headerimg" src="{{ asset('images/large_8594.jpg') }}"></a>
	</div>

	<div class="custom-wrapper pure-g" id="menu">
		<div id="hamburger">&nbsp;
			<a href="#" class="custom-toggle" id="toggle"><s class="bar"></s><s class="bar"></s><s class="bar"></s></a>
		</div>
		<div class="pure-u-1 pure-u-md-1-3">
			<div class="pure-menu pure-menu-horizontal custom-can-transform">
				<ul class="pure-menu-list">
					@auth
						@if(auth()->user()->hasVerifiedEmail())
							<li class="pure-menu-item @if (Route::currentRouteName() == 'home') pure-menu-selected @endif">
								<a class="pure-menu-link" href="{{ url('/home') }}">Home</a></li>
							<li class="pure-menu-item pure-menu-has-children pure-menu-allow-hover @if (Route::currentRouteName() == 'reglijst') pure-menu-selected @endif">
								<a class="pure-menu-link" href="#">Registers</a>
								<ul class="pure-menu-children">
									@foreach(MyRegister::where('actief', 'J')->orderBy('naam')->get() as $register)
										<li class="pure-menu-item"><a
													href="{{ url('registers/' . $register->id . '/kort') }}">{{ $register->naam }}</a>
										</li>
									@endforeach
								</ul>
							</li>
							@if (Auth::user()->isAdmin() || Auth::user()->isBeheerder())
								<li class="pure-menu-item @if (Route::currentRouteName() == 'registers.index') pure-menu-selected @endif">
									<a class="pure-menu-link" href="{{ url('/registers') }}">Beheer Registers</a></li>
							@endif
							@if (Auth::user()->isAdmin())
								<li class="pure-menu-item @if (Route::currentRouteName() == 'personen.index') pure-menu-selected @endif">
									<a class="pure-menu-link" href="{{ url('/personen') }}">Beheer Personen</a></li>
							@endif
							@if (Auth::user()->isAdmin() || Auth::user()->isBeheerder() || Auth::user()->isTC())
								<li class="pure-menu-item @if (Route::currentRouteName() == 'aanvragen.index') pure-menu-selected @endif">
									<a class="pure-menu-link" href="{{ url('/aanvragen') }}">Beheer Aanvragen</a></li>
							@endif
							<li class="pure-menu-item @if (Route::currentRouteName() == 'messages') pure-menu-selected @endif">
								<a class="pure-menu-link" href="{{ url('/messages') }}">Berichten</a></li>
							<li class="pure-menu-item @if (Route::currentRouteName() == 'personen.edit' && Route::current()->parameters['personen'] == Auth::user()->id) pure-menu-selected @endif">
								<a class="pure-menu-link" href="{{ url('/personen/' . Auth::user()->id . '/edit') }}">Mijn
									profiel</a></li>
						@endif
					@endauth
					@guest
						<li class="pure-menu-item"><a class="pure-menu-link" href="{{ route('login') }}">Inloggen</a>
						</li>
						<li class="pure-menu-item"><a class="pure-menu-link" href="{{ route('register') }}">Account
								aanmaken</a></li>
					@endguest
					@auth
						@if(auth()->user()->hasVerifiedEmail())
							<li class="pure-menu-item"><a class="pure-menu-link dropdown-item"
														  href="{{ route('logout') }}"
														  onclick="event.preventDefault();
											 document.getElementById('logout-form').submit();">Uitloggen</a></li>
						@endif
					@endauth
				</ul>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
			</div>
		</div>
	</div>
	<div id="main">
		<div class="content">
			@yield('content')
		</div>
	</div>
	<div class="footer">&copy; {{ date('Y') }} Stichting Anoniem. Vragen over deze applicatie? Mail naar <a
				href="mailto:anoniem@aspidistra.nl">anoniem@aspidistra.nl</a>.
	</div>
</div>
<!-- script src="{{ asset('js/ui.js') }}"></script -->
<script src="{{ asset('js/menu.js') }}"></script>
</body>
</html>
