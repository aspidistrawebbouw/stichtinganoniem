@extends('layouts.app')

@section('content')

    <?php
    use App\Aanvraag;
    use App\User;
    setlocale(LC_ALL, 'nl_NL.utf8');
    ?>
    @if (Auth::user()->isDeelnemer())
        <p>Welkom deelnemer {{ Auth::user()->name}}!</p>
    @elseif (Auth::user()->isBeheerder())
        <p>Welkom beheerder {{ Auth::user()->name}}!</p>
    @elseif (Auth::user()->isTC())
        <p>Welkom lid Toetsingscommissie {{ Auth::user()->name}}!</p>
    @elseif (Auth::user()->isAspirantDeelnemer())
        <p>Welkom Aspirant-deelnemer {{ Auth::user()->name}}!</p>
    @elseif (Auth::user()->isAdmin())
        <p>Welkom Admin {{ Auth::user()->name}}!</p>
    @endif

    <div id="app">
        @include('flash-message')
        @yield('content')
    </div>
    @if (Auth::user()->isDeelnemer())
        <!-- nieuws voor een deelnemer -->
        <p>Dit is uw persoonlijke registeromgeving.</p>
        @if ($goedgekeurd->count() > 0)
            <h4>Uw certificaten:</h4>
            @foreach ($goedgekeurd as $cert)
                <fieldset>
                    <?php if (!User::find ($cert->goedgekeurd_door)) $goedgekeurd_placeholder = "Bureau Stichting Anoniem"; else $goedgekeurd_placeholder = User::find ($cert->goedgekeurd_door)->name; ?>
                    <p><i class="fas fa-medal"></i> <strong>{{ $cert->register->naam }} ({{ $cert->register->code }}
                            )</strong><br/> toegekend op {{ strftime("%e %B %Y", strtotime($cert->goedgekeurd)) }}
                        door {{ $goedgekeurd_placeholder }}.</p>
                    <p>Geldig
                        tot: {{ strftime("%e %B %Y", strtotime($cert->goedgekeurd . " + " . $cert->register->geldig . " months - 1 days")) }}
                        @if ($cert->kritisch() == 2)<span style="color: orange;"><i
                                    class="fas fa-exclamation-circle"></i> Deze certificatie verloopt binnenkort.</span>
                        <br/>
                        @elseif ($cert->kritisch() == 3)<span style="color: red;"><i
                                    class="fas fa-exclamation-circle"></i> Deze certificatie is verlopen.</span><br/>
                        @endif            @if (Aanvraag::where('user_id', Auth::user()->id)->where('register_id', $cert->register->id)->where('aanvraagStatus', 1)->count() == 1)
                    </p>
                    <p>U heeft een aanvraag tot verlenging opgeslagen. Klik <a
                                href="{{ url('/aanvragen/' . Aanvraag::where('user_id', Auth::user()->id)->where('register_id', $cert->register->id)->where('aanvraagStatus', 1)->first()->id . '/edit') }}">hier</a>
                        om deze te bewerken.</p>
                    @elseif (Aanvraag::where('user_id', Auth::user()->id)->where('register_id', $cert->register->id)->where('aanvraagStatus', 2)->count() == 1)
                        <p>U heeft een aanvraag tot verlenging van deze certificatie ingediend (zie hieronder).</p>
                    @else
                        <p><a href="{{ url('aanvragen/create?register=' . $cert->register->id . '&vervolg=1') }}">
                                <button type="button" class="pure-button">Vraag verlenging {{ $cert->register->code }}
                                    aan
                                </button>
                            </a></p>
                    @endif
                </fieldset>
            @endforeach
        @endif
        <h4>Uw aanvragen:</h4>
        @if ($ingediend->count() > 0)
            <p>U heeft voor de/het volgende register(s) een aanvraag ter beoordeling ingediend:</p>
            <table class="pure-table">
                <thead>
                <tr>
                    <th>Register</th>
                    <th>Datum ingediend</th>
                    <th>Dit is een:</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($ingediend as $aanvraag)
                    <tr>
                        <td>
                            <a href="{{ url('/aanvragen/' . $aanvraag->id . '/edit') }}">{{ $aanvraag->register->code }}</a>
                            ({{ $aanvraag->register->naam }})
                        </td>
                        <td>{{ $aanvraag->ingediend }}</td>
                        <td>@if ($aanvraag->vervolg())Verlenging @else Eerste aanvraag @endif</td>
                    </tr>
                @endforeach
            </table>
        @else
            <p>U heeft nog geen aanvraag ingediend.</p>
        @endif
        @if ($opgeslagen->count() > 0)
            <p>U heeft voor de/het volgende register(s) een aanvraag opgeslagen:</p>
            <table class="pure-table">
                <thead>
                <tr>
                    <th>Register</th>
                    <th>Naam</th>
                    <th>Laatst<br/>gewijzigd</th>
                    <th style="text-align: center;">Aantal criteria<br/>ingevuld</th>
                    <th style="text-align: center;">Aantal criteria<br/>nodig</th>
                    <th>Dit is een:</th>
                    <th>Opmerking</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($opgeslagen as $aanvraag)
                    <tr>
                        <td>
                            <a href="{{url('/aanvragen/' . $aanvraag->id . '/edit') }}">{{ $aanvraag->register->code }}</a>
                        </td>
                        <td>{{ $aanvraag->register->naam }}</td>
                        <td>{{ $aanvraag->opgeslagen }}</td>
                        <td style="text-align: center;">{{ $aanvraag->prestaties->count() }}</td>
                        <td style="text-align: center;">{{ $aanvraag->register->criteria()->where('vervolg', $aanvraag->vervolg())->count() }}</td>
                        <td>@if ($aanvraag->vervolg())Verlenging @else Eerste aanvraag @endif</td>
                        <td>@if ($aanvraag->afgekeurd)Deze aanvraag is afgekeurd
                            op {{ strftime("%e %B %Y", strtotime($aanvraag->afgekeurd)) }}@endif</td>
                    </tr>
                @endforeach
            </table>
        @else
            <p>U heeft geen aanvraag opgeslagen.</p>
        @endif
        <h4>Nieuwe registratie</h4>
        <p>Vraag hier registratie aan in één van de volgende registers:</p>
        <table class="pure-table">
            @foreach ($alleRegisters as $reg)
                @if (Aanvraag::where('user_id', Auth::user()->id)->where('register_id', $reg->id)->get()->isEmpty())
                    <tr>
                        <td><a href="{{url('/aanvragen/create?register=' . $reg->id) }}">{{ $reg->code }}</a></td>
                        <td>{{ $reg->naam }}</td>
                    </tr>
                    @endif
                    @endforeach
                    </tbody>
        </table>
    @endif
    @if (Auth::user()->isAdmin())
        <h3>Administrator</h3>
        <h4>Aspirant-deelnemers</h4>
        @if ($aspirantDeelnemers->count() > 0)
            <p>Er zijn aspirant-deelnemers.</p>
            <table class="pure-table">
                <thead>
                <tr>
                    <th>Naam</th>
                    <th>Email</th>
                    <th>Aangemeld</th>
                </tr>
                </thead>
                @foreach ($aspirantDeelnemers as $aspirant)
                    <tr>
                        <td><a href="{{ url('/personen/' . $aspirant->id . '/edit') }}">{{ $aspirant->name }}</a></td>
                        <td>{{ $aspirant->email }}</td>
                        <td>{{ $aspirant->created_at }}</td>
                    </tr>
                @endforeach
            </table>
        @else
            <p>Er zijn momenteel geen aspirant-leden.</p>
        @endif
    @endif
    @if (Auth::user()->isAdmin() || Auth::user()->isBeheerder())
        <!-- nieuws voor een beheerder -->
        <h3>Beheerder</h3>
        <?php
        if (Auth::user()->isAdmin()) $aanvragen = Aanvraag::where('aanvraagStatus', 2)->get();
        else {
            $registersInBeheer = Auth::user()->registersInBeheer->pluck('id');
            $aanvragen = Aanvraag::where('aanvraagStatus', 2)->get()->whereIn('register_id', $registersInBeheer);
        }
        ?>
        @if (Auth::user()->registersInBeheer->count() > 0)
            <p>U hebt de volgende registers in beheer:</p>
            <table class="pure-table">
                <thead>
                <th>Info-<br/>pagina</th>
                <th>Beheer</th>
                <th>Code</th>
                <th>Naam</th>
                </thead>
                @foreach(Auth::user()->registersInBeheer as $reg)
                    <tr>
                        <td style="text-align: center;"><a href="{{ url('/registers/' . $reg->id) }}"><i
                                        class="fas fa-info"></i></a></td>
                        <td style="text-align: center;"><a href="{{ url('/registers/' . $reg->id . '/edit') }}"><i
                                        class="fas fa-pen"></i></a></td>
                        <td>{{ $reg->code }}</td>
                        <td>{{ $reg->naam }}</td>
                    </tr>
                @endforeach
            </table>
        @else
            <p>U heeft momenteel geen registers in beheer.</p>
        @endif

        @if ($aanvragen->count() > 0)
            <p>Er zijn aanvragen ingediend voor één van de door u beheerde registers:</p>
            <table class="pure-table">
                @foreach ($aanvragen as $aanvraag)
                    <tr>
                        <td><a href="{{ url('/aanvragen/' . $aanvraag->id) }}">{{ $aanvraag->deelnemer->name }}</a></td>
                        <td>{{ $aanvraag->register->naam }} ({{ $aanvraag->register->code }})</td>
                    </tr>
                @endforeach
            </table>
        @else
            <p>Er zijn geen openstaande aanvragen voor de/het door u beheerde register(s).</p>
        @endif
    @endif
    @if (Auth::user()->isAdmin() || Auth::user()->isTC())
        <!-- nieuws voor een lid TC -->
        <h3>Lid Toetsingscommissie</h3>
        @if ($mijnTCRegisters->count() > 0)
            <p>U bent lid van de Toetsingscommissie van de/het volgende register(s):</p>
            @foreach ($mijnTCRegisters as $register)
                <p>{{$register->code }}</p>
            @endforeach
        @else
            <p>U bent nog niet aan één of meer registers toegewezen.</p>
        @endif
        <?php $registersToetsen = Auth::user()->registersToetsen->pluck('id');
        $aanvragen = Aanvraag::where('aanvraagStatus', 2)->get()->whereIn('register_id', $registersToetsen); ?>
        @if ($aanvragen->count() > 0)
            <p>Er zijn aanvragen ingediend voor een register waarvoor U in de toetsingscommissie zit:</p>
            <table class="pure-table">
                @foreach ($aanvragen as $aanvraag)
                    <tr>
                        <td><a href="{{ url('/aanvragen/' . $aanvraag->id) }}">{{ $aanvraag->deelnemer->name }}</a></td>
                        <td>{{ $aanvraag->register->naam }} ({{ $aanvraag->register->code }})</td>
                    </tr>
                @endforeach
            </table>
        @endif
    @endif
    @if (Auth::user()->isAspirantDeelnemer())
        <!-- nieuws voor een aspirant-deelnemer -->
        <p>Uw aanvraag tot deelname is in behandeling.</p>
    @endif

@endsection
