@extends('layouts.app')

@section('content')
<h3>Criterium voor register {{ $register->naam }}: @if (isset($activiteit)) {{ $activiteit->naam }} @else nieuw criterium @endif</h3>
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif 
	
</div>
<form action="{{ $action }}" method="POST"  enctype="multipart/form-data">
	<input type="hidden" name="register_id" value="{{ $register->id }}"/>
@csrf
<?php 
setlocale(LC_ALL, 'nl_NL.utf8');
	if (isset($activiteit->id) && $activiteit->id != 0)
	{ ?>
		<input type="hidden" name="_method" value="PUT">
<?php } else 
	{ 	
		if (!isset($activiteit->id))
		{
			$activiteit = new App\Activiteit();
			$activiteit->vervolg = request()->vervolg;
		}
	} ?>
<style>fieldset label {width: 115px;} </style>
<fieldset>
	<p>Velden met * zijn verplichte velden.</p>
	<p>
		<label for="naam">Naam*:</label> <input type='text' id='naam' name='naam' value='{{$activiteit->naam}}' style="width: 500px;"></input>
	</p>
	<p>De hierna volgende beschrijving wordt vertoond op de register-informatiepagina en op de aanvraag.</p>
	<p>
		<label for="beschrijving">Beschrijving*:</label> <textarea id="beschrijving" name='beschrijving' rows="10" 
			style="width: 500px; height: unset !important;">{{ $activiteit->beschrijving}}</textarea>
	</p>
	<script>CKEDITOR.replace('beschrijving');</script>
	<p>De invulinstructie wordt alleen vertoond op de aanvraag.</p>
	<p>
		<label for="beschrijving">Invulinstructie:</label> <textarea id="invulinstructie" name='invulinstructie' rows="10" 
			style="width: 500px; height: unset !important;">{{ $activiteit->invulinstructie}}</textarea>
	</p>
	<p>Dit is een criterium voor:<input type="radio" name="vervolg" value="0" @if ($activiteit->vervolg == 0) checked @endif> Toelating</input>
								 <input type="radio" name="vervolg" value="1" @if ($activiteit->vervolg == 1) checked @endif> Verlenging</input></p>
	<p>
		<label for="criType_id">Type*:</label>
		<select name="criType_id" id="criType" onchange="hideShowVelden()">
			@if (!isset($activiteit->criType_id))<option disabled selected>Selecteer...</option>@endif
			@foreach($criTypes as $criType)
			<option value="{{ $criType->id}}" @if ($criType->id == $activiteit->criType_id) selected @endif>{{ $criType->naam }} ({!! $criType->beschrijving !!})</option>
			@endforeach
		</select>
	</p>
	<p class="critTypeAfh critType7">
		<label for="peVereist">Vereist aantal PE-punten:</label>
		<input type="number" min="0" max="1000" step="1" id="peVereist" name="peVereist" value="{{ $activiteit->veld1 }}" /><br/>&nbsp;<br/>
		<label for="peMaximum">Maximaal aantal PE-punten:</label>
		<input type="number" min="0" max="1000" step="1" id="peMaximum" name="peMaximum" value="{{ $activiteit->veld2 }}" />
	</p>
	<p class="critTypeAfh critType12">
		<label for="veld1">Eerste keuze (bijv. "Ja"):</label>
		<input type="text" id="veld1" name="veld1" value="{{ $activiteit->veld1 }}" /><br/>&nbsp;<br/>
		<label for="veld2">Tweede keuze (bijv. "Nee"):</label>
		<input type="text" id="veld2" name="veld2" value="{{ $activiteit->veld2 }}" />
	</p>
	<p class="critTypeAfh critType13">
		<label for="minRegels">Minimum aantal regels te tonen:</label>
		<input type="number" min="1" step="1" id="minRegels" name="minRegels" value="{{ $activiteit->veld1 }}"></input><br/>&nbsp;<br/>
		<label for="maxRegels">Maximaal aantal regels in te vullen:</label>
		<input type="number" min="1" step="1" id="maxRegels" name="maxRegels" value="{{ $activiteit->veld2 }}"></input>
	</p>
	<p class="critTypeAfh critType14">
		<label for="minRegels">Minimum aantal regels te tonen:</label>
		<input type="number" min="1" step="1" id="minRegels" name="minRegels" value="{{ $activiteit->veld1 }}"></input><br/>&nbsp;<br/>
		<label for="maxRegels">Maximaal aantal regels in te vullen:</label>
		<input type="number" min="1" step="1" id="maxRegels" name="maxRegels" value="{{ $activiteit->veld2 }}"></input>
	</p>
	<p><button type="submit" class="pure-button button-success waarsch">Opslaan</button></p>
</fieldset>
</form>
<?php $scrollTo = 'ttoelating'; if ($activiteit->vervolg) $scrollTo = 'tverlenging'; ?>
<p><a href="{{ url('/registers/' . $register->id . '/edit?scrollTo=' . $scrollTo) }}"><button class="pure-button button-secondary"><i class="fas fa-arrow-left"></i>Terug naar register {{ $register->naam }}</button></a></p>
@if (isset($activiteit->id) && $activiteit->id != 0)
<form id="verwijderen" method="POST" action = "{{ url('activiteiten/' . $activiteit->id) }}">
	@csrf
	@method('DELETE')
	<p><button class="pure-button button-warning">Activiteit "{{ $activiteit->naam }}" verwijderen van register {{ $activiteit->register->naam }}</button></p>
</form>
@endif
@endsection
