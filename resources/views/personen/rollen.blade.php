<?php 
use App\Rol;
use App\User;
?>

@if (Auth::user()->isAdmin() )
@if (isset($persoon->id))

<!-- 								 		-->
<!-- Rollen van deze persoon 				-->
<!-- alleen te wijzigen door de Admin		-->
<!-- 								 		-->
<div id="trollen" class="tabcontent">
	<h4>Rollen</h4>
	<form class="pure-form" action="{{ url('/beheerrollen/' . $persoon->id) }}" method="POST"  enctype="multipart/form-data">
		<!-- input type="hidden" name="_method" value="PATCH"></input -->
		@csrf
		<p>
		@foreach(Rol::orderBy("id")->get() as $rol)
		<input type="checkbox" name="rollen[]" value="{{ $rol->id }}" @if ($persoon->heeftRol($rol->id)) checked @endif>{{ $rol->naam }}&nbsp;&nbsp;</input>
		@endforeach
		</p>
		<p><button class="btn btn-primary waarsch">Opslaan</button></p>
	</form>
	<div class="infoblok">
		<p>Laatst gewijzigd op: {{ $persoon->updated_at }}</p>
		@if ($persoon->isBeheerder())
			<p>
				@if (Auth::user()->id == $persoon->id) U bent
				@else {{ $persoon->name }} is
				@endif
				@if ($persoon->registersInBeheer->count() > 0) beheerder van register(s):<p>
					<ul>
					@foreach($persoon->registersInBeheer as $rib)
					<li><a href="{{ url('registers/' . $rib->id) }}">{{ $rib->naam }} ({{ $rib->code }})</a></li>
					@endforeach
					</ul>
				@else Beheerder. Er zijn momenteel geen registers aan  
					@if (Auth::user()->id == $persoon->id) U
					@else {{ $persoon->name }}
					@endif
				toegewezen.
				@endif
			</p>
		@endif
		@if ($persoon->isTC())
			<p>@if (Auth::user()->id == $persoon->id) U bent
				@else {{ $persoon->name }} is
				@endif
			@if ($persoon->registersToetsen->count() > 0)
			lid TC van register(s):</p>
			<ul>
				@foreach($persoon->registersToetsen as $rib)
					<li><a href="{{ url('registers/' . $rib->id) }}">{{ $rib->naam }} ({{ $rib->code }})</a></li>
				@endforeach
			</ul>
			@else Lid TC. Er zijn momenteel geen registers aan  
				@if (Auth::user()->id == $persoon->id) U
				@else {{ $persoon->name }}
				@endif
			toegewezen.
			@endif
			</p>
		@endif
	</div>
</div>
@endif
@endif
