<!-- 								 	-->
<!-- Factuuradres van deze persoon 		-->
<!-- 								 	-->
<?php 
use App\Factuuradres;
use App\User;

if (isset($persoon->id))
{
	if ($persoon->factuuradres) $factuuradres = $persoon->factuuradres; else $factuuradres = new Factuuradres();
	if ($factuuradres->tenaamstelling) $tenaamstelling = $factuuradres->tenaamstelling; else $tenaamstelling = $persoon->name;
}
?>
@if (isset($persoon->id))
	<div id="tfactuuradres" class="tabcontent">
		<form class="pure-form" action="{{ url('/beheerfactadr/' . $persoon->id) }}" method="POST"  enctype="multipart/form-data">
			<h4>Factuuradres</h4>
			@csrf
			<table class="pure-table">
				<tr><td>Tenaamstelling:</td><td><input type="text" name="tenaamstelling" value="{{ $tenaamstelling }}"></input></td></tr>
				<tr><td>Adres:</td><td><input type="text" name="factuuradresr1" value="{{ $factuuradres->factuuradresr1 }}"></input></td></tr>
				<tr><td>Adres regel 2:</td><td><input type="text" name="factuuradresr2" value="{{ $factuuradres->factuuradresr2 }}"></input></td></tr>
				<tr><td>Postcode:</td><td><input type="text" name="postcode" value="{{ $factuuradres->postcode }}"></input></td></tr>
				<tr><td>Plaats:</td><td><input type="text" name="plaats" value="{{ $factuuradres->plaats }}"></input></td></tr>
			</table>
			<p><button class="btn btn-primary waarsch">Opslaan</button></p>
		</form>
	</div>
@endif
