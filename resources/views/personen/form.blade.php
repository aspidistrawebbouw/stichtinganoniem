<?php 
use App\Rol;
?>
@extends('layouts.app')

@section('content')

@if (isset($persoon) && isset($persoon->id) && $persoon->id == Auth::user()->id)
<h3>Uw profiel</h3>
@else
<h3>Persoon: @if (isset($persoon)) {{ $persoon->name }} @endif</h3>
@endif
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif 
	
</div>
<div class="tab">
  <button class="tablinks" onclick="openTab('talgemeen')">Algemeen</button>
  @if (isset($persoon->id))
	 @if (Auth::user()->isAdmin())
	  <button class="tablinks" onclick="openTab('trollen')">Rollen</button>
	 @endif
	 <button class="tablinks" onclick="openTab('tfactuuradres')">Factuuradres</button>
	 <button class="tablinks" onclick="openTab('tcertificaten')">Certificaten</button>
  @endif
</div>
@include ('personen.algemeen')
@include ('personen.factuuradres')
@include ('personen.rollen')
@include ('personen.certificaten')

@if (Auth::user()->isAdmin())
	<p><a href="{{ url('/personen') }}"><button class="pure-button button-secondary"><i class="fas fa-arrow-left"></i> Terug naar lijst Personen</button></a></p>
	@if (isset($persoon->id) && $persoon->id != 0)
	<form id="verwijderen" method="POST" action = "{{ url('personen/' . $persoon->id) }}">
		@csrf
		@method('DELETE')
		<p><button class="pure-button button-warning"><i class="fas fa-trash"></i> "{{ $persoon->name }}" verwijderen</button></p>
	</form>
	@endif
@endif
<p class="scrollTo">{{ $scrollTo }}</p>
<script>
$( document ).ready(function() {
		var element = "{{ $scrollTo }}";
		openTab(element);
	});
</script>


@endsection
