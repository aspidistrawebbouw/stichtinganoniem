@extends('layouts.app')

@section('content')

<?php
use App\User;
use App\Register;
use App\Aanvraag;
?>
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif 
	
</div>
<h3>Beheer Personen</h3>
<p><a href="{{ url('personen/create')}}"><button class="btn btn-primary"><i class="fas fa-plus"></i> Maak nieuwe gebruiker aan</button></a></p>
<p>Toon: <button class="togglecategorieenAlles pure-button button-success">Alles</button>
		@foreach ($categorieen as $cat)
			<button dataStr="{{ str_replace(' ', '_', $cat->naam) }}" class="togglecategorieen pure-button button-success"><i class="fas fa-filter"></i> {{ $cat->naam }}</button>&nbsp;
		@endforeach
</p>
<table id="personenTabel" class="pure-table stripe">
	<thead>
	<th>Naam</th>
	<th>Email</th>
	<th>Rol</th>
	<th>Register(s)</th>
	<th>Verwijder</th>
	</thead>
	@foreach ($personen as $persoon)
	<tr class="@foreach($persoon->rollen as $rol){{ str_replace(" ", "_", $rol->naam ) }} @endforeach">
		<td><a href="{{ url('/personen/' . $persoon->id . '/edit') }}">{{ $persoon->name }}</a></td>
		<td>{{ $persoon->email }}</td>
		<td>{{ implode(", ", $persoon->rollen()->pluck('naam')->toArray()) }}</td>
		<td>
			@if ($persoon->isBeheerder() && $persoon->registersinBeheer->isNotEmpty()) Beheert: 
				@foreach($persoon->registersInBeheer as $register)
					<a href="{{ url('/registers/' . $register->id . '/edit') }}">{{ $register->code }}</a>@if ($register != $persoon->registersInBeheer->last()), @endif 
				@endforeach<br/>
			@endif
			@if ($persoon->isTC() && $persoon->registersToetsen->isNotEmpty()) Lid TC van: 
				@foreach($persoon->registersToetsen as $register)
					<a href="{{ url('/registers/' . $register->id . '/edit') }}">{{ $register->code }}</a>@if ($register != $persoon->registersToetsen->last()), @endif 
				@endforeach<br/>
			@endif
			@if ($persoon->isDeelnemer() && $persoon->registersAangevraagd()->isNotEmpty()) Neemt deel aan: 
				@foreach($persoon->registersAangevraagd() as $register) 
					<a href="{{ url('/registers/' . $register->id . '/edit') }}">{{ $register->code }}</a>@if ($register != $persoon->registersAangevraagd()->last()), @endif 
				@endforeach
			@endif
		</td>
		<td><form action="{{url('personen',$persoon->id)}}" method="post" name="deleteForm"> 
			@method('delete')
			@csrf 
			<button type="submit"><i class="fas fa-trash"></i></button>
			</form>
		</td>
		</tr>
	@endforeach
</table>
</form>
<script>
$(document).ready(function() {
    $('#personenTabel').DataTable({
        "paging":   false,
        "ordering": true,
        "info":     false,
        "language": {
			search: "Zoek in de lijst:",   }
    } );
} );
</script>
@endsection
