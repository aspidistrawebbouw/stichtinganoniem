@extends('layouts.app')

@section('content')

<h3>Bevestiging verwijderen persoon: {{ $persoon->name }}</h3>
<div class="alert alert-danger">
<h4>Pas op!</h4>
<p>U staat op het punt deze persoon te verwijderen.</p>
</div>
@if ($aanvragen->count() > 0)
<p><i class="fas fa-exclamation-triangle alert-danger"></i> Deze persoon heeft nog registraties . Deze zullen ook worden verwijderd:</p>
<table class="pure-table">
	<thead>
		<th>Register</th>
		<th>Status</th>
		<th>Aangemaakt</th>
		<th>Laatst gewijzigd</th>
	</thead>
	@foreach ($aanvragen as $aanvraag)
	<tr>
		<td>{{ $aanvraag->register->naam }}</td>
		<td>{{ $aanvraag->status->naam }}</td>
		<td>{{ $aanvraag->created_at }}</td>
		<td>{{ $aanvraag->updated_at }}</td>
	</tr>
	@endforeach
</table>
@else
<p><i class="fa fa-check alert-success"></i> Deze persoon heeft geen registraties aangevraagd.</p>
@endif
@if ($persoon->isBeheerder() && $persoon->registersInBeheer->count() > 0)
<p><i class="fas fa-exclamation-triangle alert-danger"></i> Deze persoon heeft nog registers in beheer. U dient eerst de registers een andere beheerder te geven.</p>
<table class="pure-table">
	<thead>
		<th>Naam</th>
		<th>Code</th>
		<th>Laatst gewijzigd</th>
	</thead>
	@foreach ($persoon->registersInBeheer as $reg)
	<tr>
		<td><a href="{{ url('/registers/' . $reg->id . '/edit') }}">{{ $reg->code }}</a></td>
		<td>{{ $reg->naam }}</td>
		<td>{{ $reg->updated_at }}</td>
	</tr>
	@endforeach
</table>
@else
<p><i class="fa fa-check alert-success"></i> Deze persoon heeft geen registers in beheer.</p>
@endif
@if ($persoon->isTC() && $persoon->registersToetsen->count() > 0)
<p><i class="fas fa-exclamation-triangle alert-danger"></i> Deze persoon is lid van de Toetsingscommissie van één of meer registers. De persoon zal administratief uit de commissie(s) worden verwijderd.</p>
<table class="pure-table">
	<thead>
		<th>Naam</th>
		<th>Code</th>
		<th>Laatst gewijzigd</th>
	</thead>
	@foreach ($persoon->registersToetsen as $reg)
	<tr>
		<td>{{ $reg->code }}</td>
		<td>{{ $reg->naam }}</td>
		<td>{{ $reg->updated_at }}</td>
	</tr>
	@endforeach
</table>
@else
<p><i class="fa fa-check alert-success"></i> Deze persoon is geen lid van een Toetsingscommissie.</p>
@endif
<div>
	<p>Weet u zeker dat u deze persoon wilt verwijderen?</p>
	<form action="{{ url('/persoonDeleteConfirm/' . $persoon->id) }}" method="POST" class="pure-form">
		<button class="pure-button button-warning">Ja, ik weet het zeker</button>
		@csrf
	</form>
	<br/><a href="{{ url('/personen') }}"><button class="pure-button button-success">Nee, annuleren</button></a>
</div>
@endsection
