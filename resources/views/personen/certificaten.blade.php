<?php 
use App\Rol;
use App\User;
?>

<!-- 								 		-->
<!-- Certificaten van deze persoon 			-->
<!-- 								 		-->
@if (isset($persoon->id))
<div id="tcertificaten" class="tabcontent">
	<h4>Certificaten</h4>
	<table class="pure-table">
		<tr>
			<th>Register</th>
			<th>Toelating of<br/>verlenging</th>
			<th>Status</th>
			<th>Geldig van</th>
			<th>tot</th>
			<th>&nbsp;</th>
		</tr>
	@foreach ($persoon->aanvragen->sort( function ($a, $b) {
					return
						strcmp($a->register_id, $b->register_id) ?:
						- strcmp($a->aanvraagStatus, $b->aanvraagStatus) ?:
						strcmp($a->goedgekeurd, $b->goedgekeurd);
		}) as $aanvraag)

	<tr>
		<td>
			@if (Auth::user()->id == $aanvraag->user_id && $aanvraag->aanvraagStatus == 1)<a href="{{ url('/aanvragen/' . $aanvraag->id . '/edit') }}">{{ $aanvraag->register->code }} ({{ $aanvraag->register->naam }})</a>
			@else {{ $aanvraag->register->code }} ({{ $aanvraag->register->naam }})
			@endif
		</td>
		<td>@if ($aanvraag->vervolg()) Verlenging @else Toelating @endif</td>
		<td>{{ $aanvraag->status->naam }}</td>
		<td>@if ($aanvraag->status->naam == 'goedgekeurd') {{ strftime("%e %B %Y", strtotime($aanvraag->goedgekeurd)) }}@endif</td>
		<td>@if ($aanvraag->status->naam == 'goedgekeurd') {{ strftime("%e %B %Y", strtotime($aanvraag->goedgekeurd . " + " . $aanvraag->register->geldig . " months - 1 days")) }}@endif</td>
		<td>@if ($aanvraag->kritisch() == 3) <span style="color: red;"><i class="fas fa-exclamation-circle"></i> Verlopen</span>
			@elseif ($aanvraag->kritisch() == 2) <span style="color: orange;"><i class="fas fa-exclamation-circle"></i> Bijna verlopen</span>
			@elseif ($aanvraag->kritisch() == 1) <span style="color: green;"><i class="fa fa-check"></i> Geldig</span>
			@endif
		</td>
	</tr>
	@endforeach
	</table>
</div>
@endif
