<!-- 								 	-->
<!-- Stamgegevens van deze persoon 		-->
<!-- 								 	-->
<div id="talgemeen" class="tabcontent">
	<h4>Algemene gegevens</h4>
	<form class="pure-form" action="{{ $action }}" method="POST"  enctype="multipart/form-data">
	@csrf
	<?php 
	setlocale(LC_ALL, 'nl_NL.utf8');
		if (isset($persoon->id) && $persoon->id != 0)
		{ ?>
			<input type="hidden" name="_method" value="PUT">
	<?php } else 
		{
			if (!isset($persoon->id))
			{
				$persoon = new App\User();
			}
		} ?>
		<table class="pure-table">
			<tr><td>Naam:</td><td><input type='text' id='naam' name='name' value='{{$persoon->name}}' ></input></td></tr>
			<tr><td>Email:</td><td><input type='email' id='email' name='email' value='{{$persoon->email}}'></input></td></tr>
		</table>
		<p><button class="btn btn-primary waarsch">Opslaan</button></p>

	</form>
</div>
