$('.togglecategorieen').click(function (){
	var categorie = $(this).attr('dataStr');
	$('.togglecategorieen').removeClass('button-success').addClass('button-alert');
	$('tbody tr').hide();

	$(this).removeClass('button-alert').addClass('button-success');
	$('tbody tr.' + categorie).show();
});

$('.togglecategorieenAlles').click(function (){
	$(this).removeClass('button-alert').addClass('button-success');
	$('tbody tr').show();
});


/* Voor het wijzigen van een Criterium binnen een Register: */

function hideShowVelden (){
	$('.critTypeAfh').hide();
	var typeId = $('#criType').children("option:selected").val();
	$('.critType' + typeId).show();
};

hideShowVelden();

/* Vertonen van een deels ingevulde opleidingentabel */
$('#oplToev').click(function() {
	var nrvis = $('.oplRegel:visible').length;
	$('tr.oplRegel').eq(nrvis).show();
});

/* Vertonen van een deels ingevulde werkgeverstabel */
$('#werkgToev').click(function() {
	var nrvis = $('.werkgRegel:visible').length;
	$('tr.werkgRegel').eq(nrvis).show();
});

/* Tabbladen */

function openTab(tabName) {
  // Declare all variables

  // Get all elements with class="tabcontent" and hide them
  $('.tabcontent').hide();

  // Get all elements with class="tablinks" and remove the class "active"
  $('.tablinks').removeClass('active');

  // Show the current tab, and add an "active" class to the button that opened the tab
  $('#' + tabName).show();
  $( this ).addClass('active');
}
