<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home')->middleware(['auth', 'verified']);

Route::post('/updateTekst', 'RegisterController@updateTekst')->middleware(['auth', 'verified']);
Route::post('/voegTekstToe', 'RegisterController@voegTekstToe')->middleware(['auth', 'verified']);

Route::post('/voegTcToe', 'RegisterController@voegTcToe')->middleware(['auth', 'verified']);
Route::post('/verwijderTc', 'RegisterController@verwijderTc')->middleware(['auth', 'verified']);
Route::post('/voegBeheerderToe', 'RegisterController@voegBeheerderToe')->middleware(['auth', 'verified']);
Route::post('/verwijderBeheerder', 'RegisterController@verwijderBeheerder')->middleware(['auth', 'verified']);

Route::resource('registers', 'RegisterController')->middleware(['auth', 'verified'])->except(['show']);
Route::get('/registers/{id}', 'RegisterController@show');
Route::post('/registers/{id}/beheerDocument', 'RegisterController@beheerDocument')->middleware(['auth', 'verified']);
Route::post('/registers/{id}/slottekst', 'RegisterController@slottekst')->middleware(['auth', 'verified']);
Route::get('/registers/{id}/kort', 'RegisterController@kort')->middleware(['auth','verified']);
Route::post('/registers/{id}/edittekst', 'RegisterController@editCritTekst')->middleware(['auth', 'verified']);
Route::post('/verplaatsCrit', 'RegisterController@verplaatsCrit')->middleware(['auth', 'verified']);
Route::post('/verplaatsTekst', 'RegisterController@verplaatsTekst')->middleware(['auth', 'verified']);
Route::post('/verwijderTekst', 'RegisterController@verwijderTekst')->middleware(['auth', 'verified']);

Route::post('/registerDeleteConfirm/{id}', 'RegisterController@destroyConfirmed')->middleware(['auth', 'verified']);

Route::resource('activiteiten', 'ActiviteitController')->except(['index', 'show'])->middleware(['auth', 'verified']);
Route::resource('personen', 'PersoonController')->middleware(['auth', 'verified'])->except(['show']);
Route::post('/beheerrollen/{id}','PersoonController@beheerRollen')->middleware(['auth', 'verified']);
Route::post('/persoonDeleteConfirm/{id}', 'PersoonController@destroyConfirmed')->middleware(['auth', 'verified']);
Route::post('/beheerfactadr/{id}', 'PersoonController@beheerFactAdr')->middleware(['auth', 'verified']);
Route::resource('aanvragen', 'AanvraagController')->middleware(['auth', 'verified']);
Route::get('indienen/{id}', 'AanvraagController@indienen')->middleware(['auth', 'verified']);
Route::post('aanvragen/{id}/goedkeuren', 'AanvraagController@goedkeuren')->middleware(['auth', 'verified']);
Route::get('aanvragen/{id}/afkeuren', 'AanvraagController@afkeuren')->middleware(['auth', 'verified']);

Route::resource('prestaties', 'PrestatieController')->middleware(['auth', 'verified']);

Route::group(['prefix' => 'messages', 'middleware' => ['auth', 'verified']], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});

// Test events

Route::get('nieuweGebruiker', function()
{
    $subject = App\User::find(71);
    event(new \Illuminate\Auth\Events\Verified($subject));
});


// Mail previews

Route::get('aanvraagGoedgekeurd', function () {
    $aanvraag = App\Aanvraag::first();
    return new App\Mail\AanvraagGoedgekeurd($aanvraag);
});

// Laatste entry - fallback 

Route::fallback(function () {return View::make('errors.403');
});
