<?php

// Een PEPunten verwijst naar een Prestatie van criType 7.

namespace App;

use Illuminate\Database\Eloquent\Model;

class PEPunten extends Model

{
	protected $fillable = ['prestatie_id', 'punten'];
	protected $table = 'nl_pepunten';
    public $timestamps = false;


}
