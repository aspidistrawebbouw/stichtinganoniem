<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\Access\AuthorizationException;

trait VerifiesEmails
{
    use \Illuminate\Foundation\Auth\RedirectsUsers;

    /**
     * Show the email verification notice.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
		Log::info($request->user()->hasVerifiedEmail() ? "VE01 Gebruiker heeft al geverifieerd emailadres" : "VE02 Gebruiker wordt doorverwezen naar auth.verify");
                     
        return $request->user()->hasVerifiedEmail()
                        ? redirect($this->redirectPath())
                        : view('auth.verify');
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function verify(Request $request)
    {
        if ($request->route('id') != $request->user()->getKey()) {
			Log::info("VE03 Ongeldig token");
            throw new AuthorizationException;
        }

        if ($request->user()->hasVerifiedEmail()) {
			Log::info("VE04 Gebruiker heeft al een geverifieerd emailadres");
            return redirect($this->redirectPath());
        }

        if ($request->user()->markEmailAsVerified()) {
			Log::info("VE05 Emailadres is nu geverifieerd");
            event(new Verified($request->user()));
        }

        return redirect($this->redirectPath())->with('verified', true);
    }

    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
			Log::info("VE06 Gebruiker heeft al een geverifieerd emailadres bij resend");
            return redirect($this->redirectPath());
        }

        $request->user()->sendEmailVerificationNotification();

        return back()->with('resent', true);
    }
}
