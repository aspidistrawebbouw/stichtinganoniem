<?php

namespace App\Mail;

use App\Aanvraag;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AanvraagGoedgekeurd extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     
    public $aanvraag;
    
    public function __construct(Aanvraag $aanvraag)
    {
        $this->aanvraag = $aanvraag;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.aanvragen.goedgekeurd');
    }
}
