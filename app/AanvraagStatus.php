<?php

// Tekst-versies van aanvraagstatussen

namespace App;

use Illuminate\Database\Eloquent\Model;

class AanvraagStatus extends Model
{
    protected $fillable = [ 'naam'];
    protected $table = 'nl_aanvraagstatus';
}
