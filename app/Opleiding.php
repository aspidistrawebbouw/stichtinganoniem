<?php

// Een Opleiding is een regel in een OpleidingsTabel, en dat is een Prestatie van type 13.

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opleiding extends Model
{
    protected $fillable = ['prestatie_id', 'opleiding', 'periode', 'diploma' ];
    protected $table = 'nl_opleidingen';
    public $timestamps = false;
    
    public function opleidingstabel()
    {
		return $this->belongsTo('App\Prestatie');		
	}
    
}
