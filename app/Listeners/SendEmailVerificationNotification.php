<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SendEmailVerificationNotification
{
    /**
     * Handle the event.
     *
     * @param  \Illuminate\Auth\Events\Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
		if ($event->user instanceof MustVerifyEmail) Log::info("SEVN01 Gebruiker is MustVerifyEmail"); else  Log::info("SEVN01 Gebruiker is geen MustVerifyEmail");
		if ($event->user->hasVerifiedEmail()) Log::info("SEVN02 Gebruiker heeft geverifieerde email"); else  Log::info("SEVN02 Gebruiker heeft geen geverifieerde email");
        if ($event->user instanceof MustVerifyEmail && ! $event->user->hasVerifiedEmail()) {
            $event->user->sendEmailVerificationNotification();
        }
    }
}
