<?php

// Een Prestatie is een claim van deelnemer x dat hij/zij voldaan heeft aan activiteit y.
// pe en beschrijvingen kunnen wsch. vervallen

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestatie extends Model
{
    protected $fillable = [ 'aanvraag_id', 'activiteit_id', 'datum', 'beschrijving', 'opmerkingen', 'pe', 'status'];
    protected $table = 'nl_prestaties';
    public $timestamps = true;
    
    public function aanvraag()
    {
		return $this->belongsTo('App\Aanvraag');		
	}
    
    public function activiteit()
    {
		return $this->belongsTo('App\Activiteit');
	}
	
    public function uploads()
    {
		return $this->hasMany('App\PrestatieUpload');
	}

	public function opleidingen()
	{
		return $this->hasMany('App\Opleiding', 'prestatie_id', 'id');
	}
	
	public function werkgevers()
	{
		return $this->hasMany('App\Werkgever', 'prestatie_id', 'id');
	}

	public function janee()
	{
		return $this->hasOne('App\JaNee');
	}
	
	public function pepunten()
	{
		return $this->hasOne('App\PEPunten');
	}
	public function tekst()
	{
		return $this->hasOne('App\PrestatieTekst');
	}
}
