<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aanvraag;
use App\User;
use App\Factuuradres;

use App\Rol;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class PersoonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(): \Illuminate\View\View
    {
        Log::info (Auth::user ()->name . ' P01 PersoonController index');
        $this->authorize ('persoonIndex', User::class);
        $personen = User::all ();
        return View::make ('personen.index')
            ->with ('categorieen', Rol::orderBy ('id')->get ())
            ->with ('personen', $personen);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): \Illuminate\View\View
    {
        $this->authorize ('persoonCreate', User::class);
        Log::info (Auth::user ()->name . ' P06 PersoonController create ');
        return View::make ('personen.form')
            ->with ('action', url ('/personen'))
            ->with ('scrollTo', 'talgemeen');
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        Log::info (Auth::user ()->name . ' P11 PersoonController store ');
        $this->authorize ('persoonStore', User::class);
        $foutmeldingen = [
            'required' => 'Dit veld moet ingevuld zijn.',
            'email' => 'Dit is geen geldig e-mailadres'
        ];
        $regels = [
            'name' => 'required',
            'email' => 'required|email',
        ];
        $validator = Validator::make ($request->all (), $regels, $foutmeldingen);
        if ($validator->fails ()) {
            Log::info (Auth::user ()->name . ' P13 PersoonController edit validatiefout' . $request);
            $errors = $validator->errors ();
            return back ()
                ->withErrors ($validator)
                ->withInput ()
                ->with ('rollen', Rol::orderBy ('naam')->get ())
                ->with ('action', url ('/personen'));
        }
        $persoon = new User();
        $this->vul ($request, $persoon);
        $persoon->password = Hash::make (Str::random (100));
        $persoon->save ();

        return redirect ()->action ('PersoonController@edit', ['id' => $persoon->id, 'scrollTo' => 'talgemeen'])
            ->with ('action', url ('/personen/' . $persoon->id))
            ->with ('rollen', Rol::orderBy ('naam')->get ())
            ->with ('success', 'Algemene gegevens ' . $persoon->name . ' opgeslagen');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request): \Illuminate\View\View
    {
		if (!isset($scrollTo)) 
		{
			if (isset($request->scrollTo)) $scrollTo = $request->scrollTo; else $scrollTo = 'talgemeen';
		}

		try
		{
			$persoon = User::findOrFail($id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' P20 PersoonController edit - poging niet-bestaande persoon te wijzigen ' . $id);
			$request->session()->flash('error', 'U bent niet geautoriseerd deze gebruiker te wijzigen.');
			return redirect()->action('HomeController@index');
		}
		if ($persoon->isBeheerder()) { Log::info(Auth::user()->name . ' P92d4 PersoonController beheerRollen - persoon ' . $persoon->name . ' is nu beheerder'); }
		else { Log::info(Auth::user()->name . ' P92d4 PersoonController beheerRollen - persoon ' . $persoon->name . ' is nu geen beheerder'); }
		$this->authorize('persoonEdit', $persoon);
		Log::info(Auth::user()->name . ' P21 PersoonController edit ' . $id);
		if ($persoon->isBeheerder()) { Log::info(Auth::user()->name . ' P92d5 PersoonController beheerRollen - persoon ' . $persoon->name . ' is nu beheerder'); }
		else { Log::info(Auth::user()->name . ' P92d5 PersoonController beheerRollen - persoon ' . $persoon->name . ' is nu geen beheerder'); }
		return View::make('personen.form')
			->with('persoon', User::find($id))
			->with('scrollTo', $scrollTo)
			->with('rollen', Rol::orderBy('naam')->get())
			->with('action', url('personen/' . $id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
		Log::info(Auth::user()->name . ' P30 PersoonController update ' . $id);
		try
		{
			$persoon = User::findOrFail($id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->id . ' P31 PersoonController update - poging niet-bestaande persoon op te slaan ' . $id);
			$request->session()->flash('error', 'U bent niet geautoriseerd deze gebruiker op te slaan.');
			return redirect()->action('HomeController@index');
		}
		$this->authorize('persoonUpdate', $persoon);
		$foutmeldingen = [
			'required' => 'Dit veld moet ingevuld zijn.',
			'email' => 'Dit is geen geldig e-mailadres',
			'unique' => 'Er bestaat al een gebruiker met dat email-adres. Het adres is niet gewijzigd.',
		];
		$regels = [
			'name' => 'required',
			'email' => 'required|email|unique:nl_personen,email,' . $id,
		];
		$validator = Validator::make($request->all(), $regels, $foutmeldingen);
		if ($validator->fails()) {
			Log::info(Auth::user()->name . ' P34 PersoonController edit validatiefout ' . $id);
			$errors = $validator->errors();
			return back()
				->withErrors($validator)
				->withInput()
				->with('rollen', Rol::orderBy('naam')->get())
				->with('action', url('/personen'));
		}
	   $this->vul($request, $persoon);
	   $persoon->save();

		return redirect('personen/' . $persoon->id . '/edit')
			->with('persoon', $persoon)
			->with('action', url('/personen/' . $persoon->id))
			->with('rollen', Rol::orderBy('naam')->get())
			->with('success', 'Gegevens ' . $persoon->name . ' gewijzigd');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request): \Illuminate\View\View
    {
        Log::info (Auth::user ()->name . ' R51 PersoonController Persoon ' . $id . ' verwijderen');
        try {
            $persoon = User::findOrFail ($id);
        } catch (\Exception $ex) {
            Log::info (Auth::user ()->name . ' P52 PersoonController update - poging niet-bestaand persoon ' . $id . 'te verwijderen');
            return redirect ()->action ('PersoonController@index')->with ('error', 'Deze persoon bestaat niet');
        }
        $this->authorize ('persoonDelete', $persoon);
        $aanvragen = Aanvraag::where ('user_id', $id)->get ();
        return View::make ('personen.delete')
            ->with ('persoon', $persoon)
            ->with ('aanvragen', $aanvragen);
    }

    public function destroyConfirmed($id, Request $request): \Illuminate\Http\RedirectResponse
    {
        Log::info (Auth::user ()->name . ' P61 PersoonController destroyConfirmed User ' . $id . ' verwijderen met bevestiging');
        try {
            $persoon = User::findOrFail ($id);
        } catch (\Exception $ex) {
            Log::info (Auth::user ()->name . ' P62 PersoonController destroyConfirmed - poging niet-bestaand persoon ' . $id . '(met bevestiging) te verwijderen');
            return redirect ()->action ('PersoonController@index')->with ('error', 'Deze persoon bestaat niet');
        }
        $this->authorize ('persoonDeleteConfirm', $persoon);
        /**
         * Dankzij foreign key constraints in de database worden, bij het verwijderen van een persoon ook verwijderd:
         * - alle relaties met een register (beheerders, Lid TC) behorende bij deze persoon
         * - een factuuradres
         * - alle relaties met een rol
         */
        $persoon->delete ();
        return redirect ()->action ('PersoonController@index')->with ('success', $persoon->name . ' verwijderd');
    }


    /**
     * @param $request
     * @param $myitem
     */
    private function vul($request, &$myitem)
    {
        $myitem->name = substr ($request->name, 0, 190);
        $myitem->email = substr ($request->email, 0, 190);
    }

    // Maak, op basis van in de interface geplaatste vinkjes, de rollen voor deze gebruiker opnieuw aan

    /**
     * @param $request
     * @param $myitem
     */
    private function vulRollen($request, &$myitem)
    {
        $myitem->rollen ()->detach ();    // wis alle rollen en begin opnieuw
        $myitem = $myitem->fresh ();
        if (isset($request->rollen)) {
            foreach ($request->rollen as $rol => $waarde) {
                $myitem->rollen ()->attach ($waarde);
                $rolnaam = Rol::find ($waarde)->naam;
                Log::info (Auth::user ()->name . ' P83 PersoonController vul rol ' . $rolnaam);
                // Een beheerder is automatisch lid TC
                if ($waarde == 2 && !$myitem->isTC ()) {
                    $myitem->rollen ()->attach (3);
                    Log::info (Auth::user ()->name . ' P84 PersoonController vul: beheerder is automatisch lid TC');
                }
            }
        }
    }

    public function beheerRollen(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        try {
            $persoon = User::findOrFail ($id);
        } catch (\Exception $ex) {
            Log::info (Auth::user ()->name . ' P91 PersoonController beheerRollen - poging rollen van niet-bestaande persoon te wijzigen ' . $id);
            $request->session ()->flash ('error', 'U bent niet geautoriseerd de rollen van deze gebruiker te wijzigen.');
            return redirect ()->action ('HomeController@index');
        }
        $this->authorize ('persoonBeheerRollen', $persoon);
        $persoonWasBeheerder = ($persoon->isBeheerder ());
        $persoonWasLidTC = ($persoon->isTC ());
        $this->vulRollen ($request, $persoon);
        $persoon = $persoon->fresh (); // Deze is belangrijk, omdat anders bijv. isBeheerder() foutieve resultaten oplevert

        if ($persoonWasBeheerder && !($persoon->heeftRol (2))) {
		   // Persoon was registerbeheerder, nu niet meer.
		   foreach ($persoon->registersInBeheer as $reg) 
		   {
			   Log::info(Auth::user()->name . " P38 PersoonController Persoon $persoon->name verwijderd als beheerder van register " . $reg->naam);
			   $reg->beheerders()->detach($persoon);
		   }
		}
		if ($persoon->isBeheerder()) { Log::info(Auth::user()->name . ' P92d3 PersoonController beheerRollen - persoon ' . $persoon->name . ' is nu beheerder'); }
		else { Log::info(Auth::user()->name . ' P92d3 PersoonController beheerRollen - persoon ' . $persoon->name . ' is nu geen beheerder'); }
		if ($persoonWasLidTC && !($persoon->isTC()))
		{
		   // Persoon was lid TC, nu niet meer.
		   foreach ($persoon->registersToetsen as $reg) 
		   {
			   Log::info(Auth::user()->name . " P39 PersoonController Persoon $persoon->name verwijderd als lid TC van register " . $reg->naam);
               $reg->tc ()->detach ($persoon);
           }
        }
        if ($persoon->isBeheerder ()) {
            Log::info (Auth::user ()->name . ' P92d3 PersoonController beheerRollen - persoon ' . $persoon->name . ' is nu beheerder');
        } else {
            Log::info (Auth::user ()->name . ' P92d3 PersoonController beheerRollen - persoon ' . $persoon->name . ' is nu geen beheerder');
        }
        return redirect ()->action ('PersoonController@edit', ['id' => $persoon->id, 'scrollTo' => 'trollen'])
            ->with ('action', url ('/personen/' . $persoon->id))
            ->with ('rollen', Rol::orderBy ('naam')->get ())
            ->with ('success', 'Rollen van ' . $persoon->name . ' opgeslagen');
    }

    public function beheerFactAdr(Request $request, $id)
    {
        if ($request->tenaamstelling && $request->factuuradresr1 && $request->postcode && $request->plaats) {
            try {
                $persoon = User::findOrFail ($id);
            } catch (\Exception $ex) {
                Log::info (Auth::user ()->name . ' P102 PersoonController beheerFactAdr - poging factuuradres in te voeren voor niet-bestaand persoon ' . $id);
                return redirect ()->action ('PersoonController@index')->with ('error', 'Deze persoon bestaat niet');
            }
            if ($persoon->factuuradres) $factuuradres = $persoon->factuuradres;
            else {
                $factuuradres = new Factuuradres();
                $factuuradres->user_id = $id;
            }
            $this->vulFactuurAdres ($request, $factuuradres);
            $factuuradres->save ();
        }
        return redirect ()->action ('PersoonController@edit', ['id' => $id, 'scrollTo' => 'tfactuuradres'])
            ->with ('action', url ('/personen/' . $persoon->id))
            ->with ('rollen', Rol::orderBy ('naam')->get ())
            ->with ('success', 'Factuuradres opgeslagen');
    }

    private function vulFactuurAdres($request, &$myitem)
    {
        $myitem->tenaamstelling = substr ($request->tenaamstelling, 0, 99);
        $myitem->factuuradresr1 = substr ($request->factuuradresr1, 0, 99);
        $myitem->factuuradresr2 = substr ($request->factuuradresr2, 0, 99);
        $myitem->postcode = substr (str_replace (' ', '', $request->postcode), 0, 6);
        $myitem->plaats = substr ($request->plaats, 0, 99);
    }

}
