<?php

namespace App\Http\Controllers;

use App\Activiteit;
use App\CriType;
use App\Register;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class ActiviteitController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return View
     */
    public function create(Request $request)
    {
 		Log::info(Auth::user()->name . ' A06 ActiviteitController create');
 		$register = Register::find($request->register);
 		$criTypes = CriType::orderBy('naam')->get();
		return View::make('activiteiten.form')
			->with('register', $register)
			->with('vervolg', $request->vervolg)
			->with('criTypes', $criTypes)
			->with('action', url('/activiteiten'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
		Log::info(Auth::user()->name . ' A11 ActiviteitController store');
		$foutmeldingen = [
			'required' => 'Dit veld moet ingevuld zijn.',
		];
		$regels = [
			'naam' => 'required',
			'beschrijving' => 'required',
			'criType_id' => 'required',
		];
		$validator = Validator::make($request->all(), $regels, $foutmeldingen);
		if ($validator->fails()) {
			Log::info(Auth::user()->name . ' R13 RegisterController store validatiefout');
			$request->id = 0; 	// wordt in de view beschouwd als geldig (zodat de input bewaard blijft)
								// zie ook standaard-form-view
			return redirect('activiteiten/create?register=' . $request->register_id)
				->withErrors($validator)
				->withInput()
				->with('action', url('activiteiten'));
       }
        $register = Register::find($request->register_id);
        $activiteit = new Activiteit();
        $this->vul($request, $activiteit);
        $activiteit->volgnummer = Activiteit::where('register_id', $register->id)->where('vervolg', $activiteit->vervolg)->max('volgnummer') + 1;
        $activiteit->save();
		$criTypes = CriType::orderBy('naam')->get();
       return View::make('activiteiten.form')		// als je wilt blijven editen, anders index
			->with('activiteit', $activiteit)
			->with('action', url('activiteiten/' . $activiteit->id))
			->with('success-crit' . $activiteit->id, 'Gegevens criterium "' . $activiteit->naam . '" opgeslagen')
			->with('criTypes', $criTypes)
			->with('register', $register)
			;
    }

    /**
     *
     * @param Activiteit $activiteit
     * @return Response
     */
    public function edit($id, Request $request)
    {
		Log::info(Auth::user()->name . ' A21 ActiviteitController edit ' . $id);
 		$register = Register::find($request->register);
 		$criTypes = CriType::orderBy('id')->get();
		return View::make('activiteiten.form')
			->with('activiteit', Activiteit::find($id))
			->with('criTypes', $criTypes)
			->with('register', $register)
			->with('action', url('activiteiten/' . $id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Activiteit $activiteit
     * @return Response
     */
    public function update(Request $request, $id)
    {
		Log::info(Auth::user()->name . ' A11 ActiviteitController store');
		$foutmeldingen = [
			'required' => 'Dit veld moet ingevuld zijn.',
		];
		$regels = [
			'naam' => 'required',
			'beschrijving' => 'required',
			'criType_id' => 'required',
		];
		$validator = Validator::make($request->all(), $regels, $foutmeldingen);
		if ($validator->fails()) {
			Log::info(Auth::user()->name . ' R13 RegisterController store validatiefout');
			$request->id = 0; 	// wordt in de view beschouwd als geldig (zodat de input bewaard blijft)
								// zie ook standaard-form-view
			$errors = $validator->errors();
			return redirect('activiteiten/create?register=' . $request->register_id)
				->withErrors($validator)
				->withInput()
				->with('action', url('activiteiten'));
       }
        $register = Register::find($request->register_id);
        $activiteit = Activiteit::find($id);
        $this->vul($request, $activiteit);
        $activiteit->save();
        $scrollTo = 'ttoelating'; if ($activiteit->vervolg) $scrollTo = 'tverlenging';
		$criTypes = CriType::orderBy('id')->get();
		$request->session()->flash('scrollTo', 'criteria');
       return redirect()->action ('RegisterController@edit', ['id'=> $register->id, 'scrollTo' => $scrollTo])
			;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Activiteit $activiteit
     * @return Response
     */
    public function destroy($id, Request $request)
    {
		Log::info(Auth::user()->name . ' A51 Activiteit ' . $id . ' (' . $request->register_id . ' ) verwijderen');
		$myActiviteit = Activiteit::find($id);
		
	/**
	 * Dankzij foreign key constraints in de database worden, bij het verwijdern van een Activiteit (= criterium) ook verwijderd:
	 * - alle Prestaties behorende bij deze activiteit.
	 */
		$myActiviteit->delete();
		$scrollTo = 'ttoelating';
		if ($myActiviteit->vervolg) $scrollTo = 'tverlenging';
		return redirect()->action('RegisterController@edit', ['id' => $request->register_id, 'scrollTo'=> $scrollTo]); 
    }

	private function vul($request, &$myitem){
		
		// Algemene eigenschappen van het criterium
		$myitem->naam = substr($request->naam,0,254);
		$myitem->beschrijving = substr($request->beschrijving,0,65000);
		$myitem->invulinstructie = substr($request->invulinstructie,0,65000);
		$myitem->register_id = $request->register_id;
		$myitem->criType_id = $request->criType_id;
		$myitem->vervolg = $request->vervolg;
		Log::info(Auth::user()->name . ' A71 Activiteit vul ' . $request->vervolg . ' (' . $request->register_id . ' )');
		if ($request->criType_id == 7) $this->vul_PEPunten($request, $myitem);
		if ($request->criType_id == 10) $this->vul_Tekstregel($request, $myitem);
		if ($request->criType_id == 11) $this->vul_Tekstblok($request, $myitem);
		elseif ($request->criType_id == 12) $this->vul_JaNee($request, $myitem);
		elseif ($request->criType_id == 13) $this->vul_OpleidingenTabel($request, $myitem);
		elseif ($request->criType_id == 14) $this->vul_WerkgeversTabel($request, $myitem);
	}
	
	private function vul_PEPunten($request, &$myitem)
	{
		$myitem->veld1 = $request->peVereist;
		$myitem->veld2 = $request->peMaximum;
	}
	
	private function vul_JaNee($request, &$myitem)
	{
		$myitem->veld1 = $request->veld1;
		$myitem->veld2 = $request->veld2;
	}
	
	private function vul_OpleidingenTabel($request, &$myitem)
	{
		$myitem->veld1 = $request->minRegels;
		$myitem->veld2 = $request->maxRegels;
	}
	
	private function vul_WerkgeversTabel($request, &$myitem)
	{
		$myitem->veld1 = $request->minRegels;
		$myitem->veld2 = $request->maxRegels;
	}
	
	private function vul_Tekstregel($request, &$myitem)
	{
	}

	private function vul_Tekstblok($request, &$myitem)
	{
	}
}
