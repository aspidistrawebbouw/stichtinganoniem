<?php

namespace App\Http\Controllers;

use App\Aanvraag;
use App\Activiteit;
use App\Prestatie;
use App\Opleiding;
use App\Werkgever;
use App\JaNee;
use App\PEPunten;
use App\PrestatieUpload;
use App\PrestatieTekst;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class PrestatieController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        Log::info (Auth::user ()->name . ' PR01 PrestatieController index');
        $this->authorize ('viewAny', Prestatie::class);
        $aanvragen = Aanvraag::all ();
        return View::make ('aanvragen.index')
            ->with ('aanvragen', $aanvragen);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        Log::info (Auth::user ()->name . ' PR11 PrestatieController store');
        // Bestaat de aanvraag?
        try {
            $aanvraag = Aanvraag::findOrFail ($request->aanvraag_id);
        } catch (\Exception $ex) {
            Log::info (Auth::user ()->name . ' PR22 PrestatieController store - poging niet-bestaande aanvraag ' . $request->aanvraag_id . 'te wijzigen');
            return redirect ()->action ('HomeController@index')->with ('error', 'Dit criterium bestaat niet');
        }
        Log::info (Auth::user ()->name . ' PR23 PrestatieController store debug ' . $aanvraag->id . ' ' . $aanvraag->user_id);
        $this->authorize ('prestatieStore', [Prestatie::class, $aanvraag]);
        try {
            $activiteit = Activiteit::find ($request->activiteit_id);
        } catch (\Exception $ex) {
            Log::info (Auth::user ()->name . ' PR23 PrestatieController store - poging niet-bestaande activiteit ' . $request->activiteit_id . 'te wijzigen');
            return redirect ()->action ('HomeController@index')->with ('error', 'Dit criterium bestaat niet');
        }
        $prestatie = new Prestatie();
        $this->vul ($request, $prestatie);
        $prestatie->aanvraag_id = $request->aanvraag_id;
        $prestatie->activiteit_id = $request->activiteit_id;
        $prestatie->datum = date ('Y-m-d');
        $prestatie->status = 1;
        $prestatie->save ();

        if ($prestatie->activiteit->criType_id == 7) $this->vulPEPunten ($request, $prestatie);        // PE-Punten
        elseif ($prestatie->activiteit->criType_id == 8) $this->vulPrestatieUpload ($request, $prestatie); // Bestandsupload
        elseif ($prestatie->activiteit->criType_id == 11) $this->vulTekstblok ($request, $prestatie); // Tekstblok
        elseif ($prestatie->activiteit->criType_id == 12) $this->vulJaNee ($request, $prestatie);        // Ja/nee-vraag
        elseif ($prestatie->activiteit->criType_id == 13) $this->vulOpleidingenTabel ($request, $prestatie);        // Opleidingentabel
        elseif ($prestatie->activiteit->criType_id == 14) $this->vulWerkgeverTabel ($request, $prestatie);        // Werkgeverstabel
        $request->session ()->flash ('success', 'Criterium opgeslagen');
        return redirect ()->action ('AanvraagController@edit', ['id' => $aanvraag->id]);
    }

    public function edit($id): View
    {
        Log::info (Auth::user ()->naam . ' I21 AanvraagController edit ' . $id);
        $aanvraag = Aanvraag::find ($id);
        $register = $aanvraag->register;
        return View::make ('aanvragen.form')
            ->with ('aanvraag', $aanvraag)
            ->with ('register', $register)
            ->with ('action', url ('aanvragen/' . $id));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, $id)
    {
        Log::info (Auth::user ()->name . ' PR26 PrestatieController update ' . $id);
        // Bestaat de aanvraag?
        try {
            $aanvraag = Aanvraag::findOrFail ($request->aanvraag_id);
        } catch (\Exception $ex) {
            Log::info (Auth::user ()->name . ' PR27 PrestatieController update - poging niet-bestaande aanvraag ' . $request->aanvraag_id . 'te wijzigen');
            return redirect ()->action ('HomeController@index')->with ('error', 'Dit criterium bestaat niet');
        }
        Log::info (Auth::user ()->name . ' PR28 PrestatieController update debug ' . $aanvraag->id . ' ' . $aanvraag->user_id);
        try {
            $prestatie = Prestatie::findOrFail ($id);
        } catch (\Exception $ex) {
            Log::info (Auth::user ()->name . ' PR28 PrestatieController update - poging niet-bestaande prestatie ' . $id . 'te wijzigen');
            return redirect ()->action ('HomeController@index')->with ('error', 'Dit criterium bestaat niet');
        }
        $this->authorize ('prestatieUpdate', [$prestatie, $aanvraag]);
        $this->vul ($request, $prestatie);
        $prestatie->datum = date ('Y-m-d');
        $prestatie->save ();
        if ($prestatie->activiteit->criType_id == 7) $this->vulPEPunten ($request, $prestatie);                    // PE-Punten
        elseif ($prestatie->activiteit->criType_id == 8) $this->vulPrestatieUpload ($request, $prestatie);        // Bestandsupload
        elseif ($prestatie->activiteit->criType_id == 10) $this->vulTekstregel ($request, $prestatie);            // Tekstregel
        elseif ($prestatie->activiteit->criType_id == 11) $this->vulTekstblok ($request, $prestatie);            // Tekstblok
        elseif ($prestatie->activiteit->criType_id == 12) $this->vulJaNee ($request, $prestatie);                // Ja/nee-vraag
        elseif ($prestatie->activiteit->criType_id == 13) $this->vulOpleidingenTabel ($request, $prestatie);        // Opleidingentabel
        elseif ($prestatie->activiteit->criType_id == 14) $this->vulWerkgeverTabel ($request, $prestatie);        // Werkgeverstabel
        $prestatie = $prestatie->fresh ();
        foreach ($prestatie->uploads as $upload) {
            $checkbox = "doc" . $upload->id;
            if (isset($request->$checkbox) && $request->$checkbox == '1') {
                $this->authorize ('delete', $upload);
                $filenaamZonderPad = substr ($upload->filenaam, 8);
                Log::info (Auth::user ()->name . 'P35 PrestatieController Proberen te verwijderen: ' . $filenaamZonderPad);
                $verwijderd = Storage::disk ('uploads')->delete ($filenaamZonderPad);
                if ($verwijderd) {
                    $upload->delete ();
                    Log::info (Auth::user ()->name . 'P35 PrestatieController Upload ' . $upload->id . ' verwijderd');
                } else {
                    Log::info (Auth::user ()->name . 'P35 PrestatieController Upload ' . $upload->id . ' niet verwijderd');
                }
            }
        }

        $request->session ()->flash ('success' . $prestatie->activiteit_id, 'Criterium gewijzigd');
        return redirect ()->action ('AanvraagController@edit', ['id' => $aanvraag->id]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Log::info (Auth::user ()->naam . ' I51 Aanvraag ' . $id . ' verwijderen');
        $aanvraag = Aanvraag::find ($id);
        $aanvraag->delete ();
        return redirect ()->action ('AanvraagController@index')->with ('success', 'Aanvraag ' . $aanvraag->naam . ' verwijderd');

    }

    /**
     * @param $request
     * @param $myprestatie
     */
    private function vul($request, $myprestatie)
    {
        $myprestatie->datum = $request->datum;
        $myprestatie->beschrijving = substr ($request->beschrijving, 0, 254);
        $myprestatie->opmerkingen = substr ($request->opmerkingen, 0, 254);
    }

    /**
     * @param $request
     * @param $prestatie
     */
    private function vulOpleidingenTabel($request, $prestatie)
    {
        foreach ($prestatie->opleidingen as $opleiding)
            $opleiding->delete ();
        for ($i = 0; $i <= $prestatie->activiteit->veld2; $i++) {
            $periodei = 'periode' . $i;
            $opleidingi = 'opleiding' . $i;
            $diplomai = 'diplomabehaald' . $i;
            if (isset($request->$periodei) && isset($request->$opleidingi)) {
                $opleiding = new Opleiding();
                $opleiding->prestatie_id = $prestatie->id;
                $opleiding->periode = $request->$periodei;
                $opleiding->opleiding = $request->$opleidingi;
                $opleiding->diploma = isset($request->$diplomai);
                $opleiding->save ();
            }
        }
    }

    private function vulWerkgeverTabel($request, $prestatie)
    {
        foreach ($prestatie->werkgevers as $werkgever)
            $werkgever->delete ();
        for ($i = 0; $i <= $prestatie->activiteit->veld2; $i++) {
            $periodei = 'periode' . $i;
            $werkgeveri = 'werkgever' . $i;
            $functiei = 'functie' . $i;
            if (isset($request->$periodei) && isset($request->$werkgeveri)) {
                $werkgever = new Werkgever();
                $werkgever->prestatie_id = $prestatie->id;
                $werkgever->periode = substr ($request->$periodei, 0, 99);
                $werkgever->naam = substr ($request->$werkgeveri, 0, 99);
                $werkgever->functie = substr ($request->$functiei, 0, 99);
                $werkgever->save ();
            }
        }
    }

    private function vulTekstregel($request, $prestatie)
    {
        $tekst = $prestatie->tekst;
        if ($tekst) $tekst->delete ();
        $tekst = new PrestatieTekst();
        $tekst->prestatie_id = $prestatie->id;
        $tekst->tekst = substr ($request->tekst, 0, 200);
        $tekst->save ();
    }

    private function vulTekstblok($request, $prestatie)
    {
        $tekst = $prestatie->tekst;
        if ($tekst) $tekst->delete ();
        $tekst = new PrestatieTekst();
        $tekst->prestatie_id = $prestatie->id;
        $tekst->tekst = substr ($request->tekst, 0, 60000);
        $tekst->save ();
    }

    private function vulJaNee($request, $prestatie)
    {
        $janee = $prestatie->janee;
        if ($janee) $janee->delete ();
        $janee = new JaNee();
        $janee->prestatie_id = $prestatie->id;
        $janee->janee = ($request->janee === $prestatie->activiteit->veld1);
        $janee->save ();
    }

    private function vulPEPunten($request, $prestatie)
    {
        $pepunten = $prestatie->pepunten;
        if ($pepunten) $pepunten->delete ();
        $pepunten = new PEPunten();
        $pepunten->prestatie_id = $prestatie->id;
        $pepunten->punten = $request->punten;
        $pepunten->save ();
    }

    private function vulPrestatieUpload($request, $prestatie)
    {
        if ($request->file ('upload')) {
            $path = $request->file ('upload')->store ('uploads');
            $upload = new PrestatieUpload();
            $upload->filenaam = $path;
            $upload->originelefilenaam = $request->upload->getClientOriginalName ();
            $upload->prestatie_id = $prestatie->id;
            $upload->save ();
        }
    }


}

/*
 * Gebruikte logregels:
 * 
  		Log::info(Auth::user()->naam . ' I01 AanvraagController index');
		Log::info(Auth::user()->naam . ' I06 AanvraagController create');
		Log::info(Auth::user()->naam . ' I11 AanvraagController store');
			Log::info(Auth::user()->naam . ' I13 AanvraagController store validatiefout');
 		Log::info(Auth::user()->naam . ' I16 AanvraagController show ' . $id);
		Log::info(Auth::user()->naam . ' I21 AanvraagController edit ' . $id);
		Log::info(Auth::user()->naam . ' I26 AanvraagController update ' . $id);
			Log::info(Auth::user()->naam . ' I28 AanvraagController edit validatiefout ' . $id);
		Log::info(Auth::user()->naam . ' I51 Aanvraag ' . $id . ' verwijderen');
*/
