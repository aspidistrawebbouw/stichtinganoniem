<?php
namespace App\Http\Controllers;
use App\Aanvraag;
use App\AanvraagStatus;
use App\Register;

use App\Mail\AanvraagGoedgekeurd;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

/**
 * Class AanvraagController
 * @package App\Http\Controllers
 */
class AanvraagController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     * @throws AuthorizationException
     */
    public function index(Request $request): View
    {
        Log::info (Auth::user ()->name . ' A01 AanvraagController index');
        $this->authorize ('viewAny', Aanvraag::class);
        $aanvragen = Aanvraag::get ();
        $statussen = AanvraagStatus::get ();
        return View::make ('aanvragen.index')
            ->with ('statussen', $statussen)
            ->with ('aanvragen', $aanvragen);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|RedirectResponse
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        Log::info (Auth::user ()->name . ' A06 AanvraagController create');
        $this->authorize ('create', Aanvraag::class);
        try {
            $register = Register::findOrFail ($request->register);
        } catch (Exception $e) {
            Log::info (Auth::user ()->name . ' A05 AanvraagController create - poging aanvraag te doen voor niet-bestaand register ' . $request->register);
            return redirect ()->action ('HomeController@index')->with ('error', 'Dit register bestaat niet');
        }

        // een kandidaat kan een aanvraag doen als:
        // - hij niet eerder een aanvraag voor dit register heeft ingediend (in dat geval gelden de criteria met vervolg = 0) of:
        // - hij eerder een aanvraag heeft ingediend, en deze heeft de status goedgekeurd (in dat geval gelden de criteria met vervolg = 1)
        //
        // In de andere gevallen volgt er een foutmelding.

        $eerdereAanvraag = Aanvraag::where ('user_id', Auth::user ()->id)->where ('register_id', $request->register)->first ();
        if (isset($eerdereAanvraag->id)) {
            if ($eerdereAanvraag->aanvraagStatus == 3) // goedgekeurd
            {
                Log::info (Auth::user ()->name . ' A08 AanvraagController create voor eerder behaald certificaat van register nr.' . $request->register . ' en user ID ' . Auth::user ()->id);
                return View::make ('aanvragen.create')
                    ->with ('vervolg', 1)
                    ->with ('register', $register)
                    ->with ('action', url ('aanvragen'));
            }
            Log::info (Auth::user ()->name . ' A07 AanvraagController create voor eerder aangevraagd register nr.' . $request->register . ' en user ID ' . Auth::user ()->id);
            $request->session ()->flash ('error', 'U heeft al een aanvraag voor dit certificaat opgeslagen.');
            return redirect ()->action ('AanvraagController@edit', ['id' => $eerdereAanvraag->id, 'request' => $request]);
        } else {
            Log::info (Auth::user ()->name . ' A08 AanvraagController create voor niet eerder aangevraagd register nr.' . $request->register . ' en user ID ' . Auth::user ()->id);
            return View::make ('aanvragen.create')
                ->with ('vervolg', 0)
                ->with ('register', $register)
                ->with ('action', url ('aanvragen'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|RedirectResponse
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize ('create', Aanvraag::class);
        Log::info (Auth::user ()->name . ' A11 AanvraagController store');
        $register = Register::find ($request->register_id);
        if (!isset($request->voorwaarden) || !isset($request->betaald)) {
            $request->session ()->flash ('warning', 'U moet aanvinken dat u de informatie gelezen heeft, en dat u betaald heeft.');
            return View::make ('aanvragen.create')
                ->with ('register', $register)
                ->with ('action', url ('aanvragen'));
        }
        $vervolg = 0;
        $eerdereAanvraag = Aanvraag::where ('user_id', Auth::user ()->id)->where ('register_id', $register->id)->first ();
        if (isset($eerdereAanvraag->id) && $eerdereAanvraag->aanvraagStatus == 3) {
            Log::info (Auth::user ()->name . ' A13 AanvraagController store dit is een vervolgaanvraag op goedgekeurde aanvraag ' . $eerdereAanvraag->id);
            // er is een andere aanvraag gedaan en die is goedgekeurd
            $vervolg = 1;
        } else
            if (isset($eerdereAanvraag))
                if ($eerdereAanvraag->aanvraagStatus == 1) // er was al een aanvraag opgeslagen, maar nog niet ingediend, laat staan goedgekeurd
                    return redirect ()->action ('AanvraagController@edit', ['id' => $eerdereAanvraag->id, 'request' => $request]);
                else
                    Log::info (Auth::user ()->name . ' A14 AanvraagController store dit is geen eerste aanvraag, maar status destijds was ' . $eerdereAanvraag->aanvraagStatus);
            else
                Log::info (Auth::user ()->name . ' A15 AanvraagController store dit is een eerste aanvraag van gebruiker ' . Auth::user ()->id . ' tot toelating tot register ' . $register->id);


        $aanvraag = new Aanvraag();
        $this->vul ($request, $aanvraag);
        $aanvraag->aanvraagStatus = 1;
        $aanvraag->opgeslagen = strftime ("%Y-%m-%d %H:%M:%S");
        $aanvraag->save ();
        return View::make ('aanvragen.form')
            ->with ('aanvraag', $aanvraag)
            ->with ('wijzigbaar', true)
            ->with ('vervolg', $vervolg)
            ->with ('register', $register)
            ->with ('action', url ('aanvragen/' . $aanvraag->id))
            ->with ('success', 'Aanvraag opgeslagen');
    }

    /*  Toon de aanvraag, plus details, aan beheerder/Lid TC
     *
     * */


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|RedirectResponse
     * @throws AuthorizationException
     */
    public function show($id, Request $request)
    {
        try {
            $aanvraag = Aanvraag::findOrFail ($id);
        } catch (Exception $ex) {
            Log::info (Auth::user ()->name . ' A23 AanvraagController show - poging niet-bestaande aanvraag te beoordelen');
            $request->session ()->flash ('error', 'U bent niet geautoriseerd deze aanvraag te beoordelen.');
            return redirect ()->action ('AanvraagController@index');
        }
        $this->authorize ('toets', $aanvraag);
        Log::info (Auth::user ()->name . ' A16 AanvraagController show ' . $id);
        $aanvraag = Aanvraag::find ($id);
        return View::make ('aanvragen.toets')
            ->with ('aanvraag', $aanvraag);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|RedirectResponse
     * @throws AuthorizationException
     */
    public function edit($id, Request $request)
    {
        try {
            $aanvraag = Aanvraag::findOrFail ($id);
        } catch (Exception $ex) {
            Log::info (Auth::user ()->name . ' A23 AanvraagController edit - poging niet-bestaande aanvraag te wijzigen: ' . $id);
            $request->session ()->flash ('error', 'U bent niet geautoriseerd deze aanvraag te wijzigen.');
            return redirect ()->action ('HomeController@index');
        }
        $this->authorize ('edit', $aanvraag);
        Log::info (Auth::user ()->name . ' A21 AanvraagController edit ' . $id);
        $register = $aanvraag->register;
        $wijzigbaar = true;
        if ($aanvraag->aanvraagStatus == 2 or $aanvraag->aanvraagStatus == 3) $wijzigbaar = false;
        return View::make ('aanvragen.form')
            ->with ('aanvraag', $aanvraag)
            ->with ('wijzigbaar', $wijzigbaar)
            ->with ('register', $register)
            ->with ('action', url ('aanvragen/' . $id));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\View|RedirectResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $id)
    {
        Log::info (Auth::user ()->name . ' A21 AanvraagController update ' . $id);
        try {
            $aanvraag = Aanvraag::findOrFail ($id);
        } catch (Exception $ex) {
            Log::info (Auth::user ()->name . ' A23 AanvraagController edit - poging niet-bestaande aanvraag te wijzigen: ' . $id);
            $request->session ()->flash ('error', 'U bent niet geautoriseerd deze aanvraag te wijzigen.');
            return redirect ()->action ('HomeController@index');
        }
        $this->authorize ('update', $aanvraag);
        $vervolg = 0;
        $eerdereAanvraag = Aanvraag::where ('user_id', Auth::user ()->id)->where ('register_id', $aanvraag->register->id)->first ();
        if (isset($eerdereAanvraag->id) && $eerdereAanvraag->aanvraagStatus == 3) {
            Log::info (Auth::user ()->name . ' A33 AanvraagController update dit is een vervolgaanvraag op goedgekeurde aanvraag ' . $eerdereAanvraag->id);
            // er is een andere aanvraag gedaan en die is goedgekeurd
            $vervolg = 1;
        } else
            if (isset($eerdereAanvraag))
                Log::info (Auth::user ()->name . ' A34 AanvraagController update dit is geen eerste aanvraag, maar status destijds was ' . $eerdereAanvraag->aanvraagStatus);
            else
                Log::info (Auth::user ()->name . ' A35 AanvraagController update dit is een eerste aanvraag van gebruiker ' . Auth::user ()->id . ' tot toelating tot register ' . $aanvraag->register->id);

        $foutmeldingen = [
            'required' => 'Dit veld moet ingevuld zijn.',
            'regex' => 'Telnr. mag alleen cijfers, "-" en spaties bevatten.',
            'email' => 'Dit is geen geldig e-mailadres.'
        ];
        $regels = [
            'achternaam' => 'required',
            'email' => 'nullable|email',
            'telnr' => 'nullable|regex:/^[0-9 \-+\(\)]{10,16}$/',
        ];
        $validator = Validator::make ($request->all (), $regels, $foutmeldingen);
        if ($validator->fails ()) {
            Log::info (Auth::user ()->name . ' I28 AanvraagController edit validatiefout ' . $id);
            $errors = $validator->errors ();
            return back ()->withErrors ($validator)
                ->with ('aanvraag', $request)
                ->with ('action', url ('aanvragen'));
        }
        $this->vul ($request, $aanvraag);
        $aanvraag->aanvraagStatus = 1; // opgeslagen (ook voor vervolgaanvragen, daarbij is `goedgekeurd` niet NULL)
        $aanvraag->opgeslagen = strftime ("%Y-%m-%d %H:%M:%S");
        $aanvraag->save ();

        return View::make ('aanvragen.form')
            ->with ('vervolg', $vervolg)
            ->with ('aanvraag', $aanvraag)
            ->with ('wijzigbaar', true)
            ->with ('action', url ('aanvragen/' . $id))
            ->with ('success', 'Gegevens ' . $aanvraag->naam . ' opgeslagen');

    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy($id, Request $request)
    {
        Log::info (Auth::user ()->name . ' A51 Aanvraag ' . $id . ' verwijderen');
        try {
            $aanvraag = Aanvraag::findOrFail ($id);
        } catch (Exception $ex) {
            Log::info (Auth::user ()->name . ' A53 AanvraagController edit - poging niet-bestaande aanvraag te verwijderen: ' . $id);
            $request->session ()->flash ('error', 'U bent niet geautoriseerd deze aanvraag te verwijderen.');
            return redirect ()->action ('HomeController@index');
        }
        $this->authorize ('delete', $aanvraag);
        $aanvraag->delete ();
        /* dankzij foreign key constraints worden, bij het verwijderen van een aanvraag, ook verwijderd:
         * - alle Prestaties van deze aanvraag
         */
        return redirect ()->action ('AanvraagController@index')->with ('success', 'Aanvraag ' . $aanvraag->naam . ' verwijderd');

    }

    /**
     * @param $request
     * @param $myaanvraag
     */
    private function vul($request, &$myaanvraag)
    {
        $myaanvraag->register_id = $request->register_id;
        $myaanvraag->user_id = Auth::user ()->id;
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function indienen(Request $request, $id)
    {
        try {
            $aanvraag = Aanvraag::findOrFail ($id);
        } catch (Exception $ex) {
            Log::info (Auth::user ()->name . ' A53 AanvraagController edit - poging niet-bestaande aanvraag in te dienen: ' . $id);
            $request->session ()->flash ('error', 'U bent niet geautoriseerd deze aanvraag in te dienen.');
            return redirect ()->action ('HomeController@index');
        }
        $this->authorize ('indienen', $aanvraag);
        $register = $aanvraag->register;

        /*
         * Bij de huidige inrichting van registers kan niet automatisch gecontroleerd worden of een kandidaat aan
         * alle criteria voldoet.
         * Het is immers mogelijk om te voldoen aan één van twee criteria (geen theorieexamen afgelegd, maar wel werkzaam in het
         * vakgebied) en dan toch in aanmerking te komen voor het certificaat. Zulks is ter beoordeling van de toetsingscommissie.
         * Daarom is het niet verplicht bij alle criteria iets in te vullen, en kan er dus ook geen controle op komen.
         *
         * De volgende code is daarom komen te vervallen.
         *
        if ($aanvraag->vervolg()) $criteria = $register->vervolgCriteria(); else $criteria = $register->toelatingsCriteria();
        foreach ($criteria as $crit)
        {
            $prestaties = $aanvraag->prestaties->where('activiteit_id', $crit->id); // de geleverde prestatie bij dit criterium
            if ($prestaties->isEmpty() ||
                $crit->criType_id == 7 && $prestaties->first()->pepunten->punten < $crit->veld1) // minimale aantal PE-punten
            {
                $request->session()->flash('error', 'U heeft nog niet aan alle criteria voldaan.');
                return redirect()->action('AanvraagController@edit', ['id' => $id, 'request' => $request]);
            }
        }
        *
        * */
        $aanvraag->aanvraagStatus = 2; // ingediend
        $aanvraag->ingediend = date ("Y-m-d H:i:s");
        $aanvraag->save ();
        $alleRegisters = Register::where ('actief', 'J')->orderBy ('code')->get ();
        $aanvragen = Aanvraag::where ('user_id', Auth::user ()->id)->get ();
        $request->session ()->flash ('success', 'Uw aanvraag voor register ' . $register->naam . ' (' . $register->code . ') is ingediend.');
        return redirect ()->action ('HomeController@index');
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function goedkeuren(Request $request, $id)
    {
        $aanvraag = Aanvraag::find ($id);
        $aanvraag->aanvraagStatus = 3; // goedgekeurd
        $aanvraag->goedgekeurd = $request->goedgekeurdDT;
        $aanvraag->goedgekeurd_door = Auth::user ()->id;
        $aanvraag->save ();
        Mail::to ($aanvraag->deelnemer->email)->send (new AanvraagGoedgekeurd($aanvraag));
        $alleRegisters = Register::where ('actief', 'J')->orderBy ('code')->get ();
        $aanvragen = Aanvraag::where ('user_id', Auth::user ()->id)->get ();
        $request->session ()->flash ('success', 'Aanvraag voor register ' . $aanvraag->register->naam . ' (' . $aanvraag->register->code . ') voor ' . $aanvraag->deelnemer->name . ' is goedgekeurd.');
        return redirect ()->action ('AanvraagController@index');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\View
     */
    public function afkeuren(Request $request, $id)
    {
        $aanvraag = Aanvraag::find ($id);
        $aanvraag->aanvraagStatus = 1; // opgeslagen
        $aanvraag->afgekeurd = date ("Y-m-d H:i:s");
        $aanvraag->save ();
        $alleRegisters = Register::where ('actief', 'J')->orderBy ('code')->get ();
        $aanvragen = Aanvraag::where ('user_id', Auth::user ()->id)->get ();
        $request->session ()->flash ('warning', 'Aanvraag voor register ' . $aanvraag->register->naam . ' (' . $aanvraag->register->code . ') voor ' . $aanvraag->deelnemer->name . ' is afgekeurd.');
        $statussen = AanvraagStatus::get ();
        return View::make ('aanvragen.index')
            ->with ('statussen', $statussen)
            ->with ('aanvragen', $aanvragen);
    }
}
