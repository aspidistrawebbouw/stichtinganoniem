<?php

namespace App\Http\Controllers;

use App\Register;
use App\Aanvraag;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $alleRegisters = Register::where ('actief', 'J')->orderBy ('code')->get ();
        $opgeslagen = Aanvraag::where ('user_id', Auth::user ()->id)->where ('aanvraagStatus', 1)->get ();
        $ingediend = Aanvraag::where ('user_id', Auth::user ()->id)->where ('aanvraagStatus', 2)->get ();
        $goedgekeurd = Aanvraag::where ('user_id', Auth::user ()->id)->where ('aanvraagStatus', 3)->get ();

        $aspirantDeelnemers = User::whereHas ('rollen', function (\Illuminate\Database\Eloquent\Builder $query) {
            $query->where ('naam', 'Aspirant-deelnemer');
        })->get ();
        $mijnTCRegisters = Auth::user ()->registersToetsen;
        Log::info (Auth::user () . " H01 op home scherm");
        return view ('home')
            ->with ('opgeslagen', $opgeslagen)      // Aanvragen die deze gebruiker heeft lopen
            ->with ('ingediend', $ingediend)        // Aanvragen die deze gebruiker heeft ingediend
            ->with ('goedgekeurd', $goedgekeurd)    // Aanvragen van deze gebruiker die zijn goedgekeurd
            ->with ('aspirantDeelnemers', $aspirantDeelnemers)  // De aspirantdeelnemers (heads up voor de admin)
            ->with ('alleRegisters', $alleRegisters)    // Actieve registers
            ->with ('mijnTCRegisters', $mijnTCRegisters); // Registers waarvan de gebruiker lid TC is
    }
}
