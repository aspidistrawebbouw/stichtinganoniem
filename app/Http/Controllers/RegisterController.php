<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Register;
use App\RegisterUpload;
use App\Aanvraag;
use App\Activiteit;
use App\Tekst;
use App\User;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request): \Illuminate\View\View

        // Lijst van registers voor de beheerder/admin

    {
        Log::info(Auth::user()->name . ' R01 RegisterController index');
        $this->authorize('register_index', Register::class);
        if (Auth::user()->isAdmin()) $registers = Register::orderBy('code')->get();
        elseif (Auth::user()->isBeheerder()) $registers = Auth::user()->registersInBeheer()->orderBy('code')->get();
        // er is geen else, anders was je niet door de authorize heengekomen
        return View::make('registers.index')
            ->with('registers', $registers);
    }

    // Lijst van registers met informatie en aanmeld-knoppen voor de bezoeker

    public function lijst(Request $request): \Illuminate\View\View
    {
        Log::info(Auth::user()->name . ' R01 RegisterController lijst');
        $this->authorize('register_lijst', Register::class);
        $registers = Register::orderBy('code')->get();
        return View::make('registers.lijst')
            ->with('registers', $registers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(): \Illuminate\View\View
    {
        $this->authorize('register_create', Register::class);
        Log::info(Auth::user()->name . ' R06 RegisterController create');
        return View::make('registers.form')
            ->with('beheerdersOverig', User::whereHas('rollen', function (Builder $query) {
                $query->where('naam', 'Beheerder');
            })->orderBy('name')->get())
            ->with('tcOverig', User::whereHas('rollen', function (Builder $query) {
                $query->where('naam', 'Lid TC');
            })->orderBy('name')->get())
            ->with('scrollTo', 'talgemeen') // Tabblad algemeen
            ->with('action', url('/registers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->authorize('register_store', Register::class);
        Log::info(Auth::user()->name . ' R11 RegisterController store');
        $foutmeldingen = [
            'required' => 'Dit veld moet ingevuld zijn.',
            'code.unique' => 'Er bestaat al een register met die code.',
            'naam.unique' => 'Er bestaat al een register met die naam.',
        ];
        $regels = [
            'naam' => 'required|unique:nl_registers',
            'code' => 'required|unique:nl_registers',
        ];
        $validator = Validator::make($request->all(), $regels, $foutmeldingen);
        if ($validator->fails()) {
            Log::info(Auth::user()->name . ' R13 RegisterController store validatiefout: ' . $request->naam . ' en ' . $request->code);
            $request->id = 0;
            $errors = $validator->errors();
            Log::info(Auth::user()->name . 'R13 De fout was:  ' . $errors);
            return redirect('registers/create')
                ->withErrors($validator)
                ->withInput()
                ->with('action', url('registers'));
        }
        $register = new Register();
        $this->vul($request, $register);
        $register->save();
        if ($request->file('upload')) {
            $path = $request->file('upload')->store('uploads');
            $upload = new RegisterUpload();
            $upload->filenaam = $path;
            $upload->originelefilenaam = $request->upload->getClientOriginalName();
            $upload->register_id = $register->id;
            $upload->save();
        }
        return redirect('registers/' . $register->id . '/edit')
            ->with('register', $register)
            ->with('action', url('registers/' . $register->id))
            ->with('beheerders', User::whereHas('rollen', function (Builder $query) {
                $query->where('naam', 'Beheerder');
            })->orderBy('name')->get())
            ->with('success', 'Gegevens register "' . $register->naam . '" opgeslagen');
    }


    /**
     * Korte info over het register
     *
     * @param $id
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */

    public function kort($id)
    {
        try
        {
            $register = Register::findOrFail($id);
        }

        catch
        (\Exception $ex)
        {
            return redirect()->action('HomeController@index')->with('error', 'Dit register bestaat niet');
        }
        return View::make('registers.kort')
            ->with('register', $register)
            ->with('teksten', $register->teksten->sortBy('volgnummer'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		Log::info(' R16 RegisterController show ' . $id);
        // toon de volledige informatiepagina van dit register.
        try
        {
			$register = Register::findOrFail($id);
		} catch (\Exception $ex)
		{
			return redirect()->action('HomeController@index')->with('error', 'Dit register bestaat niet');
		}
		$alleRegisters = Register::where('actief', 'J')->orderBy('code')->get();
        return View::make('registers.info')
			->with('register', $register)
			->with('teksten', $register->teksten->sortBy('volgnummer'))
			->with('alleRegisters', $alleRegisters)
			;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id, Request $request)
    {
		if (!isset($scrollTo)) 
		{
			if (isset($request->scrollTo)) $scrollTo = $request->scrollTo; else $scrollTo = 'talgemeen';
		}
		Log::info(Auth::user()->name . ' R21 RegisterController edit ' . $id . ' met scrollTo = ' . session('scrollTo') );
		// Bestaat het register?
        try
        {
			$register = Register::findOrFail($id);
		} catch (\Exception $ex)
		{
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet');
		}
		// Mag de gebruiker het register wijzigen?
 		$this->authorize('register_edit', $register);
		$tcOverig = User::whereHas('rollen', function (Builder $query) {
					$query->where('naam', 'Lid TC');
					})
				->whereNotIn('id', $register->tc->pluck('id'))
				->orderBy('name')
				->get();
		$beheerdersOverig = User::whereHas('rollen', function (Builder $query) {
					$query->where('naam', 'Beheerder');
					})
				->whereNotIn('id', $register->beheerders->pluck('id'))
				->orderBy('name')
				->get();
	    // omdat een criterium veranderd kan zijn van vervolg = 1 naar vervolg = 0 (of v.v.) moeten we nu alle criteria hernummeren
	    // dat kan sowieso nooit kwaad
	    $this->hernummerCriteria($register, 0);
	    $this->hernummerCriteria($register, 1);
		return View::make('registers.form')
			->with('register', $register)
			->with('teksten', $register->teksten->sortBy('volgnummer'))
			->with('tcOverig', $tcOverig)
			->with('beheerdersOverig', $beheerdersOverig)
			->with('scrollTo', $scrollTo)
			->with('action', url('registers/' . $id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
		Log::info(Auth::user()->name . ' R26 RegisterController update ' . $id);
		// Bestaat het register?
        try
        {
			$register = Register::findOrFail($id);
		} catch (\Exception $ex)
		{
            Log::info (Auth::user ()->name . ' R28 RegisterController update - poging niet-bestaand register ' . $id . 'te wijzigen');
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet');
		}
		// Mag de gebruiker het register wijzigen?
 		$this->authorize('register_update', $register);
		$foutmeldingen = [
			'required' => 'Dit veld moet ingevuld zijn.',
			'code.unique' => 'Er bestaat al een register met die code.',
			'naam.unique' => 'Er bestaat al een register met die naam.',
		];
		$regels = [
			'naam' => 'required|unique:nl_registers,naam,' . $id,
			'code' => 'required|unique:nl_registers,code,' . $id,
		];
		$validator = Validator::make($request->all(), $regels, $foutmeldingen);
		if ($validator->fails()) {
			Log::info(Auth::user()->name . ' R27 RegisterController store validatiefout (' . $id . ') : ' . $request->naam . ' en ' . $request->code);
			$errors = $validator->errors();
			return redirect('registers/create')
				->withErrors($validator)
				->withInput()
				->with('action', url('registers'));
		}
		$register = Register::find($id);
		$this->vul($request, $register);
	    $register->save();
	    // omdat een criterium veranderd kan zijn van vervolg = 1 naar vervolg = 0 (of v.v.) moeten we nu alle criteria hernummeren
	    // dat kan sowieso nooit kwaad
	    $this->hernummerCriteria($register, 0);
	    $this->hernummerCriteria($register, 1);
	    	   

		return redirect()->action('RegisterController@edit', ['id'=> $register->id, 'scrollTo' => 'talgemeen'])
			->with('action', url('registers/' . $id))
			->with('success', 'Gegevens register "' . $register->naam . '" gewijzigd.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id, Request $request)
    {
        Log::info (Auth::user ()->name . ' R51 Register ' . $id . ' (' . $id . ' ) verwijderen');
        // Bestaat het register?
        try {
            $register = Register::findOrFail ($id);
        } catch (\Exception $ex) {
            Log::info (Auth::user ()->name . ' R52 RegisterController update - poging niet-bestaand register ' . $id . 'te verwijderen');
            return redirect ()->action ('RegisterController@index')->with ('error', 'Dit register bestaat niet');
        }
        // Mag de gebruiker het register verwijderen?
        $this->authorize ('register_delete', $register);
        $aanvragen = Aanvraag::where ('register_id', $id)->get ();
        $criteria = Activiteit::where ('register_id', $id)->get ();
        return View::make ('registers.delete')
            ->with ('register', $register)
            ->with ('aanvragen', $aanvragen)
            ->with ('criteria', $criteria);
    }

    public function destroyConfirmed($id, Request $request)
    {
        Log::info (Auth::user ()->name . ' R61 Registercontroller destroyConfirmed Register ' . $id . ' verwijderen met bevestiging');
        try {
            $register = Register::findOrFail ($id);
        } catch (\Exception $ex) {
            Log::info (Auth::user ()->name . ' R62 RegisterController destroyConfirmed - poging niet-bestaand register ' . $id . '(met bevestiging) te verwijderen');
            return redirect ()->action ('RegisterController@index')->with ('error', 'Dit register bestaat niet');
        }
        $this->authorize ('register_delete_confirm', $register);

        /* dankzij foreign key constraints in de database worden, bij het verwijderen van een register, ook verwijderd:
         * - criteria
         * - aanvragen
         * - relaties met personen: Leden TC en beheerders
         * - registeruploads
         * - teksten
         */

        $register->delete ();
        return redirect ()->action ('RegisterController@index')->with ('success', 'Register "' . $register->naam . '" verwijderd');
    }

    private function vul($request, $myitem): void
    {
        // hevel attributen van $request over naar $myitem
        $myitem->code = substr ($request->code, 0, 9);
        $myitem->naam = substr ($request->naam, 0, 99);
//		if (Auth::user()->isBeheerder()) 
//			$myitem->beheerder_id = Auth::user()->id;
//		else 
//			$myitem->beheerder_id = $request->beheerder_id;
        if ($request->actief == 'J') $myitem->actief = 'J'; else $myitem->actief = 'N';
        $myitem->geldig = $request->geldig;
        Log::info (Auth::user ()->name . ' R61 RegisterController vul ' . $myitem->code . ' ' . $myitem->naam . ' ' . $myitem->actief);
    }

    public function slottekst($id, Request $request)
    {
        Log::info (Auth::user ()->name . ' R61 Registercontroller slottekst Register ' . $id);
        try {
            $register = Register::findOrFail ($id);
        } catch (\Exception $ex) {
            Log::info (Auth::user ()->name . ' R62 RegisterController slottekst - poging slottekst van niet-bestaand register ' . $id . 'te verwijderen');
            return redirect ()->action ('RegisterController@index')->with ('error', 'Dit register bestaat niet');
        }
        $this->authorize ('register_slottekst', $register);
        $register->slottekst = substr ($request->slottekst, 0, 999);
        $register->save ();
        return redirect ()->action ('RegisterController@edit', ['id' => $register->id, 'scrollTo' => 'ttoelating'])
            ->with ('action', url ('registers/' . $id))
            ->with ('success', 'Slottekst register "' . $register->naam . '" gewijzigd.');
    }

    public function updateTekst(Request $request)
    {
        Log::info (Auth::user ()->name . ' R72 RegisterController updateTekst - ' . $request->tekst_id);
        try {
            $tekst = Tekst::findOrFail ($request->tekst_id);
        } catch (\Exception $ex) {
            Log::info (Auth::user ()->name . ' R73 RegisterController updateTekst - poging niet-bestaande tekst ' . $request->tekst_id . ' te wijzigen');
            return redirect ()->action ('RegisterController@edit', ['id' => $request->register_id])->with ('error', 'Deze tekst bestaat niet');
		}
 		try
 		{
			$register = Register::findOrFail($tekst->register_id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R74 RegisterController updateTekst - poging tekst van niet-bestaand register ' . $tekst->register_id . ' te wijzigen');
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet');
		}
 		$this->authorize('register_updateTekst', $register);
		if (trim($request->tekst) != '')
		{
			$tekst->tekst = substr($request->tekst,0,65000);
			$boodschap = 'Tekst opgeslagen';
			$tekst->save();
			$volgnummer = $tekst->volgnummer;
		} else 
		{
			$boodschap = 'Tekst verwijderd';
			// scroll naar het vorige tekstblok, als dat er was
			$volgnummer = $tekst->volgnummer;
			Log::info("volgnummer was $volgnummer");
			if ($volgnummer > 0) $volgnummer = $volgnummer - 1;
			Log::info("volgnummer is nu $volgnummer");
			$tekst->delete();
			$this->hernummerTeksten($register);
			Log::info("volgnummer is $tekst->volgnummer");
		}
		try
		{
			// wat is nu de tekst op die plek?
			$tekst = Tekst::where('register_id', $register->id)->where('volgnummer', $volgnummer)->firstOrFail();
		} catch (\Exception $ex)
		{
			// vreemde situatie. Geen tekstblokken meer? dan geen boodschap
			$volgnummer = 0;
		}
		
		return redirect()->action('RegisterController@edit', ['id'=> $register->id, 'scrollTo' => 'tteksten'])
			->with('successTekst' . $tekst->id, $boodschap)
		;
	}
	
	public function voegTekstToe(Request $request)
	{
		Log::info(Auth::user()->name . ' R81 RegisterController voegTekstToe aan register ' . $request->register_id );
 		try
 		{
			$register = Register::findOrFail($request->register_id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R84 RegisterController updateTekst - poging tekst toe te voegen aan niet-bestaand register ' . $request->register_id );
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet');
		}
 		$this->authorize('register_voegTekstToe', $register);
		$tekst = new Tekst();
		$tekst->register_id = $request->register_id;
		$volgnummer = Tekst::where('register_id', $request->register_id)->max('volgnummer');
		$tekst->tekst = "";
		$tekst->volgnummer = $volgnummer + 1;
		$tekst->save();
		return redirect()->action('RegisterController@edit', ['id' => $register->id, 'scrollTo' => 'tteksten' ]);
	}

	public function verplaatsTekst(Request $request)
	{
		Log::info(Auth::user()->name . ' R91 RegisterController verplaatsTekst ' . $request->tekst_id);
		// is de actie toegestaan?
		// bestaat het tekstblok?
		try
		{
			$tekst = Tekst::findOrFail($request->tekst_id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R92 RegisterController uverplaatsTekst - poging niet-bestaande tekst te verplaatsen: ' . $request->tekst_id );
			return redirect()->action('RegisterController@index')->with('error', 'Deze tekst bestaat niet');
		}
		// bestaat het register?
		try
		{
			$register = Register::findOrFail($tekst->register_id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R92 RegisterController uverplaatsTekst - poging tekst te verplaatsen van een niet-bestaand register ' . $request->register_id );
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet');
		}
	// mag deze gebruiker de tekst verplaatsen?
 		$this->authorize('register_verplaatsTekst', $register);
		$volgnummer = $tekst->volgnummer;
		$teksten = $register->teksten;
	// kan de tekst verplaatst worden?
		if (($volgnummer == $teksten->min('volgnummer') && $request->richting == 'op') || ($volgnummer == $teksten->max('volgnummer') && $request->richting == 'neer'))
		{
			Log::info(Auth::user()->name . ' R94 RegisterController uverplaatsTekst - poging tekst buiten de rij te verplaatsen, volgnummer:  ' . $request->volgnummer );
			$this->hernummerTeksten();
			return redirect()->action('RegisterController@edit', ['id' => $register->id, 'scrollTo' => 'tteksten' ])
			;
		} else 
		{
	// verwissel de teksten
			$anderVolgnummer = ($request->richting == 'op' ? $volgnummer -1 : $volgnummer + 1);
			$andereTekst = Tekst::where('register_id', $register->id)->where('volgnummer', $anderVolgnummer)->first();
			$andereTekst->volgnummer = $volgnummer; $andereTekst->save();
			$tekst->volgnummer = $anderVolgnummer; $tekst->save();
			return redirect()->action('RegisterController@edit', ['id' => $register->id, 'scrollTo' => 'tteksten' ])
			;
		}
	}

	public function verplaatsCrit(Request $request)
	{
		Log::info(Auth::user()->name . ' R91 RegisterController verplaatsCrit ' . $request->crit_id);
		// is de actie toegestaan?
		// bestaat het criterium?
		try
		{
			$crit = Activiteit::findOrFail($request->crit_id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R96 RegisterController verplaatsCrit - poging niet-bestaand criterium te verplaatsen: ' . $request->crit_id );
			return redirect()->action('RegisterController@index')->with('error', 'Dit criterium bestaat niet');
		}
		// bestaat het register?
		try
		{
			$register = Register::findOrFail($crit->register_id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R92 RegisterController verplaatsCrit - poging criterium te verplaatsen van een niet-bestaand register ' . $request->register_id );
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet');
		}
	// mag deze gebruiker het criterium verplaatsen?
 		$this->authorize('register_verplaatsCrit', $register);
		$volgnummer = $crit->volgnummer;
		$vervolg = $request->vervolg;
		$teksten = $register->teksten;
		if ($vervolg) 
		{
			$criteria = $register->vervolgCriteria();
			$scrollTo = 'tverlenging';
		} else
		{
			$criteria = $register->toelatingsCriteria();
			$scrollTo = 'ttoelating';
		}
	// kan het criterium verplaatst worden?
		if (($volgnummer == $criteria->min('volgnummer') && $request->richting == 'op') || ($volgnummer == $criteria->max('volgnummer') && $request->richting == 'neer'))
		{
			Log::info(Auth::user()->name . ' R94 RegisterController verplaatsCrit - poging criterium buiten de rij te verplaatsen, volgnummer:  ' . $request->volgnummer );
			$this->hernummerCriteria($register,$vervolg);
			return redirect()->action('RegisterController@edit', ['id' => $register->id, 'scrollTo' => $scrollTo ])
			;
		} else 
		{
	// verwissel de criteria
			$anderVolgnummer = ($request->richting == 'op' ? $volgnummer -1 : $volgnummer + 1);
//			dd("Verwissel van register " . $register->id . " criteria volgnummer " . $volgnummer . " en " . $anderVolgnummer . " terwijl vervolg = " . $request->vervolg);
			$andereCrit = Activiteit::where('register_id', $register->id)->where('vervolg', $vervolg)->where('volgnummer', $anderVolgnummer)->first();
			$andereCrit->volgnummer = $volgnummer; $andereCrit->save();
			$crit->volgnummer = $anderVolgnummer; $crit->save();
			return redirect()->action('RegisterController@edit', ['id' => $register->id, 'scrollTo' => $scrollTo ])
			;
		}
	}


	public function verwijderTekst(Request $request)
	{
		Log::info(Auth::user()->name . ' R111 RegisterController verwijderTekst ' . $request->tekst_id);
		// bestaat het tekstblok?
		try
		{
			$tekst = Tekst::findOrFail($request->tekst_id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R112 RegisterController uverwijderTekst - poging niet-bestaande tekst te verwijderen: ' . $request->tekst_id );
			return redirect()->action('RegisterController@index')->with('error', 'Deze tekst bestaat niet');
		}
		// bestaat het register?
		try
		{
			$register = Register::findOrFail($tekst->register_id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R112 RegisterController uverwijderTekst - poging tekst te verwijderen van een niet-bestaand register ' . $request->register_id );
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet');
		}
		// is de actie toegestaan?
 		$this->authorize('register_verwijderTekst', $register);
		$tekst->delete();
		$this->hernummerTeksten($register);
		return redirect()->action('RegisterController@edit', ['id' => $register->id, 'scrollTo' => 'tteksten' ]);
	}

	public function voegTcToe(Request $request)
	{
		try
		{
			$register = Register::findOrFail($request->register_id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R104 RegisterController voegTcToe - poging lid TC toe te voegen aan een niet-bestaand register ' . $request->register_id );
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet');
		}
		try
		{
			$lidtc = User::findOrFail($request->lidtc);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R104 RegisterController voegTcToe - poging niet bestaand lid TC toe te voegen aan register ' . $request->lidtc );
			return redirect()->action('RegisterController@index')->with('error', 'Deze persoon bestaat niet');
		}
 		$this->authorize('register_voegTcToe', [$register, $lidtc]);
		$register->tc()->attach($lidtc);
		return redirect()->action('RegisterController@edit', ['id' => $register->id, 'scrollTo' => 'tledenTC' ])
		;
	}
	
	
	public function verwijderTc(Request $request)
	{
		try
		{
			$register = Register::findOrFail($request->register_id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R104 RegisterController voegTcToe - poging lid TC te verwijderen van een niet-bestaand register ' . $request->register_id );
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet');
		}
		try
		{
			$lidtc = User::findOrFail($request->lidtc);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R104 RegisterController voegTcToe - poging niet bestaand lid TC te verwijderen van register ' . $request->lidtc );
			return redirect()->action('RegisterController@index')->with('error', 'Deze persoon bestaat niet');
		}
 		$this->authorize('register_verwijderTc', [$register, $lidtc]);
		$register->tc()->detach($lidtc);
		$register = $register->fresh();
		return redirect()->action('RegisterController@edit', ['id' => $register->id, 'scrollTo' => 'tledenTC' ])
		;
	}
	
	public function voegBeheerderToe(Request $request)
	{
		try
		{
			$register = Register::findOrFail($request->register_id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R114 RegisterController voegBeheerderToe - poging beheerder toe te voegen aan een niet-bestaand register ' . $request->register_id );
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet');
		}
		try
		{
			$beheerder = User::findOrFail($request->beheerder);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R115 RegisterController voegBeheerderToe - poging niet bestaande beheerder toe te voegen aan register ' . $request->beheerder );
			return redirect()->action('RegisterController@index')->with('error', 'Deze persoon bestaat niet');
		}
 		$this->authorize('register_voegBeheerderToe', [$register, $beheerder]);
		$register->beheerders()->attach($beheerder);
		return redirect()->action('RegisterController@edit', ['id' => $register->id,'scrollTo' => 'tbeheerder' ])
		;
	}
	
	public function verwijderBeheerder(Request $request)
	{
		Log::info(Auth::user()->name . ' R121 RegisterController verwijderBeheerder ' . $request->beheerder);
		try
		{
			$register = Register::findOrFail($request->register_id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R124 RegisterController verwijderBeheerder - poging beheerder te verwijderen van een niet-bestaand register ' . $request->register_id );
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet');
		}
		try
		{
			$beheerder = User::findOrFail($request->beheerder);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R125 RegisterController verwijderBeheerder - poging niet bestaande beheerder te verwijderen van register ' . $request->lidtc );
			return redirect()->action('RegisterController@index')->with('error', 'Deze persoon bestaat niet');
		}
 		$this->authorize('register_verwijderBeheerder', [$register, $beheerder]);
		$register->beheerders()->detach($beheerder);
		$register = $register->fresh();
		return redirect()->action('RegisterController@edit', ['id' => $register->id, 'scrollTo' => 'tbeheerder' ])
		;
	}
	
	public function editCritTekst(Request $request, $id)
	{
		Log::info(Auth::user()->name . ' R90 RegisterController editCritTekst register ' . $id);
		try
		{
			$register = Register::findOrFail($id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R92 RegisterController editCritTekst - poging teksten van niet-bestaand register ' . $id . 'te beheren');
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet (meer)');
		}
 		$this->authorize('register_editCritTekst', $register);
		Log::info(Auth::user()->name . ' R92 RegisterController editCritTekst ' . $register->id . ' plaats: ' . $request->plaats . ' vervolg? ' . $request->vervolg);
		if ($request->plaats == 'voor')
			if ($request->vervolg == 0) $register->tekstvoortoel = substr($request->tekstvoor0,0,65000); 
			else $register->tekstvoorverl = substr($request->tekstvoor1,0,65000);
		else if ($request->plaats == 'na')
			if ($request->vervolg == 0) $register->tekstnatoel = substr($request->tekstna0,0,65000); 
			else $register->tekstnaverl = substr($request->tekstna1,0,65000);

		$scrollTo = 'ttoelating'; if ($request->vervolg == 1) $scrollTo = 'tverlenging';
		$register->save();
		return redirect()->action('RegisterController@edit', ['id' => $register->id, 'scrollTo' => $scrollTo ])
		;
	}

	public function beheerDocument(Request $request, $id)
	{
		Log::info(Auth::user()->name . ' R90 RegisterController beheerDocumenten register ' . $id);
		try
		{
			$register = Register::findOrFail($id);
		} catch (\Exception $ex)
		{
			Log::info(Auth::user()->name . ' R92 RegisterController beheerDocument - poging documenten van niet-bestaand register ' . $id . 'te beheren');
			return redirect()->action('RegisterController@index')->with('error', 'Dit register bestaat niet (meer)');
		}
 		$this->authorize('register_verwijderTc', $register);
		$gewijzigd = false;
		Log::info(Auth::user()->name . ' R92 RegisterController beheerDocument - gebruiker is geautoriseerd register ' . $id . ' te beheren');
		if ($request->hasFile('logo'))
		{
			Log::info(Auth::user()->name . ' R93 RegisterController beheerDocument - bestand toevoegen aan register ' . $id);
			$path = $request->logo->store('uploads');
			$upload = new RegisterUpload();
			$upload->filenaam = $path;
			$upload->originelefilenaam = $request->logo->getClientOriginalName();
			$upload->register_id = $id;
			$upload->save();
			$gewijzigd = true;
		} else
			Log::info(Auth::user()->name . ' R94 RegisterController beheerDocument - geen bestand');
	   foreach($register->uploads as $upload)
		{
			$checkbox = "doc" . $upload->id;
			if (isset($request->$checkbox) && $request->$checkbox == '1')
			{
				$filenaamZonderPad = substr($upload->filenaam,8);
				Log::info(Auth::user()->name . 'R35 RegisterController Proberen te verwijderen: '. $filenaamZonderPad);
				$verwijderd = Storage::disk('uploads')->delete($filenaamZonderPad);
				$gewijzigd = true;
				if ($verwijderd) {
					$upload->delete();
					Log::info(Auth::user()->name . 'R35 RegisterController Upload ' . $upload->id . ' verwijderd');
				} else 
				{
					Log::info(Auth::user()->name . 'R35 RegisterController Upload ' . $upload->id . ' niet verwijderd');
				}
			}
		}
		if ($gewijzigd) $request->session()->flash('documentSuccess', 'Wijzigingen opgeslagen.');
		return redirect()->action('RegisterController@edit', ['id' => $id, 'scrollTo' => 'tdocumenten' ])
		;
	}

	private function hernummerTeksten($register)
	{
		$teksten = $register->teksten->sortBy('volgnummer');
		$i = 0;
		foreach ($teksten as $tekst)
		{
			$tekst->volgnummer = $i;
			$tekst->save();
			$i++;
		}
	}
	private function hernummerCriteria($register, $vervolg)
	{
		$criteria = $register->criteria->where('vervolg', $vervolg)->sortBy('volgnummer');
		$i = 0;
		foreach ($criteria as $crit)
		{
			$crit->volgnummer = $i;
			$crit->save();
			$i++;
		}
	}
}
