<?php

// Een RegisterUpload is een bestand dat geüpload is om bij een Register weergegeven te worden.

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterUpload extends Model
{
    protected $fillable = [ 'register_id', 'filenaam', 'originelefilenaam'];
    protected $table = 'nl_registeruploads';
    public $timestamps = true;
    
    public function register()
    {
		return $this->belongsTo('App\Register');		
	}

}
