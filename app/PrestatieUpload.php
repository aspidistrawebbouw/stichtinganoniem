<?php

// Een PrestatieUpload is een bestand dat geüpload is in het kader van een Prestatie

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrestatieUpload extends Model
{
    protected $fillable = [ 'prestatie_id', 'filenaam', 'originelenaam'];
    protected $table = 'nl_prestatieuploads';
    public $timestamps = true;
    
    public function prestatie()
    {
		return $this->belongsTo('App\Prestatie');		
	}

}
