<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $fillable = ['code', 'naam', 'actief', 'laatste_wijziging'];
    protected $table = 'nl_registers';
    public $timestamps = true;
    
    public function criteria()
    {
		return $this->hasMany('App\Activiteit');
	}
    
    public function toelatingsCriteria()
    {
		return $this->criteria()->where('vervolg', 0)->get();
	}
	
    public function vervolgCriteria()
    {
		return $this->criteria()->where('vervolg', 1)->get();
	}
	
    public function teksten()
    {
		return $this->hasMany('App\Tekst');
	}
	
	public function uploads()
	{
		return $this->hasMany('App\RegisterUpload');
	}
	
	public function beheerders()
	{
		return $this->belongsToMany('App\User', 'nl_registerbeheer');
	}
	
	public function tc()
	{
		return $this->belongsToMany('App\User', 'nl_tc');
	}
	
	public function wordtBeheerdDoor($id)
	// is gebruiker $id een beheerder van dit register?
	{
		return in_array($id, $this->beheerders()->pluck('nl_personen.id')->toArray());
	}
	
	public function wordtGetoetstDoor($id)
	// is gebruiker $id lid van de TC van dit register?
	{
		return in_array($id, $this->tc()->pluck('nl_personen.id')->toArray());
	}
	
}
