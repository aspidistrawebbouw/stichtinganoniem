<?php

namespace App;

// Als alle types criterium verwijzen naar een Model kunnen veld1-3 en check1-2 vervallen.
// Variabele velden zijn verplaatst naar Activiteit.

use Illuminate\Database\Eloquent\Model;

class CriType extends Model
{
    protected $fillable = ['naam', 'veld1', 'veld2', 'veld3', 'check1', 'check2', 'min', 'max' ];
    protected $table = 'nl_critypes';
    public $timestamps = false;
  
}
