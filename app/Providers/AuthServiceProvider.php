<?php

namespace App\Providers;

use App\Aanvraag;
use App\Policies\AanvraagPolicy;
use App\Register;
use App\Policies\RegisterPolicy;
use App\Prestatie;
use App\Policies\PrestatiePolicy;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Prestatie::class => PrestatiePolicy::class,
        Aanvraag::class => AanvraagPolicy::class,
        Register::class => RegisterPolicy::class,
        User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
// Personen

		Gate::define('personen-wijzigRol', function ($user) {
			return ($user->isAdmin() );
		});
		Gate::define('personen-delete', function ($user) {
			return ($user->isAdmin() );
		});
		Gate::define('personen-delete-confirm', function ($user, $persoon) {
			return ($user->isAdmin() && $user->id != $persoon->id && $persoon->registersInBeheer->count() == 0);
		});
	}

}
