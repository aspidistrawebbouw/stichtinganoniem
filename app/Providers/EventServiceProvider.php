<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
		'Illuminate\Auth\Events\Login' => [
			'App\Listeners\LogSuccessfulLogin',
		], 
	    'Illuminate\Auth\Events\Registered' => [
			'App\Listeners\LogRegisteredUser','App\Listeners\SendEmailVerificationNotification',
		],

        'Illuminate\Auth\Events\Verified' => [
            'App\Listeners\SendEmailNieuweGebruiker',
        ],

        'Illuminate\Auth\Events\Attempting' => [
			'App\Listeners\LogAuthenticationAttempt',
		],

		'Illuminate\Auth\Events\Authenticated' => [
			'App\Listeners\LogAuthenticated',
		],

		'Illuminate\Auth\Events\Login' => [
			'App\Listeners\LogSuccessfulLogin',
		],

		'Illuminate\Auth\Events\Failed' => [
			'App\Listeners\LogFailedLogin',
		],

		'Illuminate\Auth\Events\Logout' => [
			'App\Listeners\LogSuccessfulLogout',
		],

		'Illuminate\Auth\Events\Lockout' => [
			'App\Listeners\LogLockout',
		],

		'Illuminate\Auth\Events\PasswordReset' => [
			'App\Listeners\LogPasswordReset',
		],  
		'Illuminate\Mail\Events\MessageSending' => [
			'App\Listeners\LogMessageSending',
		],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
