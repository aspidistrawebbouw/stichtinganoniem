<?php

namespace App;
use App\AanvraagStatus;
use \DateTime;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Aanvraag extends Model
{
    protected $fillable = ['user_id', 'register_id', 'aanvraagStatus'];
    protected $table = 'nl_aanvragen';
    public  $timestamps = true;

	public function deelnemer()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
	public function register()
	{
		return $this->belongsTo('App\Register');
	}
	
	public function status()
	{
		return $this->belongsTo('App\AanvraagStatus', 'aanvraagStatus');
	}
	
	public function prestaties()
	{
		return $this->hasMany('App\Prestatie');
	}
	
	public function goedkeurder()
	{
		return $this->belongsTo('App\User', 'goedgekeurd_door');
	}
	
	public function vervolg()
	{
		// is deze aanvraag een eerste aanvraag, of een vervolg op een eerder ingediende en goedgekeurde aanvraag voor hetzelfde register?
		return Aanvraag::where('user_id', $this->user_id)->where('register_id', $this->register_id)->where('aanvraagStatus', 3)->where('id', '!=', $this->id)->get()->isNotEmpty();
	}
	
	public function kritisch()
	{
// Om te kunnen laten zien of een certificaat geldig is, bijna verlopen, of verlopen, kijken we naar het verschil (in dagen) tussen 
// vandaag ($datetime2) en de datum van goedkeuring van de aanvraag plus de geldigheid ervan ($datetime1).
// Als dat verschil < 0 is, is het certificaat verlopen;
// als het >= 0 maar < 28 (4 weken) is het certificaat bijna verlopen;
// als het > 28 is is het certificaat geldig.

		if ($this->aanvraagStatus != 3) return 0; 					// 2 = goedgekeurd
		$datetime1 = new DateTime(date('Y-m-d'));
		$datetime2 = new DateTime($this->goedgekeurd . " + " . $this->register->geldig . " months - 1 days");
		$interval = date_diff($datetime1, $datetime2); // aantal dagen dat het certificaat nog geldig is. Indien negatief (invert == 1) is het certificaat dus verlopen.
		if ($interval->invert == 0 && $interval->days >= 28) return 1;	// geldig
		if ($interval->invert == 0 && $interval->days > 0) return 2;	// geldig maar bijna verlopen
		return 3;														// verlopen
	}

}
