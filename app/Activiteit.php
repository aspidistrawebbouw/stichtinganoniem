<?php
// Zou beter "Criterium" kunnen heten, maar dat is kennelijk een beschermd woord. Blijft hangen op "Class App\Criterium not found".
// Anyway, een Activiteit is een vereiste voor één specifiek register. Als voor twee registers dezelfde prestatie verlangd wordt, 
// (bijv VoG) dan betekent dat twee entries, niet één.
// Eigenlijk moet hierin ruimte zijn voor variabele velden.
namespace App;

use Illuminate\Database\Eloquent\Model;

class Activiteit extends Model
{
    protected $fillable = ['beschrijving', 'naam', 'register_id', 'invulinstructie', 'volgnummer', 'criType_id', 'vervolg', 'veld1', 'veld2'];
    
    // vervolg = 0: dit is een criterium voor toelating
    // vervolg = 1: dit is een vervolg-criterium
    
    protected $table = 'nl_criteria';
    public $timestamps = true;

    public function criteriumType()
    {
		return $this->belongsTo('App\CriType', 'criType_id');
	}
	
	public function register()
	{
		return $this->belongsTo('App\Register', 'register_id');
	}
}


/*
 * Veld1 en veld2 in het geval de Activiteit vastgelegd is als Model:
 * 
 * criType_id = 7 (PEPunten): veld1 = minimum nodig voor certificering; veld2 = maximum dat nog beoordeeld wordt
 * criType_id = 12 (JaNee): veld1 = tekst bij waarde 0, veld2 = tekst bij waarde1
 * criType_id = 13 (Opleidingentabel): veld1 = min aantal opleidingen getoond, veld2 = max aantal opleidingen in te vullen
 * */
