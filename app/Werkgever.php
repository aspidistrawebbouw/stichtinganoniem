<?php

// Een Opleiding is een regel in een OpleidingsTabel, en dat is een Prestatie van type 13.

namespace App;

use Illuminate\Database\Eloquent\Model;

class Werkgever extends Model
{
    protected $fillable = ['prestatie_id', 'periode', 'naam', 'functie' ];
    protected $table = 'nl_werkgevers';
    public $timestamps = false;
    
    public function werkgevertabel()
    {
		return $this->belongsTo('App\Prestatie');		
	}
    
}
