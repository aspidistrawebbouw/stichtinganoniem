<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $fillable = ['naam'];
    protected $table = 'nl_rollen';
    public  $timestamps = false;

	public function personen()
	{
		return $this->hasMany('App\User');
	}
}
