<?php

namespace App\Policies;

use App\User;
use App\Aanvraag;
use App\PrestatieUpload;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class PrestatieUploadPolicy
{
    use HandlesAuthorization;
    
	public function before($user, $ability)
	{
		if ($user->isAdmin()) {
			return true;
		}
	}
    /**
     * Determine whether the user can view any aanvraags.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user, PrestatieUpload $upload)
    {
		// Een persoon mag een upload verwijderen als:
		// - het z'n eigen upload is
		// - hij/zij beheerder is van het register
		// - hij/zij in de toetsingscommissie zit van het register
		// - hij/zij admin is
		$prestatie = $upload->prestatie;
		$aanvraag = $prestatie->aanvraag;
		Log::info(Auth::user()->name . " PUP01 Mag ik upload " . $upload->id . " verwijderen?");
		
		return  ($user->isDeelnemer() && $aanvraag->user_id == $user->id)  || 
				($user->isBeheerder() && $aanvraag->register->wordtBeheerdDoor($user->id)) || 
				($user->isTC() && $aanvraag->register->wordtGetoetstDoor($user->id)) || 
				$user->isAdmin();
    }

    /**
     * Determine whether the user can store the aanvraag.
     *
     * @param  \App\User  $user
     * @param  \App\Aanvraag  $aanvraag
     * @return mixed
     */
}
