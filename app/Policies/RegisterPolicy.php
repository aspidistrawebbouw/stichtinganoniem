<?php

namespace App\Policies;

use App\User;
use App\Register;
use Illuminate\Auth\Access\HandlesAuthorization;

class RegisterPolicy
{
    use HandlesAuthorization;
    
	public function before($user, $ability)
	{
		if ($user->isAdmin()) {
			return true;
		}
	}
    /**
     * Determine whether the user can view any aanvraags.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewBeheer(User $user)
    {
		return $user->isAdmin();
    }

    /**
     * Determine whether the user can view the aanvraag.
     *
     * @param  \App\User  $user
     * @param  \App\Aanvraag  $aanvraag
     * @return mixed
     */
    public function register_index(User $user)
    {
		return $user->isBeheerder() || $user->isAdmin();
	}
    public function register_lijst(User $user)
    {
		return true;
	}
    public function register_create(User $user)
    {
		return $user->isBeheerder() || $user->isAdmin();
	}
    public function register_store(User $user)
    {
		return $user->isBeheerder() || $user->isAdmin();
	}
    public function register_edit(User $user, Register $register)
    {
		return ($user->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}
    public function register_update(User $user, Register $register)
    {
		return ($user->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}
    public function register_delete(User $user, Register $register)
    {
		return ($user->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}
    public function register_delete_confirm(User $user, Register $register)
    {
		return ($user->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}

    public function register_beheerDocument(User $user, Register $register)
    {
		return ($user->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}
    public function register_editCritTekst(User $user, Register $register)
    {
		return ($user->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}

// Teksten beheren

    public function register_slottekst(User $user, Register $register)
    {
		return ($user->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}
    public function register_updateTekst(User $user, Register $register)
    {
		return ($user->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}
    public function register_voegTekstToe(User $user, Register $register)
    {
		return ($user->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}
    public function register_verplaatsTekst(User $user, Register $register)
    {
		return ($user->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}    
	public function register_verplaatsCrit(User $user, Register $register)
    {
		return ($user->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}
    public function register_verwijderTekst(User $user, Register $register)
    {
		return ($user->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}

// Leden TC

    public function register_voegTcToe(User $user, Register $register, User $lidtc)
    {
		return ($lidtc->isTC() && $register->wordtGetoetstDoor($user->id)) || $user->isAdmin();
	}
    public function register_verwijderTc(User $user, Register $register, User $lidtc)
    {
		return ($lidtc->isTC() && $register->wordtGetoetstDoor($user->id)) || $user->isAdmin();
	}

// Beheerders

    public function register_voegBeheerderToe(User $user, Register $register, User $beheerder)
    {
		return ($beheerder->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}
    public function register_verwijderBeheerder(User $user, Register $register, User $beheerder)
    {
		return ($beheerder->isBeheerder() && $register->wordtBeheerdDoor($user->id)) || $user->isAdmin();
	}
     
}
