<?php

namespace App\Policies;

use App\User;
use App\Aanvraag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Log;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Stel vast of de gebruiker een lijst van personen mag inzien
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function persoonIndex(User $user)
    {
		Log::info(Auth::user()->name . ' P11P PersoonPolicy index ' . $user->naam);
		// beheerders mogen alle personen zien
		return $user->isBeheerder();
    }
    public function persoonCreate(User $user)
    {
		Log::info(Auth::user()->name . ' P21P PersoonPolicy create ' . $user->naam);
		// beheerders mogen een nieuwe gebruiker aanmaken
		return $user->isBeheerder();
    }
    public function persoonStore(User $user)
    {
		Log::info(Auth::user()->name . ' P31P PersoonPolicy store ' . $user->naam);
		// beheerders mogen gegevens van een nieuwe gebruiker opslaan
		return $user->isBeheerder();
    }
    public function persoonEdit(User $user, User $teWijzigen)
    {
		Log::info(Auth::user()->name . ' P41P PersoonPolicy edit ' . $user->naam);
		// beheerders mogen gegevens van een nieuwe gebruiker wijzigen,
		// en een gebruiker mag zijn/haar eigen gegevens wijzigen
		return $user->isBeheerder() || $user->id == $teWijzigen->id;
    }
    public function persoonUpdate(User $user, User $teWijzigen)
    {
		Log::info(Auth::user()->name . ' P51P PersoonPolicy update ' . $user->naam);
		// beheerders mogen gewijzigde gegevens van een nieuwe gebruiker opslaan
		// en een gebruiker mag zijn/haar eigen gewijzigde gegevens opslaan
		return $user->isBeheerder() || $user->id == $teWijzigen->id;
    }
    public function persoonDelete(User $user, User $teWijzigen)
    {
		Log::info(Auth::user()->name . ' P61P PersoonPolicy delete ' . $user->naam);
		// beheerders mogen gebruikers verwijderen.
		// Voor toekomstig gebruik wordt $teWijzigen meegegeven zodat bijv. registerbeheerders de deelnemers aan hun eigen register kunnen verwijderen
		return $user->isBeheerder();
    }
    public function persoonDeleteConfirm(User $user, User $teWijzigen)
    {
		Log::info(Auth::user()->name . ' P71P PersoonPolicy delete ' . $user->naam);
		// beheerders mogen gebruikers verwijderen.
		// Voor toekomstig gebruik wordt $teWijzigen meegegeven zodat bijv. registerbeheerders de deelnemers aan hun eigen register kunnen verwijderen
		return $user->isBeheerder();
    }
    public function persoonBeheerRollen(User $user, User $teWijzigen)
    {
		Log::info(Auth::user()->name . ' P81P PersoonPolicy beheerRollen ' . $user->naam);
		// admins mogen de rol van gebruikers wijzigen
		// Voor toekomstig gebruik wordt $teWijzigen meegegeven zodat bijv. registerbeheerders de deelnemers aan hun eigen register kunnen wijzigen
		return $user->isAdmin();
    }
    /**
     * Determine whether the user can view the aanvraag.
     *
     * @param  \App\User  $user
     * @param  \App\Aanvraag  $aanvraag
     * @return mixed
     */
}
