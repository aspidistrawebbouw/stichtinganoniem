<?php

namespace App\Policies;

use App\User;
use App\Aanvraag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\Access\HandlesAuthorization;

class AanvraagPolicy
{
    use HandlesAuthorization;
    
	public function before($user, $ability)
	{
		if ($user->isAdmin()) {
			return true;
		}
	}
    /**
     * Determine whether the user can view any aanvraags.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {  
		Log::info(Auth::user()->name . ' A01P AanvraagPolicy index');
		return $user->isBeheerder() || $user->isTC() || $user->isAdmin();
    }

    /**
     * Determine whether the user can view the aanvraag.
     *
     * @param  \App\User  $user
     * @param  \App\Aanvraag  $aanvraag
     * @return mixed
     */
    public function toets(User $user, Aanvraag $aanvraag)
    {
		// Een persoon mag een aanvraag toetsen (is aan alle criteria voldaan?) als:
		// - hij/zij beheerder is van het register
		// - hij/zij in de toetsingscommissie zit van het register
		// - hij/zij admin is
		return	($user->isBeheerder() && $aanvraag->register->wordtBeheerdDoor($user->id)) || 
				($user->isTC()        && $aanvraag->register->wordtGetoetstDoor($user->id)) || 
				$user->isAdmin();
	}

    public function indienen(User $user, Aanvraag $aanvraag)
    {
		// Een persoon mag een aanvraag indienen (= ter toetsing voorleggen) als:
		// - het z'n eigen aanvraag is
		// - hij/zij beheerder is van het register
		// - hij/zij in de toetsingscommissie zit van het register
		// - hij/zij admin is
		return 	($user->isDeelnemer() && $aanvraag->user_id == $user->id) || 
				($user->isBeheerder() && $aanvraag->register->wordtBeheerdDoor($user->id)) || 
				($user->isTC()        && $aanvraag->register->wordtGetoetstDoor($user->id)) || 
				$user->isAdmin();
   }
    /**
     * Determine whether the user can create aanvraags.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
		return ($user->isDeelnemer() || $user->isBeheerder() || $user->isTC() || $user->isAdmin());
    }

    /**
     * Determine whether the user can update the aanvraag.
     *
     * @param  \App\User  $user
     * @param  \App\Aanvraag  $aanvraag
     * @return mixed
     */
    public function edit(User $user, Aanvraag $aanvraag)
    {
		// Een persoon mag een aanvraag wijzigen als:
		// - het z'n eigen aanvraag is, en de status is "opgeslagen"
		// - het z'n eigen aanvraag is, en de status is "ingediend" of "goedgekeurd", maar dan kan de aanvraag niet gewijzigd worden
		// - hij/zij beheerder is van het register
		// - hij/zij in de toetsingscommissie zit van het register
		// - hij/zij admin is
		return  ($user->isDeelnemer() && $aanvraag->user_id == $user->id)  || 
				($user->isBeheerder() && $aanvraag->register->wordtBeheerdDoor($user->id)) || 
				($user->isTC()        && $aanvraag->register->wordtGetoetstDoor($user->id)) || 
				$user->isAdmin();
   }
    public function update(User $user, Aanvraag $aanvraag)
    {
		// Een persoon mag een gewijzigde aanvraag opslaan als:
		// - het z'n eigen aanvraag is
		// - hij/zij beheerder is van het register
		// - hij/zij in de toetsingscommissie zit van het register
		// - hij/zij admin is
		return  ($user->isDeelnemer() && $aanvraag->user_id == $user->id && $aanvraag->aanvraagStatus == 1)  || 
				($user->isBeheerder() && $aanvraag->register()->wordtBeheerdDoor($user->id)) || 
				($user->isTC()        && $aanvraag->register()->wordtGetoetstDoor($user->id)) || 
				$user->isAdmin();
   }

    /**
     * Determine whether the user can delete the aanvraag.
     *
     * @param  \App\User  $user
     * @param  \App\Aanvraag  $aanvraag
     * @return mixed
     */
    public function delete(User $user, Aanvraag $aanvraag)
    {
		// Een persoon mag een aanvraag verwijderen als:
		// - het z'n eigen aanvraag is
		// - hij/zij beheerder is van het register
		// - hij/zij in de toetsingscommissie zit van het register
		// - hij/zij admin is
		return  ($user->isDeelnemer() && $aanvraag->user_id == $user->id)  || 
				($user->isBeheerder() && $aanvraag->register()->wordtBeheerdDoor($user->id)) || 
				($user->isTC()        && $aanvraag->register()->wordtGetoetstDoor($user->id)) || 
				$user->isAdmin();
    }

    /**
     * Determine whether the user can store the aanvraag.
     *
     * @param  \App\User  $user
     * @param  \App\Aanvraag  $aanvraag
     * @return mixed
     */
    public function store(User $user, Aanvraag $aanvraag)
    {
		return $user->isDeelnemer() || $user->isBeheerder() || $user->isTC() || $user->isAdmin();
    }

    /**
     * Determine whether the user can permanently delete the aanvraag.
     *
     * @param  \App\User  $user
     * @param  \App\Aanvraag  $aanvraag
     * @return mixed
     */
    public function forceDelete(User $user, Aanvraag $aanvraag)
    {
        //
    }
}
