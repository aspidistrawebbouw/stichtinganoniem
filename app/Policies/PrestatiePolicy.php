<?php

namespace App\Policies;

use App\User;
use App\Aanvraag;
use App\Prestatie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Log;

class PrestatiePolicy
{
    use HandlesAuthorization;

	public function before($user, $ability)
	{
		if ($user->isAdmin()) {
			return true;
		}
	}

    /**
     * Stel vast of de gebruiker een prestatie kan opslaan.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function prestatieStore(User $user, Aanvraag $aanvraag)
    { 
		Log::info(Auth::user()->name . ' PR11P PrestatiePolicy store ' . $user->naam . ' ' . $aanvraag->user_id . ' ' . $aanvraag->id);
		// een deelnemer kan een prestatie opslaan in z'n eigen aanvraag
		return $user->isBeheerder() || $user->isTC() || $user->isAdmin() || ($user->isDeelnemer() && $aanvraag->user_id == $user->id);
    }
    public function prestatieUpdate(User $user, Prestatie $prestatie, Aanvraag $aanvraag)
    {
		Log::info(Auth::user()->name . ' PR12P PrestatiePolicy update ' . $user->naam . ' ' . $aanvraag->user_id . ' ' . $aanvraag->id);
		// een deelnemer kan een prestatie wijzigen in z'n eigen aanvraag
		return $user->isBeheerder() || $user->isTC() || $user->isAdmin() || ($user->isDeelnemer() && $aanvraag->user_id == $user->id && $prestatie->aanvraag_id = $aanvraag->id);
    }
    public function prestatieDelete(User $user, Prestatie $prestatie, Aanvraag $aanvraag)
    {
		Log::info(Auth::user()->name . ' PR12P PrestatiePolicy delete ' . $user->naam . ' ' . $aanvraag->user_id . ' ' . $aanvraag->id);
		// een deelnemer kan een prestatie verwijderen in z'n eigen aanvraag
		// beheerders kunnen een prestatie verwijderen voor registers die zij beheren
		// leden TC kunnen een prestatie verwijderen voor registers die zij toetsen
		return  ($user->isBeheerder() && $aanvraag->register()->wordtBeheerdDoor($user->id)) || 
				($user->isTC()  && $aanvraag->register()->wordtGetoetstDoor($user->id)) || 
				$user->isAdmin() || 
				($user->isDeelnemer() && $aanvraag->user_id == $user->id && $prestatie->aanvraag_id = $aanvraag->id);
    }

    /**
     * Determine whether the user can view the aanvraag.
     *
     * @param  \App\User  $user
     * @param  \App\Aanvraag  $aanvraag
     * @return mixed
     */
}
