<?php

// Een JaNee verwijst naar een Prestatie van criType 12.

namespace App;

use Illuminate\Database\Eloquent\Model;

class JaNee extends Model

{
	protected $fillable = ['prestatie_id', 'janee'];
	protected $table = 'nl_janee';
    public $timestamps = false;

	public function prestatie()
	{
		return $this->belongsTo('App\Prestatie');
	}
}
