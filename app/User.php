<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cmgmyr\Messenger\Traits\Messagable;
use App\Aanvraag;
use App\Register;
use App\Notifications\ResetWachtwoordMail;
use App\Notifications\NieuweGebruikerMail;
use App\Notifications\VerifieerEmailadresMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use Messagable;

	/**
	 * The table that holds the user data
	 * 
	 * @var string
	 */
	protected $table = 'nl_personen';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	/**
	 * Functies voor wachtwoordprocedures: stuur wachtwoord-reset-link, en verifieer emailadres
	 * 
	 * */
	 
	public function sendEmailVerificationNotification()
	{
		Log::info("User Verificatie-email gestuurd voor " . $this->name);
		$this->notify(new VerifieerEmailadresMail());
	}

    public function sendPasswordResetNotification($token)
	{
		Log::info("User Herstel-wachtwoord-email gestuurd voor " . $this->name);
		$this->notify(new ResetWachtwoordMail($token));
	}
    
    public function sendEmailNieuweGebruiker()
    {
        Log::info("Nieuwe gebruiker " . $this->name . " met geverifiëerd emailadres " . $this->email);
        $admins = User::whereHas('rollen',function (Builder $query){$query->where('nl_rollen.id', 1);})->get();
        foreach($admins as $admin)
        {
            $admin->notify(new NieuweGebruikerMail($this, $admin));
        }
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function rollen()
    {
		return $this->belongsToMany('App\Rol', 'nl_rol_user');
	}
	
	public function aanvragen()
	{
		return $this->hasMany('App\Aanvraag');
	}
	
	public function registersInBeheer()
	{
		return $this->belongsToMany('App\Register', 'nl_registerbeheer');
	}

	public function registersToetsen()
	{
		return $this->belongsToMany('App\Register', 'nl_tc');
	}
	
	public function registersAangevraagd()
	{
		return Register::whereIn('id', Aanvraag::where('user_id', $this->id)->pluck('register_id'))->get();
	}	
	
	public function heeftRol($rol)
	{
		return $this->rollen->where('id', $rol)->isNotEmpty();
	}
	
	public function isAdmin()
	{
		return $this->rollen->where('naam', 'Admin')->isNotEmpty();
	}
	
	public function isBeheerder()
	{
		return $this->rollen->where('naam', 'Beheerder')->isNotEmpty();
	}
	
	public function isTC()
	{
		return $this->rollen->where('naam', 'Lid TC')->isNotEmpty();
	}
	
	public function isDeelnemer()
	{
		foreach($this->rollen as $rol) { if ($rol->naam == 'Deelnemer') return true; } return false;
	}
	
	public function isAspirantDeelnemer()
	{
		foreach($this->rollen as $rol) { if ($rol->naam == 'Aspirant-deelnemer') return true; } return false;
	}
	
	public function factuuradres() 
	{
		return $this->hasOne('App\Factuuradres');
	}
}
