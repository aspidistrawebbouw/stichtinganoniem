<?php
// Tekst gegeven als antwoord op een criterium

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrestatieTekst extends Model
{
    protected $fillable = ['prestatie_id', 'tekst'];
    protected $table = 'nl_prestatieteksten';
    public $timestamps = false;
    
    public function prestatie()
    {
		return $this->belongsTo('App\Prestatie');
	}
}
