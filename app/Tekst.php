<?php
// Informatieteksten behorend bij een register"

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tekst extends Model
{
    protected $fillable = ['register_id', 'volgnummer', 'tekst'];
    protected $table = 'nl_teksten';
    public $timestamps = false;
    
    public function register()
    {
		return $this->belongsTo('App\Register');
	}
}
