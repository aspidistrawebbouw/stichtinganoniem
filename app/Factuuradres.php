<?php

// Een Opleiding is een regel in een OpleidingsTabel, en dat is een Prestatie van type 13.

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factuuradres extends Model
{
    protected $fillable = ['user_id', 'tenaamstelling', 'factuuradresr1', 'factuuradresr2','postcode', 'plaats' ];
    protected $table = 'nl_factuuradressen';
    public $timestamps = false;
    
    public function persoon()
    {
		return $this->belongsTo('App\User');		
	}
    
}
