<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNlOpleidingenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('nl_opleidingen', function(Blueprint $table)
		{
			$table->foreign('prestatie_id', 'nl_opleidingen_ibfk_1')->references('id')->on('nl_prestaties')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('nl_opleidingen', function(Blueprint $table)
		{
			$table->dropForeign('nl_opleidingen_ibfk_1');
		});
	}

}
