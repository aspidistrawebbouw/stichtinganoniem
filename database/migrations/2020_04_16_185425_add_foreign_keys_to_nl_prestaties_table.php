<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNlPrestatiesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table ('nl_prestaties', function (Blueprint $table) {
            $table->foreign ('activiteit_id', 'nl_prestaties_ibfk_1')->references ('id')->on ('nl_criteria')->onUpdate ('RESTRICT')->onDelete ('CASCADE');
            $table->foreign ('aanvraag_id', 'nl_prestaties_ibfk_2')->references ('id')->on ('nl_aanvragen')->onUpdate ('RESTRICT')->onDelete ('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table ('nl_prestaties', function (Blueprint $table) {
            $table->dropForeign ('nl_prestaties_ibfk_1');
            $table->dropForeign ('nl_prestaties_ibfk_2');
        });
    }

}
