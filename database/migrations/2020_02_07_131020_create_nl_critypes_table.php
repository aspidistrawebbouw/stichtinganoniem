<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlCritypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nl_critypes', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('naam');
			$table->text('veld1', 65535)->nullable();
			$table->text('veld2', 65535)->nullable();
			$table->text('veld3', 65535)->nullable();
			$table->text('check1', 65535)->nullable();
			$table->text('check2', 65535)->nullable();
			$table->integer('min')->nullable()->default(0);
			$table->integer('max')->nullable()->default(0);
			$table->text('beschrijving', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nl_critypes');
	}

}
