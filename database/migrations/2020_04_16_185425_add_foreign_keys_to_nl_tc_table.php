<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNlTcTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table ('nl_tc', function (Blueprint $table) {
            $table->foreign ('register_id', 'nl_tc_ibfk_1')->references ('id')->on ('nl_registers')->onUpdate ('RESTRICT')->onDelete ('CASCADE');
            $table->foreign ('user_id', 'nl_tc_ibfk_2')->references ('id')->on ('nl_personen')->onUpdate ('RESTRICT')->onDelete ('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table ('nl_tc', function (Blueprint $table) {
            $table->dropForeign ('nl_tc_ibfk_1');
            $table->dropForeign ('nl_tc_ibfk_2');
        });
    }

}
