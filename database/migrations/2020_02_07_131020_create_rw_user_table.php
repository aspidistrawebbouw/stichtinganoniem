<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRwUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rw_user', function(Blueprint $table)
		{
			$table->increments('user_id');
			$table->string('username')->default('')->unique('username');
			$table->string('user_password')->default('');
			$table->string('user_firstname', 100)->nullable();
			$table->string('user_lastname', 100)->nullable();
			$table->string('user_email', 150)->nullable()->unique('user_email');
			$table->string('user_role')->nullable()->default('');
			$table->dateTime('createdate')->nullable();
			$table->dateTime('updatedate')->nullable();
			$table->string('user_hash')->nullable();
			$table->string('publisher', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rw_user');
	}

}
