<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNlAanvragenTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table ('nl_aanvragen', function (Blueprint $table) {
            $table->foreign ('register_id', 'nl_aanvragen_ibfk_1')->references ('id')->on ('nl_registers')->onUpdate ('RESTRICT')->onDelete ('CASCADE');
            $table->foreign ('user_id', 'nl_aanvragen_ibfk_2')->references ('id')->on ('nl_personen')->onUpdate ('RESTRICT')->onDelete ('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table ('nl_aanvragen', function (Blueprint $table) {
            $table->dropForeign ('nl_aanvragen_ibfk_1');
            $table->dropForeign ('nl_aanvragen_ibfk_2');
        });
    }

}
