<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlRegistersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nl_registers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 10);
			$table->string('naam', 100);
			$table->string('actief', 1);
			$table->timestamp('laatste_wijziging')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamps();
			$table->integer('geldig')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nl_registers');
	}

}
