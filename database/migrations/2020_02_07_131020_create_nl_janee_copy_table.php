<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlJaneeCopyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nl_janee_copy', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('janee')->nullable();
			$table->bigInteger('prestatie_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nl_janee_copy');
	}

}
