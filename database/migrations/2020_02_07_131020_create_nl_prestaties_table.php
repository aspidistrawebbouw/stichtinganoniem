<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlPrestatiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nl_prestaties', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('aanvraag_id')->nullable();
			$table->integer('activiteit_id');
			$table->date('datum');
			$table->string('beschrijving');
			$table->string('opmerkingen');
			$table->integer('pe')->nullable();
			$table->integer('status');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nl_prestaties');
	}

}
