<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlAanvragenTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('nl_aanvragen', function (Blueprint $table) {
            $table->bigInteger ('id', true)->unsigned ();
            $table->bigInteger ('user_id')->unsigned ()->index ('user_id');
            $table->integer ('register_id')->index ('register_id');
            $table->integer ('aanvraagStatus');
            $table->dateTime ('opgeslagen')->nullable ();
            $table->dateTime ('ingediend')->nullable ();
            $table->dateTime ('afgekeurd')->nullable ();
            $table->timestamps ();
            $table->dateTime ('goedgekeurd')->nullable ();
            $table->integer ('goedgekeurd_door')->nullable ();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('nl_aanvragen');
    }

}
