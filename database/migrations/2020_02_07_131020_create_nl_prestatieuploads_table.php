<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlPrestatieuploadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nl_prestatieuploads', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('prestatie_id')->nullable();
			$table->string('filenaam', 500)->nullable();
			$table->string('originelefilenaam', 500)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nl_prestatieuploads');
	}

}
