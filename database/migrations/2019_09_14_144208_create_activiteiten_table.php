<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActiviteitenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rw_activiteiten', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('naam');
            $table->string('beschrijving');
            $table->integer('register_id');
            $table->integer('actType_id');
            $table->integer('peVereist');
            $table->integer('peMaximum');
            $table->timestamps();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rw_activiteiten');
    }
}
