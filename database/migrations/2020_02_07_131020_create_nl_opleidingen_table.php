<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlOpleidingenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nl_opleidingen', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->bigInteger('prestatie_id')->unsigned()->index('prestatie_id');
			$table->string('opleiding', 100)->nullable();
			$table->string('periode', 100)->nullable();
			$table->boolean('diploma')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nl_opleidingen');
	}

}
