<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlRolUserTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('nl_rol_user', function (Blueprint $table) {
            $table->integer ('id', true);
            $table->integer ('rol_id')->nullable ();
            $table->bigInteger ('user_id')->unsigned ()->index ('user_id');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('nl_rol_user');
    }

}
