<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlCriteriaTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('nl_criteria', function (Blueprint $table) {
            $table->bigInteger ('id', true)->unsigned ();
            $table->string ('naam');
            $table->text ('beschrijving', 65535)->nullable ();
            $table->string ('invulinstructie')->nullable ();
            $table->integer ('volgnummer')->nullable ();
            $table->integer ('register_id')->index ('register_id');
            $table->bigInteger ('criType_id')->unsigned ()->index ('criType_id');
            $table->timestamps ();
            $table->boolean ('vervolg')->nullable ()->default (0);
            $table->string ('veld1', 100)->nullable ();
            $table->string ('veld2', 100)->nullable ();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('nl_criteria');
    }

}
