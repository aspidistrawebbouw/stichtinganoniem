<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNlPrestatietekstenTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table ('nl_prestatieteksten', function (Blueprint $table) {
            $table->foreign ('prestatie_id', 'nl_prestatieteksten_ibfk_1')->references ('id')->on ('nl_prestaties')->onUpdate ('RESTRICT')->onDelete ('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table ('nl_prestatieteksten', function (Blueprint $table) {
            $table->dropForeign ('nl_prestatieteksten_ibfk_1');
        });
    }

}
