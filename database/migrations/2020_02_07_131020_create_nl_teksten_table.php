<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlTekstenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nl_teksten', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('register_id')->nullable();
			$table->integer('volgnummer')->nullable();
			$table->string('tekst', 999)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nl_teksten');
	}

}
