<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlCritypesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('nl_critypes', function (Blueprint $table) {
            $table->bigInteger ('id', true)->unsigned ();
            $table->string ('naam');
            $table->text ('beschrijving', 65535)->nullable ();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('nl_critypes');
    }

}
