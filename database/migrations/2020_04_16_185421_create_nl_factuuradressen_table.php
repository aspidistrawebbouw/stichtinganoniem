<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlFactuuradressenTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('nl_factuuradressen', function (Blueprint $table) {
            $table->integer ('id', true);
            $table->bigInteger ('user_id')->unsigned ()->index ('user_id');
            $table->string ('tenaamstelling', 100)->nullable ();
            $table->string ('factuuradresr1', 100)->nullable ();
            $table->string ('factuuradresr2', 100)->nullable ();
            $table->string ('postcode', 6)->nullable ();
            $table->string ('plaats', 100)->nullable ();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('nl_factuuradressen');
    }

}
