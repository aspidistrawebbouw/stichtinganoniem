<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlRegisteruploadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nl_registeruploads', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->integer('register_id')->nullable();
			$table->string('filenaam', 500)->nullable();
			$table->string('originelefilenaam', 500)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nl_registeruploads');
	}

}
