<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegistersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('registers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('Titel', 100)->nullable();
			$table->string('Voorletters', 100)->nullable();
			$table->string('Tussenvoegsel', 100)->nullable();
			$table->string('Achternaam', 100)->nullable();
			$table->string('Bedrijfsnaam', 100)->nullable();
			$table->string('Plaats', 100)->nullable();
			$table->string('Email', 100)->nullable();
			$table->string('Website', 100);
			$table->string('Soort', 100)->nullable()->index('Soort_7');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('registers');
	}

}
