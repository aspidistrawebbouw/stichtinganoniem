<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlTekstenTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('nl_teksten', function (Blueprint $table) {
            $table->integer ('id', true);
            $table->integer ('register_id')->index ('register_id');
            $table->integer ('volgnummer')->nullable ();
            $table->text ('tekst', 65535)->nullable ();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('nl_teksten');
    }

}
