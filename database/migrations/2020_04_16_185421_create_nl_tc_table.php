<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlTcTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('nl_tc', function (Blueprint $table) {
            $table->integer ('id', true);
            $table->bigInteger ('user_id')->unsigned ()->index ('user_id');
            $table->integer ('register_id')->index ('register_id');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('nl_tc');
    }

}
