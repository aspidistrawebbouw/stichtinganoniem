<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRwPersonenTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('rw_personen', function (Blueprint $table) {
            $table->integer ('id', true);
            $table->string ('titel', 100)->nullable ();
            $table->string ('voorletters', 100)->nullable ();
            $table->string ('achternaam', 200)->nullable ();
            $table->string ('bedrijfsnaam', 200)->nullable ();
            $table->string ('plaats', 100)->nullable ();
            $table->string ('email', 100)->nullable ();
            $table->string ('website', 200)->nullable ();
            $table->string ('register', 2000)->default ('List()');
            $table->string ('actief', 1)->default ('N');
            $table->timestamp ('laatste_wijziging')->default (DB::raw ('CURRENT_TIMESTAMP'));
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('rw_personen');
    }

}
