<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlWerkgeversTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('nl_werkgevers', function (Blueprint $table) {
            $table->integer ('id', true);
            $table->bigInteger ('prestatie_id')->unsigned ()->index ('prestatie_id');
            $table->string ('periode', 100)->nullable ();
            $table->string ('naam', 100)->nullable ();
            $table->string ('functie', 100)->nullable ();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('nl_werkgevers');
    }

}
