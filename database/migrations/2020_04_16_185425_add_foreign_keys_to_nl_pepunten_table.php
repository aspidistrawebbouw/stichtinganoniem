<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNlPepuntenTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table ('nl_pepunten', function (Blueprint $table) {
            $table->foreign ('prestatie_id', 'nl_pepunten_ibfk_1')->references ('id')->on ('nl_prestaties')->onUpdate ('RESTRICT')->onDelete ('CASCADE');
            $table->foreign ('prestatie_id', 'nl_pepunten_ibfk_2')->references ('id')->on ('nl_prestaties')->onUpdate ('RESTRICT')->onDelete ('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table ('nl_pepunten', function (Blueprint $table) {
            $table->dropForeign ('nl_pepunten_ibfk_1');
            $table->dropForeign ('nl_pepunten_ibfk_2');
        });
    }

}
