<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNlPrestatietekstenTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('nl_prestatieteksten', function (Blueprint $table) {
            $table->integer ('id', true);
            $table->text ('tekst', 65535)->nullable ();
            $table->bigInteger ('prestatie_id')->unsigned ()->index ('prestatie_id');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('nl_prestatieteksten');
    }

}
