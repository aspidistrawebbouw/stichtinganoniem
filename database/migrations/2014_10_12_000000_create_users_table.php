<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    // gewijzigd Jos
        Schema::create('nl_personen', function (Blueprint $table) {
	//
            $table->bigIncrements('id');
    // gewijzigd Jos
            $table->string('naam');
    //
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
    // toegevoegd Jos
            $table->integer('rol');
    //
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    // gewijzigd Jos
        Schema::dropIfExists('nl_personen');
    //
    }
}
