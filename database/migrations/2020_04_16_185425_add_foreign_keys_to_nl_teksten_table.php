<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNlTekstenTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table ('nl_teksten', function (Blueprint $table) {
            $table->foreign ('register_id', 'nl_teksten_ibfk_1')->references ('id')->on ('nl_registers')->onUpdate ('RESTRICT')->onDelete ('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table ('nl_teksten', function (Blueprint $table) {
            $table->dropForeign ('nl_teksten_ibfk_1');
        });
    }

}
