<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNlRolUserTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table ('nl_rol_user', function (Blueprint $table) {
            $table->foreign ('user_id', 'nl_rol_user_ibfk_1')->references ('id')->on ('nl_personen')->onUpdate ('RESTRICT')->onDelete ('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table ('nl_rol_user', function (Blueprint $table) {
            $table->dropForeign ('nl_rol_user_ibfk_1');
        });
    }

}
