<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Register;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Register::class, function (Faker $faker) {
    return [
        'naam' => $faker->text($maxNbChars = 100),
        'code' => $faker->regexify('[A-Z]{3,5}'),
        'beheerder_id' => 4,
        'actief' => 'J',
    ];
});
