<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Tekst;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Faker\Provider\Base;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Tekst::class, function (Faker $faker) {
    return [
        'register_id' => $faker->numberBetween($min = 10, $max = 32),
        'volgnummer' => $faker->randomDigit,
        'tekst' => $faker->text($maxNbChars = 500)
    ];
});
