<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Activiteit;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Faker\Provider\Base;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Activiteit::class, function (Faker $faker) {
    return [
        'register_id' => $faker->numberBetween($min = 10, $max = 32),
        'criType_id' => $faker->numberBetween($min = 1, $max = 8),
        'beschrijving' => $faker->text($maxNbChars = 255),
        'naam' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        'peVereist' => $faker->numberBetween($min = 1, $max = 8) * 10,
        'peMaximum' => $faker->numberBetween($min = 5, $max = 10) * 10,
    ];
});
