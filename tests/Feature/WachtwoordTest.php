<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Register;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Generator as Faker;
use Faker\Provider\Lorem;

class WachtwoordTest extends TestCase
{

/** @test */
	public function a_password_can_be_validated()
	{
		$rule = ['password' => [new \App\Rules\SterkWachtwoord]];
		$this->assertFalse(validator(['password' => '1'], $rule)->passes());
		$this->assertFalse(validator(['password' => '1#'], $rule)->passes());
		$this->assertFalse(validator(['password' => 'a1#'], $rule)->passes());
		$this->assertFalse(validator(['password' => 'aB1#'], $rule)->passes());
		$this->assertFalse(validator(['password' => 'Ert123#'], $rule)->passes());
		$this->assertTrue(validator(['password' => 'Ert1234#'], $rule)->passes());
		$this->assertTrue(validator(['password' => 'H!ghwayPatr0l'], $rule)->passes());
		
	}

}
