<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Register;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Generator as Faker;
use Faker\Provider\Lorem;

class AanvraagTest extends TestCase
{

    public function testAanvragen()
    {
        $garry = User::where ('name', 'Garry Swaniawski')->first ();
        $this->be ($garry);
// doe aanvraag voor reeds eerder aangevraagd register
        $response = $this->get ('http://olympus/aspidistra/anoniem/public/aanvragen/create?register=34');
// je wordt omgeleid naar de edit-pagina
//		dd($response->headers);
        $location2 = $response->headers->get ('location');
        echo "URL: " . $location2;
        $response->assertStatus (302);

    }
}
