<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Register;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Generator as Faker;
use Faker\Provider\Lorem;

class RegisterTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
/** @test */
    public function testRegisteraanmaken()
    {
		$dick = User::where('name', 'Dick Sijtsma')->first();
		$this->be($dick);
// maak een nieuw register aan met random velden
        $response = $this->json('POST', 'http://localhost:8000/registers', [
				'naam' => app('Faker\Generator')->text($maxNbChars = 60),
				'code' => app('Faker\Generator')->regexify('[A-Z]{3,5}'),
				'beheerder_id' => 8,
				'actief' => 'J',
				]);
// je wordt omgeleid naar de edit-pagina
        $response->assertStatus(302);
        
// haal de id van het nieuwe register uit de omleid-locatie
		$nieuwReg = explode('/', $response->headers->all()['location'][0]);
		$nieuwRegister = $nieuwReg[4];
		$this->assertIsNumeric($nieuwRegister, $message = 'Aanmaken nieuw register mislukt');
		echo "Nieuw register: $nieuwRegister";

// bezoek de omleid-locatie (d.w.z. de edit-pagina van het register)
		$location = $response->headers->all()['location'][0];
		$response = $this->get($location);
        $response->assertStatus(200);
        
// wijzig de naam van de app 
        $response = $this->json('POST', 'http://localhost:8000/registers/' . $nieuwRegister, [
				'naam' => app('Faker\Generator')->text($maxNbChars = 60),
				'code' => app('Faker\Generator')->regexify('[A-Z]{3,5}'),
				'beheerder_id' => 4,
				'actief' => 'N',
				'_method' => 'PUT'
				]);
				
// je wordt omgeleid naar de edit-pagina
        $response->assertStatus(302);
		$location2 = $response->headers->all()['location'][0];
		$this->assertEquals($location, $location2);

// verwijder het nieuwe register        
        $response = $this->json('DELETE', 'http://localhost:8000/registers/' . $nieuwRegister);
        $response->assertStatus(200);
        
    }
}
